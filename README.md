# README - Audit web portal build process #
**Version 1.0**
This README documents steps required to build & run audit web portal in browser.

## 1.	Prerequisites and Hardware requirements ##
### Environment  required	###
    * Mac OS X 10.12 OR  Above
    * Windows	7 OR Above	

### Software required ###
    * Install Node 
		- Download and install from https://nodejs.org/en/.
		- Set the path to NodeJs(upto bin directory) in evironment variable.
	* Install Git
		- Download and install from https://git-scm.com/
	* Install gulp
		- npm install -g gulp (install gulp globally) 
    * ruby 	2.0.0p648 (2015-12-16 revision 53162)[universal.x86_64-darwin16] 
		- Download and install from https://www.ruby-lang.org/en/downloads/
	* Sass	3.4.22 
		- Download and install from http://sass-lang.com/install 
    
## 2. Building Audit web portal ##
### When building Audit web portal for the first time after checkout ###

* Checkout the project from BitBucket     
```
#!Shell
git fetch && git checkout master
```

* Navigate to the project directory
* install gulp locally
```
#!Shell
npm install --save-dev gulp 
```

* Build the Audit web portal
* Run gulp task to import all images,html file, js files, directives to the single folder(cleans folders before importing) and that is to be referred from index.hmtl
```
#!shell
gulp
```

* Run and open the Audit web portal in broswer    
```
#!shell
gulp dev
```

### When building Audit web portal subsequently  ###

* Build the Audit web portal (Required if the files other than images, partials, js files altered)
```
#!shell
gulp
```

* Run and open the Audit web portal in broswer    
```
#!shell
gulp dev
```

## 3. Browser versions supported
* Internet Explorer (version 11 and above) 
* Chrome
* Mozilla Firefox 


## 4. References
* https://nodejs.org/en/
* https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md
* https://docs.angularjs.org
