/*global angular */

/**
 * The main  app module
 *
 * @type {angular.Module}
 */

angular = require('angular');
require('angular-route');
require('../AuditPortal/templateCachePartials');
require('angular-ui-router');
require('angular-ui-bootstrap');
require('angular-messages');
require('angular-cookies');
require('angular-utils-pagination');
require('ngStorage');
require('group-array');



angular.module('dealerAudit', ['tempPartials','ui.router','ui.bootstrap','ngMessages','ngStorage','ngCookies','angularUtils.directives.dirPagination'])
	.config(['$stateProvider', '$urlRouterProvider', '$httpProvider','$locationProvider', function($stateProvider, $urlRouterProvider, $httpProvider,$locationProvider) {
	$httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
	/*$locationProvider.html5Mode({
        enabled: true
    });*/
	/*$urlRouterProvider.rule(function ($injector, $location) {
       //what this function returns will be set as the $location.url
        var path = $location.path(), normalized = path.toLowerCase();
        if (path != normalized) {
            //instead of returning a new url string, I'll just change the $location.path directly so I don't have to worry about constructing a new url string and so a new state change is not triggered
            $location.replace().path(normalized);
        }
        // because we've returned nothing, no state change occurs
    });*/
	$stateProvider
		.state('login', {
			url: '/login',
			views: {
				header: {
					
				},		
				content: {
					templateUrl: '/partials/loginview.html',
					controller: 'loginController'
				},
				footer: {
					templateUrl: '/partials/footer.html',
				}
			},
			authenticate: false
		})


        .state('location', {
			url: '/location',
			views: {
				header: {
					templateUrl: '/partials/header.html',
					controller: 'homeController'
				},
		
				content: {
					templateUrl: '/partials/managelocations.html',
					controller : 'manageLocationController'
				},
				footer: {
					templateUrl: '/partials/footer.html',
				}
			},
			authenticate: true
		})


        .state('auditdetails', {
			url: '/auditdetails',
			views: {
				header: {
					templateUrl: '/partials/header.html',
					controller: 'homeController'
				},
				subheader: {
					templateUrl: '/partials/auditdetails.html',
					controller: 'auditController'
					
				},

				content: {
					templateUrl: '/partials/auditsquestion.html',
					controller: 'auditController'
				},
		
				
				footer: {
					templateUrl: '/partials/footer.html'
				}
			},
			authenticate: true
		})



			.state('questionnaire', {
			url: '/questionnaire',
			views: {
				header: {
					templateUrl: '/partials/header.html',
					controller: 'questionController'
				},
				subheader: {
					templateUrl: '/partials/managetemplatetabs.html',
					controller: 'questionController'
				},
				subheader1: {
					templateUrl: '/partials/creatquestionaire.html',
					controller: 'questionController'
				},
		
				content: {
					templateUrl: '/partials/createquestions.html',
					controller: 'questionController'
					
				},
                list : {
                    templateUrl: '/partials/questionlist.html',
                    controller: 'questionController'

                },

				footer: {
					templateUrl: '/partials/footer.html'
				}
			},
			authenticate: true
		})

      	 .state('templateList', {
			url: '/templateList',
			views: {
				header: {
					templateUrl: '/partials/header.html',
					controller: 'questionController'
				},
				subheader: {
					templateUrl: '/partials/managetemplatetabs.html',
					controller: 'questionController'
				},
				list : {
				     templateUrl: '/partials/template.html',
				     controller: 'questionController'
				},
				footer: {
					templateUrl: '/partials/footer.html'
				}
			},
			authenticate: true
		})

      	 /* .state('currentyearaudits', {
			url: '/currentyearaudits',
			views: {
				header: {
					templateUrl: '/partials/header.html',
					controller: 'homeController'
				},
				subheader: {
					templateUrl: '/partials/manageaudittabs.html',
					controller: 'auditController'
				},
				list : {
				     templateUrl: '/partials/currentyearaudits.html',
				     controller: 'auditController'
				},
				footer: {
					templateUrl: '/partials/footer.html'
				}
			},
			authenticate: true
		})

      	  .state('previousaudits', {
			url: '/previousaudits',
			views: {
				header: {
					templateUrl: '/partials/header.html',
					controller: 'homeController'
				},
				subheader: {
					templateUrl: '/partials/manageaudittabs.html',
					controller: 'auditController'
				},
				list : {
				     templateUrl: '/partials/previousaudits.html',
				     controller: 'auditController'
				},
				footer: {
					templateUrl: '/partials/footer.html'
				}
			},
			authenticate: true
		})*/

      	  /*.state('uploadaudits', {
			url: '/uploadaudits',
			views: {
				header: {
					templateUrl: '/partials/header.html',
					controller: 'homeController'
				},
				subheader: {
					templateUrl: '/partials/manageaudittabs.html',
					controller: 'auditController'
				},
				list : {
				     templateUrl: '/partials/uploadaudits.html',
				     controller: 'auditController'
				},
				footer: {
					templateUrl: '/partials/footer.html'
				}
			},
			authenticate: true
		})*/
      	  
		.state('home', {
			url: '/home',
			views: {
				header: {
					templateUrl: '/partials/header.html',
					controller: 'homeController'
				},
				content: {
					templateUrl: '/partials/homeview.html',
					controller: 'homeController'
				},
				footer: {
					templateUrl: '/partials/footer.html',
				}
			},
			authenticate: true
		})

		//State Provider for Dealer Screen
		.state('audits', {
			url: '/audits',
			views: {
				header: {
					templateUrl: '/partials/header.html',
					controller: 'auditController'
				},
				content: {
					templateUrl: '/partials/manageaudittabs.html',
					controller: 'auditController'
				},
				footer: {
					templateUrl: '/partials/footer.html',
				}
			},
			authenticate: true

		})
		.state('audits.previousaudits', {
			url: '/previousaudits',
			templateUrl: '/partials/previousaudits.html',
			controller: 'auditController',
			authenticate: true
		})
		
		.state('audits.currentyearaudits', {
			url: '/currentyearaudits',
			templateUrl: '/partials/currentyearaudits.html',
			controller: 'auditController',
			authenticate: true
		})
		.state('audits.uploadaudits', {
			url: '/uploadaudits',
			templateUrl: '/partials/uploadaudits.html',
			controller: 'auditController',
			authenticate: true
		})

		
		//State Provider for Dealer Screen
		.state('dealer', {
			url: '/dealer',
			views: {
				header: {
					templateUrl: '/partials/header.html',
					controller: 'dealerController'
				},
				content: {
					templateUrl: '/partials/createdealer.html',
					controller: 'dealerController'
				},
				footer: {
					templateUrl: '/partials/footer.html',
				}
			},
			authenticate: true

		})
		
		.state('dealer.createdealer', {
			url: '/createdealerform/:createOrUpdate',
			templateUrl: '/partials/createdealerform.html',
			controller: 'dealerController',
			authenticate: true
		})
		
		.state('dealer.listdealer', {
			url: '/dealerlist',
			templateUrl: '/partials/dealerlist.html',
			controller: 'homeController',
			authenticate: true
		})
		//State Provider for Dealer Screen
		.state('manageTeams', {
			url: '/manageTeams',
			views: {
				header: {
					templateUrl: '/partials/header.html',
					controller: 'homeController'
				},
				content: {
					templateUrl: '/partials/manageteam.html',
					controller: 'manageTeamsController'
				},
				footer: {
					templateUrl: '/partials/footer.html',
				}
			},
			authenticate: true

		})
	//$urlRouterProvider.otherwise('/Login');
}])





.run(function($rootScope, $location, $state, dealerAudit_ConstantsConst,$uibModal,passParameterFactory) {
	
	// after refresh retrieve date stored in localstorage
	
	$rootScope.session = JSON.parse(localStorage.getItem('sessionObj'));

	$rootScope.UserName= localStorage.getItem('userName');
	//Username
	$rootScope.UserName= localStorage.getItem('userName');

	//dealerlist elements in edit mode
	var dealerListElements = localStorage.getItem('dealerListElements');
	$rootScope.dealerListElements = JSON.parse(dealerListElements);

	// template info
	var TemplateInfo = localStorage.getItem('templateInfo');
	$rootScope.templateInfo = JSON.parse(TemplateInfo);

	//flag to detect template edit mode 
	$rootScope.updateTemplate = localStorage.getItem('updateTemplate');	

	//template id
	$rootScope.savedtemplateId = localStorage.getItem('savedtemplateId');

	//template master date in edit mode
	$rootScope.templateToUpdate = localStorage.getItem('templateToUpdate');

	$rootScope.networkValues =  JSON.parse(localStorage.getItem('networkValues'));
	$rootScope.tts_nameValues =  JSON.parse(localStorage.getItem('tts_nameValues'));

	if((typeof($rootScope.session)!='undefined'|| $rootScope.session!=null)&&($location.path()!='')){
			if($location.path()!='/login'&& $location.path()!='/location' && $location.path()!='/audits/currentyearaudits'&& $location.path()!='/audits/previousaudits'&& $location.path()!='/audits/uploadaudits' && $location.path()!='/auditdetails' && $location.path()!='/questionnaire' && $location.path()!='/templateList' && $location.path()!='/home' && $location.path()!='/dealer' && $location.path()!='/dealer/dealerlist' && $location.path()!='/dealer/createdealerform/create' && $location.path()!='/dealer/createdealerform/update'){

				var rootURL = localStorage.getItem('rootURL');
				if(rootURL!=null){
					if(rootURL=='/dealer' || rootURL =='/createdealerform/:createOrUpdate' || rootURL == '/dealerlist' ){
						$state.go('dealer');

					}else if(rootURL =='/questionnaire' || rootURL == '/templateList'){
						$state.go('templateList');
					}
					else if(rootURL =='/currentyearaudits' || rootURL == '/previousaudits'|| rootURL == '/uploadaudits'){
						$state.go('audits.currentyearaudits');
					}
					else{
						rootURL = rootURL.substring(rootURL.lastIndexOf('/')+1);
						$state.go(rootURL);
					}
				}else{
					$state.go('login');
				}
			}
	}

	if(typeof($rootScope.session)=='undefined'||$rootScope.session==null){
			
			$state.go('login');
	}	
	
		$rootScope.open = function(value,controller) {
		    $rootScope.modalInstance = $uibModal.open({
				animation: true,
				templateUrl: '/partials/confirmationmodal.html',
				controller: controller,
				size: 'sm',
				scope: $rootScope,
				// Prevent closing by clicking outside or escape button.
				backdrop: 'static',
				keyboard: false
			});
			
			$rootScope.modalInstance.modalValue = value;
		};



	$rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
		
		//fetching stored session object and based on authentication preventing navigation if session has been lost
		var sessionObj = localStorage.getItem('sessionObj');		
		sessionObj = JSON.parse(sessionObj);	
		var rootURL = toState.url;
		localStorage.setItem('rootURL',rootURL);
		if(toState.authenticate && (sessionObj == null || sessionObj == undefined)) {
			// User isn’t authenticated				
			event.preventDefault();	   
			window.history.forward();   
			
		
		}
	});


	$rootScope.$on('$locationChangeStart', function(event,next, current) {

			var next = next.substring(next.lastIndexOf('/')+1);
			var current = current.substring(current.lastIndexOf('/')+1);
			var sessionObj = localStorage.getItem('sessionObj');		
			sessionObj = JSON.parse(sessionObj);
			// When browser back button is clicked from home page ask for confirmation to logout before navigating to login page
			// checking for $rootScope.checkFlag so that popup should be shown only 1st time once after logged out flag is made as true until user login again to avoid multiple popups
			
			
				// 1.if session object is present then only show popup
				//2. When auto logged off dont show the popup


			if((next == 'login' && current == 'home' && $rootScope.autoLogoutFlag== undefined &&(sessionObj != null ||sessionObj != undefined)) && !$rootScope.checkFlag){
				
				
				event.preventDefault();
				$rootScope.checkFlag=true;	
				$rootScope.modalTitle = "Confirmation";
				$rootScope.modalContent = "Are you sure you want to logout?";
				$rootScope.open('logoutForBackButn','homeController');		

			}else if((next == 'login' && current == 'home' && $rootScope.autoLogoutFlag== undefined &&(sessionObj != null ||sessionObj != undefined)) && $rootScope.checkFlag){
				event.preventDefault();
			}
		
			// when user in create or update page if user click back button is should go to home page not to dealerlist page 
			if((next == 'dealerlist' && current == 'create') || (next == 'dealerlist' && current == 'update')){
				if($rootScope.formChanged && !$rootScope.modelOpened){
						
						event.preventDefault();
						$rootScope.modelOpened = true;						
						$rootScope.modalTitle = "Confirmation";
						$rootScope.modalContent = "Are you sure you do not want to save the changes?";
						$rootScope.open('navigateToBackTab','dealerController');
				}else if($rootScope.formChanged && $rootScope.modelOpened){
					event.preventDefault();
				}
			}
			// when user in dealer list page if user click back button is should go to home page not to dealer page 
			if(current == 'dealerlist' && next == 'dealer'){
				event.preventDefault();	   
				$state.go('home');
			}



			if(current == 'uploadaudits' && (next == 'previousaudits' ||  next == 'currentyearaudits')){
				passParameterFactory.setNavigateToLocation(next);
				if($rootScope.selected_files.length>0 && !$rootScope.modelOpened &&(sessionObj != null ||sessionObj != undefined)){
						event.preventDefault();
						$rootScope.modelOpened = true;						
						$rootScope.modalTitle = "Confirmation";
						$rootScope.modalContent = "Are you sure you do not want to save the changes?";
						$rootScope.open('navigateToBackTab','auditController');
				}else if($rootScope.selected_files.length>0  && $rootScope.modelOpened){
					event.preventDefault();
				}
			}

			if(current == 'questionnaire' && next == 'templateList'){
				if((($rootScope.templateInfoChanged || $rootScope.questionInfoChanged) || ($rootScope.editQuestion && $rootScope.questionupdateInfoChanged)|| ($rootScope.editQuestion && $rootScope.questionupdateInfoChangedMinMax))&& !$rootScope.modelOpened ){
						
						event.preventDefault();	
						$rootScope.modelOpened = true;
						$rootScope.modalTitle = "Confirmation";
						$rootScope.modalContent = "Are you sure you do not want to save the changes?";
						$rootScope.open('navigateToTemplateList','questionController');
				}else if((($rootScope.templateInfoChanged || $rootScope.questionInfoChanged) || ($rootScope.editQuestion && $rootScope.questionupdateInfoChanged)|| ($rootScope.editQuestion && $rootScope.questionupdateInfoChangedMinMax))&& $rootScope.modelOpened ){
						event.preventDefault();	
				}
			}
		  	
		  
	});

	$rootScope.$on('$locationChangeSuccess', function(event) {
		  	
		  	if($location.path()=='/login'){		

		  	//when logged out clearing all the data from locat storage		
				localStorage.removeItem('userName');
				localStorage.removeItem('sessionObj');
				localStorage.removeItem('isFirstTime');
				localStorage.removeItem('isFirstTimeHomePage');
				localStorage.removeItem('isIndealerListPage');

				localStorage.removeItem('networkValues');
				localStorage.removeItem('tts_nameValues');
				localStorage.removeItem('templateInfo');
				localStorage.removeItem('savedtemplateId');
				localStorage.removeItem('templateToUpdate');
				localStorage.removeItem('updateTemplate');
				localStorage.removeItem('collectAuditResponse');
				localStorage.removeItem('rootURL');
				localStorage.removeItem('ttsAuditInformation');
				localStorage.removeItem('topDealers');
				localStorage.removeItem('auditResonseData');
				localStorage.removeItem('previousLocation');
				localStorage.removeItem('currentyearpagination');
				localStorage.removeItem('prevyearpagination');
				localStorage.removeItem('questionupdateInfoChanged');
				localStorage.removeItem('questionInfoChanged');
				localStorage.removeItem('questionupdateInfoChangedMinMax');
				$rootScope.logoutFlag = true; 
				//$rootScope.checkFlag=null;
			}
	});

})


.filter('highlight', function($sce) {
          return function(text, phrase) {
            if (phrase) text = text.replace(new RegExp('('+phrase+')', 'gi'),
              '<span class="highlighted" style="color:yellow;background:black">$1</span>')
            
            return $sce.trustAsHtml(text)
          }
 });

/**
 * List of all controllers
 */

require('./controllers/loginController');
require('./controllers/modelController');
require('./controllers/createdealercontroller');
require('./controllers/homeController');
require('./controllers/autoSessionOut');
require('./controllers/questionController');
require('./controllers/manageLocationController');
require('./controllers/auditController');
require('./controllers/manageTeamsController');
require('./controllers/passParameterFactory');

/**
 * List of all common files like assests, constants
 */
require('./common/constants');
require('./common/assets');

/**
 * List of all directives used
 */

require('./directives/googleplace.js');
require('./directives/googleregion.js');
require('./directives/uniqueResults.js');






