/**
 * @controller: homeController
 * @description This module includes functionalities for adding countries under manager , navigating to different pages
 */

angular = require('angular');

angular.module('dealerAudit').controller('homeController', function ($scope,$state,$rootScope,$uibModal,$location,$q,$filter,$timeout,dealerAudit_ConstantsConst) {
	
	
try{

	$scope.username = $rootScope.UserName;
    var groupArray = require('group-array');
     $rootScope.shouldShowTyreImg = false;
     $scope.auditReportReady=false;
     $scope.noDetails=false;

     $scope.auditRecordYear = new Date().getFullYear();
	/**
     * @function dealer
     * @description function to navigate to Truckforce dealers page.     
     */
	$scope.dealer = function() {
           
			localStorage.setItem('dealerlistPagination',1);
			$state.go('dealer');
	};
	
	/**
     * @function audit
     * @description function to navigate to Audit's page.     
     */
	$scope.audit = function() {			
			//$state.go('audits');
            localStorage.setItem('currentyearpagination',1);
            localStorage.setItem('prevyearpagination',1);
            $state.go('audits.currentyearaudits');
	};

	/**
     * @function gotoHome
     * @description function to navigate to homepage(Dashboard) page.     
     */
	$scope.gotoHome = function(){
		$state.go('home');
    };  

	/**
     * @function question
     * @description function to navigate to Manage Audit templates page.     
     */ 
     $scope.question = function(){
       $state.go('questionnaire');
    }; 
   

	/**
     * @function template
     * @description function to navigate to template list page.     
     */
    $scope.template = function(){
       $state.go('templateList');
    }; 
    /**
     * @function template
     * @description function to navigate to template list page.     
     */
    $scope.manageTeams = function(){
       $state.go('manageTeams');
    }; 
    /**
     * @function goTolocation
     * @description function to navigate to Manage locations page.     
     */
    $scope.goTolocation = function(){
    	 $state.go('location');
    }

     /**
     * @function navigateToHome
     * @description function checkk for all the fields whether they have been modified, if so ask for confirmation before navigating to other page.     
     */
     $scope.navigateToHome = function(event){
			   
		$state.go('home');		  
    };

    /**
     * @function signOut
     * @description function for logout.     
     */
	$scope.signOut = function() {
			try {
				
					$rootScope.session.logout();
					$rootScope.logoutFlag = true;				
					localStorage.removeItem('sessionObj');

					$state.go('login');
				
			} catch(e) {
				
			}
			
		}		
		$scope.createPoll = function(){			
		}

    /**
     * @function ok
     * @description function to perform logout on click of ok button in confirmation popup when browser back button is pressed from dashboard page.     
     */
    $scope.ok = function(){

            if($rootScope.modalInstance.modalValue == "logoutForBackButn"){                                       
                $rootScope.modalInstance.dismiss();
                $state.go('login');
                
            }else{
                $rootScope.modalInstance.dismiss();
            }   
   }

   /**
     * @function cancel
     * @description function to prevent navigation to login page on click of cancel button in confirmatipon popup when browser back button is pressend from dashboard .     
     */
   $scope.cancel = function(){
        $rootScope.checkFlag = false; 
        $rootScope.modalInstance.dismiss();
   }
	/**
     * @function addCOuntryModel
     * @description function opens a bootstrap modal for selecting country and adding under manager.     
     */
	$scope.addCOuntryModel = function (value) {
				
		       $scope.modalInstance = $uibModal.open({
		       templateUrl: '/partials/homepopup.html',
		       backdrop: 'static',
		       windowClass: 'modal',
		       size: 'lg',
		       controller:'modelController',
		       scope: $scope,		      
				keyboard: false
		    });

		     $scope.modalInstance.modalValue = value;
		};  

     
	$scope.tempSavedLocationForTam = [];
    var locationPresentForTam= false;

    $scope.initLoad= function(){
        ISession.getInstance().then(function(session) {
        $rootScope.session = session;   
        
        var loginFlag = localStorage.getItem('loginFirstTime');
        if(loginFlag == 1){
                localStorage.setItem('loginFirstTime',0);
                $rootScope.shouldShowTyreImg = true;  
               
                         
                    getSaved_locationForTam().then(function(result) {
                        //flag is used to download data only one time after login  
                        if(result){                           
                            $scope.getAuditData();                                            
                        }else{
                            $rootScope.shouldShowTyreImg = false;  
                            $scope.addCOuntryModel();
                        }                        
                    },function(getSaved_locationForTam){
                        $rootScope.shouldShowTyreImg = false;
                        $scope.addCOuntryModel();                        
                    });
              
        }else{
            ttsAuditInformation = JSON.parse(localStorage.getItem('ttsAuditInformation'));
            topDealersInfo = JSON.parse(localStorage.getItem('topDealers'));
         
            if(ttsAuditInformation==null || topDealersInfo==null || ttsAuditInformation.length == 0 || topDealersInfo.length==0 ||ttsAuditInformation.length == undefined || topDealersInfo.length==undefined){                        
                     $scope.getAuditData();  
            }else{
                    $rootScope.shouldShowTyreImg = false;
                    var graphData = JSON.parse(localStorage.getItem('auditResonseData'));
                    calculateMonthlyAuditChart(graphData);
            }    
        }


                             
        });

    }
    /**
     * @function getSaved_locationForTam
     * @description function check for countries present under manager, if not opens a modal to add a country under manager.     
     */ 
    function getSaved_locationForTam(){

    try{
    	       			
        return $q.when($rootScope.session.getClientObjectType("saved_locations").then(function(clientObjectType){
                    
                    return $q.when(clientObjectType.list(0,100000,null,true).then(function(response) {
                       
                        for(i=0;i<response.length;i++){
                            $scope.tempSavedLocationForTam.push(response[i].data);
                        }
                        
                        $scope.tempSavedLocationForTam.forEach(function(o){
                            if(o.tam_name == $rootScope.UserName){
                                locationPresentForTam = true;
                            }
                        }); 
                        if(!locationPresentForTam){                                    
                           return false;
                           // $scope.addCOuntryModel();
                        }

                     return true;
                    },function(error){                                	
                    	return false;
                       
                    }));

                },function(error){
                    return false;
                    
                }));
			

	} catch(error){			
	
		
	}		
			

    };



    $scope.$watch(function(){   
        
        $scope.ttsAuditInformation = ttsAuditInformation;
        $scope.topDealersInfo=topDealersInfo;

    });

    var response_data=[];
    var templateQuestionList=[];

    if(ttsAuditInformation ==null || typeof(ttsAuditInformation)=='undefined'){
            var ttsAuditInformation=[];
    }
    if(topDealersInfo ==null || typeof(topDealersInfo)=='undefined'){
            var topDealersInfo=[];
    }
    if(topDealersInfo ==null || typeof(topDealersInfo)=='undefined'){
           var topDealersInfo=[];
    }
    if($scope.ttsAuditInformation ==null || typeof($scope.ttsAuditInformationo)=='undefined'){
            $scope.ttsAuditInformation=[];
    }   
    
    $scope.dealerMasterData=[];
    $scope.templateMasterData = [];
    $scope.templateQuestionList = [];
    $scope.auditResonseData=[];
    $scope.ttsListAssignedToManager=[];


    $scope.getAuditData = function(){

            $rootScope.shouldShowTyreImg = true;          

                getDealerData().then(function(result) {

                    $scope.dealerMasterData = result;                                
                    
                    //calling next functions only when getDealerData function does not give error
                    if(result.code == undefined || result.code == null){
                        getTemplateMasterData().then(function(result){

                                $scope.templateMasterData = result;                              
                                
                                if(result.code == undefined || result.code == null){
                                    getQuestionMasterData($scope.templateMasterData).then(function(result){

                                        $scope.templateQuestionList = result;
                                      
                                        if(result.code == undefined || result.code == null ){

                                            getAuditResponseData().then(function(result){

                                                    $scope.auditResonseData=result; 
                                                    localStorage.setItem('auditResonseData',JSON.stringify($scope.auditResonseData))   
                                                    calculateMonthlyAuditChart($scope.auditResonseData);
                                                    getTTSassignedToManager().then(function(result){

                                                        $scope.ttsListAssignedToManager = result;                                                   
                                                        calculateAuditPercentage();

                                                    },function(errorGetTTSassignedToManager){
                                                       
                                                        $scope.noDetails=true;
                                                        $rootScope.shouldShowTyreImg=false;
                                                        $scope.$apply();
                                                        
                                                    })

                                            },function(errorGetAuditResponseData){
                                             
                                                $rootScope.shouldShowTyreImg=false;
                                                $scope.noDetails=true;
                                                $scope.$apply();                                                
                                            })
                                        }else{
                                           
                                            $rootScope.shouldShowTyreImg = false; 
                                            $scope.noDetails=true;
                                        
                                        }
                                    },function(errorGetQuestionMasterData){
                                        $rootScope.shouldShowTyreImg=false;
                                        $scope.noDetails=true;
                                        $scope.$apply();                                       
                                    });
                                }else{
                                    
                                    $rootScope.shouldShowTyreImg = false; 
                                    $scope.noDetails=true;
                                    
                                }
                        },function(errorGetTemplateMasterData){

                            $rootScope.shouldShowTyreImg=false;
                            $scope.$apply();                           
                        });
                    }else{
                        
                        $rootScope.shouldShowTyreImg = false; 
                       
                    }
                },function(errorFromgetDealerData){
                    $rootScope.shouldShowTyreImg=false;
                    $scope.$apply();                   
                });           
        }

        function getDealerData(){           
                  
                var dealerListItems = [];
                // collecting all the dealer information
                return $q.when($rootScope.session.getClientObjectType("dealers").then(function(clientObjectType){
                        return $q.when(clientObjectType.list(0, 100000, null, true).then(function(response) {                       
                         
                                  for(i=0;i<response.length;i++){
                                      dealerListItems.push(response[i].getData());
                                  }                    
                                                
                                  return dealerListItems;
                        },function(error){
                            
                               return error;      
                        }));
           
                },function(error){                 
                      return error;
                      

                }));           

        }


        function getTemplateMasterData(){           
                $scope.templateMasterData=[];  
                var filterParams = {
                    "tam_name": $rootScope.UserName,                                               
                };
                // collecting all the dealer information
               return $q.when($rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){
                        return $q.when(clientObjectType.search("template_master", 0, 10000, filterParams, null, null, null, null, null, true).then(function(response) {

                             
                             for(i=0;i<response.length;i++){
                                $scope.templateMasterData.push(response[i].getData())
                             }

                             //Filtering the list of templates which are not deleted
                            /* for(template=0;template<$scope.templateMasterData.length;template++){
                                if($scope.templateMasterData[template].status=='Deleted'){
                                    $scope.templateMasterData.splice(template,0);
                                }
                             }*/
                                               
                                                
                            return $scope.templateMasterData;
                        },function(error){
                            
                               return error;      
                        }));
           
                },function(error){                 
                      return error;

                }));           

        }

         function getQuestionMasterData(templateMasterData){           
                 var templateMasterData=templateMasterData; 
                 var questionMasterData = [];
                // collecting all the dealer information
                return $q.when($rootScope.session.getClientObjectType("question").then(function(clientObjectType){
                        return $q.when(clientObjectType.list(0, 100000, null, true).then(function(response) {                       
                            
                                for(i=0;i<response.length;i++){
                                  questionMasterData.push(response[i].getData());
                                }                    
                                   

                                // forming json array containing template id and number of questions it has
                                templateMasterData.forEach(function(template){
                                    var object={};
                                    var count=0;
                                    object['template_id']=template.template_id;

                                    questionMasterData.forEach(function(questionObj){

                                            if(template.template_id==questionObj.template_id){
                                                count=count+1;
                                            }
                                    });
                                    object['no_of_questions']=count;                                               
                                    templateQuestionList.push(object);
                                }); //end of for loop             
                                return templateQuestionList;
                        },function(error){
                            
                               return error;      
                        }));
           
                },function(error){                 
                      return error;
                     
                }));           

        }

        function getAuditResponseData(){

               
                var fromDate =  $filter('date')(new Date(new Date().getFullYear(), 0, 1),'yyyy-MM-dd:HH:mm:ss');
                var toDate =  $filter('date')(new Date(),'yyyy-MM-dd:HH:mm:ss');

                var filterStatement = [];
                var fs1=new FilterStatement("audit_date",FilterStatement.GREATER_THAN_EQUAL,fromDate).getFilterStatement();                                           
                var fs2=new FilterStatement("audit_date",FilterStatement.LESS_THAN_EQUAL,toDate).getFilterStatement();

                filterStatement.push(fs1);
                filterStatement.push(fs2);

                // collecting the audit response data belongs to current year
                return $q.when($rootScope.session.getClientObjectType("audit_response").then(function(clientObjectType){
                        return $q.when(clientObjectType.search("audit_response", 0,100000,null,null,null,null,null,filterStatement,true).then(function(response){

                                var auditResponseResult = [];
                                //getting complete audit response data
                                for(i=0;i<response.length;i++){
                                        auditResponseResult.push(response[i].getData());
                                }
                        
                                var audit_response_data=[];
                                //filter the audit response data which are submitted for the templates created by logged in manager
                               
                               
                                auditResponseResult.forEach(function(obj){                        
                                      for(i=0; i<$scope.templateMasterData.length;i++){
                                            if(obj.template_id == $scope.templateMasterData[i].template_id){
                                                 audit_response_data.push(obj);
                                            }
                                      }
                                });

                                if(audit_response_data.length<=0){
                                    $scope.noDetails=true;
                                }else{
                                    $scope.noDetails=false;
                                }
                                return audit_response_data;

                        },function(error){  
                            $scope.noDetails=true;                                 
                            return error;
                        })); // end of list audit_response
                                   
                },function(error){
                
                  return error;
                
                })); // end of getClientObjectType audit_response

        }


       function getTTSassignedToManager(){         
           
                                            

            var filterParams={
                "tam_name": $rootScope.UserName,
            }
            return $q.when($rootScope.session.getClientObjectType("TTS_master").then(function(clientObjectType){
       
                    return $q.when(clientObjectType.search("TTS_master", 0, 10000, filterParams, null, null, null, null, null, true).then(function(response) {
                            
                                var ttsAssignedToManager = [];
                                // getting the list of tts assigned to logged in manager
                                for(j=0;j<response.length;j++){
                                    ttsAssignedToManager.push(response[j].getData());
                                } 
                        
                                return ttsAssignedToManager;
                        
                    },function(error){
                        return error;               
                    })); // end of search TTS_master
             },function(error){     
                  return error;
             }));  // end of getClientObjectType TTS_master
                    
        }

        function calculateAuditPercentage(){
            try{
                    var auditsByTTSUnderManager = [];
                    // Filtering the audit response data which are done by the TTS who are under the logged manager
                    $scope.auditResonseData.forEach(function(o){
                            for(k=0;k<$scope.ttsListAssignedToManager.length;k++){                                                                
                                if(o.audit_done_by==$scope.ttsListAssignedToManager[k].tts_name){
                                    auditsByTTSUnderManager.push(o);
                                }
                            }
                    });  
                    auditsByTTSUnderManager = groupArray(auditsByTTSUnderManager, 'template_id','audit_done_by','dealer_id','audit_date'); 
                    
                  
                    //Below part is to calculate percentage of individual audit
                    var dealerAuditInformation=[]
                    for(index1 in auditsByTTSUnderManager){
                        //object1 is template
                        var object1 = auditsByTTSUnderManager[index1];                            
                        for(index2 in object1){

                            //object2 is tts who submitted an audit for above template
                            var object2 = object1[index2];                                
                            for(index3 in object2){

                                //object3 is dealer 
                                var object3 = object2[index3];                                
                                var recentAudit = (last=Object.keys(object3))[last.length-1];
                                //recentAuditByDealer is an array of audit records submitted by tts , dealer
                                var recentAuditByDealer = object3[recentAudit];                              
                                                                              
                                        var total_no_questions = 0;
                                        $scope.templateQuestionList.forEach(function(temp){
                                            if(temp.template_id == recentAuditByDealer[0].template_id){
                                                total_no_questions =temp.no_of_questions;
                                            }
                                        })                                       
                                        var no_of_quest_answered= recentAuditByDealer.length;                                       
                                        var percentage_audit_by_dealer = (100*no_of_quest_answered)/total_no_questions;
                                        percentage_audit_by_dealer = percentage_audit_by_dealer.toFixed(2);
                                        dealerAuditInformation.push({'template_id':recentAuditByDealer[0].template_id,'tts_name':recentAuditByDealer[0].audit_done_by,'dealer_id':recentAuditByDealer[0].dealer_id,'questions_answered':no_of_quest_answered,'total_questions':total_no_questions,'audit_percentage':percentage_audit_by_dealer,'audit_date':recentAuditByDealer[0].audit_date});
                                
                            }
                        }
                    }
                             
                    var dealerAuditInformationGroupBy = groupArray(dealerAuditInformation,'tts_name');

                    ttsAuditInformation=[];
                    // Below part is to calculate the Number of dealers TTS audited for and average audit percentage
                    for(index in dealerAuditInformationGroupBy){
                                var object=dealerAuditInformationGroupBy[index];
                                var tts_name=object[0].tts_name;
                                var no_of_dealers=object.length;
                                var avg_audit_result=0;
                                var total_audit_perc=0;
                                for(i=0;i<object.length;i++){                                                                            
                                    total_audit_perc = parseFloat(total_audit_perc)+ parseFloat(object[i].audit_percentage);
                                    
                                }   
                                avg_audit_result = parseFloat(total_audit_perc)/parseFloat(no_of_dealers);                                 

                                var str=avg_audit_result.toString();
                                var numarray=str.split('.');
                                var a=[];
                                a=numarray;
                                if(a[0]=='100'){
                                        avg_audit_result = avg_audit_result.toFixed(0);  
                                }else{
                                        avg_audit_result = avg_audit_result.toFixed(2);  
                                }
                                                                                           
                                ttsAuditInformation.push({'tts_name':tts_name,'no_of_dealers':no_of_dealers,'avg_audit_result':avg_audit_result});
                    }
                    ttsAuditInformation = ttsAuditInformation.sort(function(a, b) {return parseFloat(b.avg_audit_result) - parseFloat(a.avg_audit_result);})
                    //collecting top 5 dealers 
                    var dealer_audit_info_in_order =  dealerAuditInformation.sort(function(a, b) {return parseFloat(b.audit_percentage) - parseFloat(a.audit_percentage);});
                    var top_five_dealers =dealer_audit_info_in_order.slice(0, 5);

                    
                    topDealersInfo=[];
                    //collecting dealer informations of top 5 dealers
                    top_five_dealers.forEach(function(dealer){
                          
                            $scope.dealerMasterData.forEach(function(dealerInfo){

                                     if(dealer.dealer_id == dealerInfo.dealer_id){
                                        topDealersInfo.push({'dealer_name':dealerInfo.dealer_name,'dealer_country':dealerInfo.country_name,'dealer_score':dealer.audit_percentage});
                                     }
                            });
                    });


                    localStorage.setItem('ttsAuditInformation',JSON.stringify(ttsAuditInformation));
                    localStorage.setItem('topDealers',JSON.stringify(topDealersInfo));

                    $rootScope.shouldShowTyreImg=false;
                  //  $scope.$apply();

            }catch(error){           
                $rootScope.shouldShowTyreImg=false;                
                
            }


        };

         function calculateMonthlyAuditChart(data){

            try{                    
                        // calculating the number of audits done in each month of the year for the templates created by logged in manager.

                        var auditsCount = groupArray(data, 'template_id','audit_done_by','dealer_id','audit_date');                        
                       
                        var auditDetails=[];
                        for(audits in auditsCount){                          

                            var obj = auditsCount[audits];                        
                            for(index in obj){

                                var obj1 = obj[index];                               
                                for(index1 in obj1){

                                    var obj2 = obj1[index1];  
                                    var recentAudit = (last=Object.keys(obj2))[last.length-1];    
                                    var recentAuditByDealer = obj2[recentAudit];                                                    
                                    auditDetails.push({'template_id':recentAuditByDealer[0].template_id,'dealer_id':recentAuditByDealer[0].dealer_id,'audit_done_by':recentAuditByDealer[0].audit_done_by,'audit_date':recentAuditByDealer[0].audit_date});
                                }
                            }
                        }   

                      


                        var monthlyReport = {}
                        for (var i=0; i<auditDetails.length; i++) {
                           var obj = auditDetails[i];
                           var date = new Date(obj.audit_date);
                           var month = date.getMonth();
                          
                           if (monthlyReport[month]) {

                               monthlyReport[month].push(obj);  // already have a list- append to it
                           }
                           else {
                               monthlyReport[month] = [obj]; // no list for this month yet - create a new one
                           }
                        }                      
                      
                        if(Object.keys(monthlyReport).length>0){
                            $scope.auditReportReady=true;
                        }
                        $scope.monthlyAuditReport=[];
                        var months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                        for(i=0;i<12;i++){                           
                            if(monthlyReport[i]){
                                var number_of_audits_inMonth = monthlyReport[i].length;
                                $scope.monthlyAuditReport.push(number_of_audits_inMonth);
                            }else{
                                $scope.monthlyAuditReport.push(0);
                            }                                                         
                        }                     
                        
                        
                        var chart = {type: 'column',
                                     events: {
                                        load: function () {
                                          var chart = this;
                                          $timeout(function () { chart.reflow(); }, 200);                                              
                                        }
                                      }
                                    }; 
                        var title = {text: ""};                      
                        var xAxis = {categories: months,title: {text: 'Month'}};
                        var yAxis = {min: 0,title: {text: 'Number of Audits' },labels: {overflow: 'justify'}};
                        var tooltip = {valueSuffix: ''};
                        var plotOptions = {bar: {dataLabels: {enabled: true}}};
                        var colors = ['#2882dd' ];                        
                        var credits = {enabled: false};                       
                        var series= [{name: 'Audits',data: $scope.monthlyAuditReport}];     
                            
                        var auditGraphProperties = {};   
                        auditGraphProperties.chart = chart; 
                        auditGraphProperties.title = title;  
                        auditGraphProperties.colors = colors;                     
                        auditGraphProperties.tooltip = tooltip;
                        auditGraphProperties.xAxis = xAxis;
                        auditGraphProperties.yAxis = yAxis;  
                        auditGraphProperties.series = series;
                        auditGraphProperties.plotOptions = plotOptions;                        
                        auditGraphProperties.credits = credits;
                        //angular.element('#container').highcharts(auditGraphProperties);
                        $timeout(function(){angular.element('#container').highcharts(auditGraphProperties);},100);                 
                                           
            }catch(e){

                
            }

        }      

      } catch(error){			
	
		
	}
});

	
	