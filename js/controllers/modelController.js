/**
 * @controller: modelController
 * @description This module includes handling all the confirmations popup and add country popup
 */

angular = require('angular');
angular.module('dealerAudit').controller('modelController',function($scope,$state, $rootScope,$uibModal,dealerAudit_ConstantsConst) {
try{


     $scope.erroFlag = false;
    $scope.selectedCountry='';
    $scope.isAddCountryInProgress = false;
          
    /**
     * @function okClicked
     * @description function to perform actions when user click ok on confirmation popup   
     */
      
    $scope.okClicked = function() {

      if($scope.modalInstance.modalValue == "Confirmation"){          
          $scope.clearAllFields();
          $rootScope.isEditDealer = false;
          $scope.modalInstance.dismiss();
          
          //.go('dealer.createdealer', { 'createOrUpdate': "create", 'dealerId': '' });
          //return true;
      }
      else if($scope.modalInstance.modalValue == "Logout"){
        ISession.getInstance().then(function(session) {
          session.logout();
          $rootScope.logoutFlag = true;
          $state.go('login');         
          //below statement is required for only browser button handling .
          localStorage.removeItem('sessionObj');
        });
        
      }else if($scope.modalInstance.modalValue == "CreateDealer"){
        $state.go('dealer');
        //$scope.listdealer();
        //$state.reload();
        $scope.clearAllFields();
        $scope.saveDealerFlag = false;
      }
      else if($scope.modalInstance.modalValue == "UpdateDealer"){
        $state.go('dealer',{reload: true}); 
        $scope.clearAllFields();
        $scope.saveDealerFlag = false;      
      }
      else if($scope.modalInstance.modalValue == "Error"){
          $rootScope.popUpActive=  false;
          $scope.modalInstance.dismiss(); 
          $scope.saveDealerFlag = false;        
      }
      // else if($scope.modalInstance.modalValue == "Delete"){
      //    $scope.modalInstance.dismiss();
      // }
      else if($scope.modalInstance.modalValue == "DeleteConfirm"){
          $scope.deleteDealer($scope.dealer_idToDelete);
          
          $scope.modalInstance.dismiss();
          
      }   
      else if($scope.modalInstance.modalValue == "handleEditMode"){
        $state.go('home');
        $rootScope.isEditDealer = false;
      }
      else if($scope.modalInstance.modalValue == "handleCreateNewDealer"){
        
        $scope.clearAllFields();        
        $state.go('dealer.createdealer', { 'createOrUpdate': "create", 'dealerId': '' });
        $scope.modalInstance.dismiss();

      }else if($scope.modalInstance.modalValue == "handlecreatePageToDealerPage"){
            
        createPageToDealerPageHandle = false;
        $scope.clearAllFields();        
        $state.go('dealer',{reload: true});
        //$scope.listdealer();        
        $scope.modalInstance.dismiss();
      }
      else if($scope.modalInstance.modalValue == "logoutForBackButn"){
      //  $rootScope.checkFlag=true;        
        $scope.modalInstance.dismiss();
        $state.go('login');
        
      }
      else if($scope.modalInstance.modalValue == "AutoLogout"||$scope.modalInstance.modalValue == "Login"||$scope.modalInstance.modalValue == "connectionError" ||$scope.modalInstance.modalValue == "downloadExcelError"||$scope.modalInstance.modalValue == "publishTemplateHandle"|| $scope.modalInstance.modalValue == "handleSavebtnInerror" || $scope.modalInstance.modalValue == "alertHandle"){
        //$rootScope.autoLogoutFlag = undefined;
        $scope.modalInstance.dismiss();
                
      }
      else{
          $scope.modalInstance.dismiss();
         return;
      }
      

    };
     


    function getHeaderId() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)

        }
          return s4() + s4();
    }

    function getCountryForTam(arr, value) {    
      var result = [];
      var val=value;
      arr.forEach(function(o){        
        if (o.tam_name == val) 
            result.push(o);           
      } );
      return result? result : null; // or undefined
    }

    
     $scope.countryList =[];
    // get the countries stored for tam
    $scope.getLocations = function(){

        try{
                    $rootScope.session.getClientObjectType("Country").then(function(clientObjectType){
                        clientObjectType.list(0,-1,null,true).then(function(response) {
                           
                            $scope.tempCountryList = [];
                            $scope.countryList =[];
                            for(i=0 ; i< response.length ; i ++){
                                $scope.tempCountryList.push(response[i].getData());
                                
                            }
                            $scope.countryList = $scope.tempCountryList;                                               
                            $scope.$apply();

                            
                        },function(error){
                           
                        });

                    },function(error){
                    });

         }catch(e){           
        }
    };
   
   
    $scope.addLocationForManager = function(){

        try{

           if(!$scope.isAddCountryInProgress){

                   $scope.isAddCountryInProgress = true;
                   if($scope.selectedCountry.country_name == null || $scope.selectedCountry.country_name=="" ||$scope.selectedCountry.country_name == undefined||typeof($scope.selectedCountry.country_name)=="undefined"){

                     $scope.erroFlag = true;
                     $scope.isAddCountryInProgress = false;
                   }else{  

                        $scope.erroFlag = false;        
                        var locaId=getHeaderId();

                        $scope.saved_locations_info={
                            country_id: $scope.selectedCountry.country_id,
                            country_name : $scope.selectedCountry.country_name,
                            location_id : locaId,
                            tam_name : $rootScope.UserName,
                            longitude: $scope.selectedCountry.longitude,
                            latitude: $scope.selectedCountry.latitude
                        };

                      
                        $rootScope.session.getClientObjectType("saved_locations").then(function(clientObjectType){

                            clientObjectType.create($scope.saved_locations_info).then(function(response) {
                               $scope.isAddCountryInProgress = false;
                               $scope.modalInstance.close();                                    
                               
                               $scope.$apply();
                               $state.reload();

                            },function(error){

                                $scope.isAddCountryInProgress = false;
                            });

                        },function(error){

                            $scope.isAddCountryInProgress = false;
                        });                   
                   
                }
            }
         }catch(e){           
        }
    }

    $scope.deleteCountry = function(country){

        try{

                var country= country;
             
                $scope.countryToDelete={};
                $scope.savedLocationForTam.forEach(function(o){        
                if (o.country_name == country)                 
                    $scope.countryToDelete = o;                   
                });

                $rootScope.session.getClientObjectType("saved_locations").then(function(clientObjectType){
                  
                    clientObjectType.get($scope.countryToDelete.location_id).then(function(response) {
                        
                       response.remove().then(function(response){
                        
                            alert('Location removed successfully');
                            $scope.$apply();
                            $state.reload();
                       },function(error){
                       });

                    },function(error){
                    });

                },function(error){
                });

         }catch(e){           
        }

    }



   }catch(e){           
    }
});