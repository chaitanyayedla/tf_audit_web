/**
 * @controller: manageLocationController
 * @description This module includes adding countries under logged in manager and deleting the countries  
 */
angular = require('angular');

angular.module('dealerAudit').controller('manageLocationController', function ($scope,$rootScope,$state,$uibModal,$filter,dealerAudit_ConstantsConst) {

try{

  
   /**
    * setting up longitude,longitude to show europe map
    */ 
    var mapOptions = {
        zoom: 3,
        center: new google.maps.LatLng(54.5260, 15.2551),
        mapTypeId: google.maps.MapTypeId.TERRAIN
    }

    $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
    $scope.countryForTam = [];
    $scope.markers = [];
    
    var infoWindow = new google.maps.InfoWindow();

    $scope.selectedCountry='';
    
    /**
     * @function createMarker
     * @description function to create the marker on the map after adding country under manager.
     * @params country geo information 
     */ 
   $scope.createMarker = function (info){
     
     try{
            
            var marker = new google.maps.Marker({
                map: $scope.map,
                position: new google.maps.LatLng(info.latitude, info.longitude),
                title: info.country_name
            });
            marker.content = '<div class="infoWindowContent">' + info.country_name + '</div>';
            
            google.maps.event.addListener(marker, 'click', function(){
                infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
                infoWindow.open($scope.map, marker);
            });
            
            $scope.markers.push(marker);
        } catch(error){            
       
       
    }   
    }  
    
    $scope.saveFlag = false;
    
    $rootScope.shouldShowTyreImg = false;
    /**
     * @function openModel
     * @description function to open confirmation popup when deleting a country.
     * @params country  
     */ 
    $scope.openModel = function(value) {
            $scope.modalInstance = $uibModal.open({
        
            animation: true,
            templateUrl: '/partials/alertpopup.html',
            controller: 'modelController',
            size: 'sm',
            scope: $scope,
            // Prevent closing by clicking outside or escape button.
            backdrop: 'static',
            keyboard: false
          });
      
          $scope.modalInstance.modalValue = value;
    };

    $scope.open = function(value) {
            $scope.modalInstance = $uibModal.open({
        
                animation: true,
                templateUrl: '/partials/confirmationmodal.html',
                controller: 'manageLocationController',
                size: 'sm',
                scope: $scope,
                // Prevent closing by clicking outside or escape button.
                backdrop: 'static',
                keyboard: false
            });
            
            $scope.modalInstance.modalValue = value;
        };

        $scope.ok = function() {

            if($scope.modalInstance.modalValue == "deleteCountryHandle"){
                    $scope.modalInstance.dismiss('cancel');                               
                   $scope.deleteCountry();
                   $scope.deleteTemplate();

             }else if($scope.modalInstance.modalValue == "deleteCountry"){
                    $scope.modalInstance.dismiss('cancel');                               
                   $scope.deleteCountry();
                  // $scope.deleteTemplate();
             }

        }
        $scope.cancel = function() {
            if($scope.modalInstance.modalValue == "deleteCountryHandle"){
                $scope.modalInstance.dismiss('cancel');                               
                   
             }
             if($scope.modalInstance.modalValue == "deleteCountry"){
                $scope.modalInstance.dismiss('cancel');                               
                   
             }
        }   

    $scope.openInfoWindow = function(e, selectedMarker){
       
        google.maps.event.trigger(selectedMarker, 'click');
    }

    function getHeaderId() {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)

                }
                  return s4() + s4();
    }

    /**
     * @function addCOuntryModel
     * @description function opens a bootstrap modal for selecting country and adding under manager.     
     */
      $scope.addCOuntryModel = function (value) {
            
               $scope.modalInstance = $uibModal.open({
               templateUrl: '/partials/homepopup.html',
               backdrop: 'static',
               windowClass: 'modal',
               size: 'lg',
               controller:'modelController',
               scope: $scope,         
            keyboard: false
            });

             $scope.modalInstance.modalValue = value;
        };  
    /**
     * @function getCountryForTam
     * @description function to list the countries present for manager    
     */ 
    function getCountryForTam(arr, value) {
    
    try{

      var result = [];
      var val=value;
      arr.forEach(function(o){        
        if (o.tam_name == val) 
            result.push(o);            
      } );
      return result? result : null; // or undefined

    } catch(error){   
        
    }   
    }

    $scope.countryList =[];
   
    $scope.initLoad= function(){
        ISession.getInstance().then(function(session) {

             $rootScope.session = session;
             $scope.getLocations();            
             $scope.getSaved_locationForTam();
        }, function(error) {   

            $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
            $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
            $scope.openModel("connectionError");        
               
        });  
    } 

   /**
     * @function getLocations
     * @description function to list the all countries present for manager
    
     */ 
    $scope.getLocations = function(){

        try{        $rootScope.shouldShowTyreImg = true; 
                    
                    //getting the list of countries to show in dropdown               
                    $rootScope.session.getClientObjectType("Country").then(function(clientObjectType){
                        clientObjectType.list(0,100000,null,true).then(function(response) {
                           
                            $scope.tempCountryList = [];
                            $scope.countryList =[];
                            for(i=0 ; i< response.length ; i ++){
                                $scope.tempCountryList.push(response[i].getData());
                                
                            }
                            $scope.countryList = $scope.tempCountryList; 
                                              
                            $scope.$apply();

                           $rootScope.session.getClientObjectType("saved_locations").then(function(clientObjectType){
                                                    
                                clientObjectType.list(0,100000,null,true).then(function(response) {
                                                    
                                                    $scope.tempSavedLocList = [];
                                                    $scope.savedLocationList =[];
                                                    for(i=0 ; i< response.length ; i ++){
                                                        $scope.tempSavedLocList.push(response[i].getData());
                                                        
                                                    }
                                                    $scope.savedLocationList = $scope.tempSavedLocList; 

                                                   // $scope.countryForTam = getCountryForTam($scope.savedLocationList,$rootScope.UserName);
                                                        $scope.savedLocationForTam=[];
                                                        $scope.countryForTam=[];

                                                        //get the saved location of perticular tam
                                                         $scope.markers = [];
                                                        $scope.savedLocationForTam = getCountryForTam($scope.savedLocationList,$rootScope.UserName);
                                                        for (i = 0; i < $scope.savedLocationForTam.length; i++){
                                                            $scope.createMarker($scope.savedLocationForTam[i]);
                                                        }                                                      
                                                        $rootScope.shouldShowTyreImg = false;
                                                        $scope.$apply();                                                      

                                                },function(error){
                                                   $rootScope.shouldShowTyreImg = false;
                                                   $scope.$apply();
                                                   
                                                });

                            },function(error){
                                $rootScope.shouldShowTyreImg = false;
                                $scope.$apply();
                               
                            });

                            
                        },function(error){
                            $rootScope.shouldShowTyreImg = false;
                            $scope.$apply();
                                                                             
                        });

                    },function(error){
                      $rootScope.shouldShowTyreImg = false;
                      $scope.$apply();                      
                      
                    }); 
                                   
           } catch(error){  
            $rootScope.shouldShowTyreImg = false; 
           
        } 
    }

   /**
     * @function addLocationClicked
     * @description function add the country under manager    
     */
    $scope.addLocationClicked = function(){

        try{
                if(!$scope.saveFlag){
                $rootScope.shouldShowTyreImg = true;
                $scope.saveFlag = true;
                var countryPresentFlag = false;
               

                        if($scope.selectedCountry.country_name){
                            $rootScope.session.getClientObjectType("saved_locations").then(function(clientObjectType){

                                clientObjectType.list(0,100000,null,true).then(function(response) {
                                    
                                    var tempSavedLocation = [];
                                    var countOfsavedLocForTam = [];
                                    for(i=0;i<response.length;i++){
                                        tempSavedLocation.push(response[i].data);                                       
                                    }
                                    //validating for more than 3 countries for manager
                                    tempSavedLocation.forEach(function(o){                                               
                                        if (o.tam_name == $rootScope.UserName){
                                            countOfsavedLocForTam.push(o);  
                                        }                                                                                      
                                    });
                                    if(countOfsavedLocForTam.length >= 3){
                                            //alert('already 3 languages are present for manager');
                                            $rootScope.shouldShowTyreImg = false;                                             
                                            $scope.saveFlag = false;
                                            $scope.selectedCountry='';
                                            $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                            $scope.modalContent = dealerAudit_ConstantsConst.countiesExceedLimitErrorMsg;
                                            $scope.openModel("alertHandle");
                                     }else{
                                            //validation for same country
                                           countOfsavedLocForTam.forEach(function(o){
                                                 if (o.country_name == $scope.selectedCountry.country_name){
                                                       // alert('Selected country is already present for manager, Please select another country');
                                                        $rootScope.shouldShowTyreImg = false;
                                                        $scope.saveFlag = false;
                                                        $scope.selectedCountry='';
                                                        $scope.modalTitle =dealerAudit_ConstantsConst.errorModalTitle;
                                                        $scope.modalContent = dealerAudit_ConstantsConst.duplicateCountryErrorMsg;
                                                        $scope.openModel("alertHandle");
                                                        countryPresentFlag = true;
                                                    }
                                           });
                                           if(!countryPresentFlag){                                                
                                                $scope.addLocationForManager();
                                           }                                            
                                     }   
                                },function(error){                                   
                                    $scope.addLocationForManager();
                                   
                                    if(error.code == 55 && error.message == "No Content"){
                             
                                      
                                    }                                    
                                });

                            },function(error){
                                $rootScope.shouldShowTyreImg = false;
                                $scope.saveFlag = false;
                                $scope.$apply();
                                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                $scope.openModel("connectionError"); 
                                
                            }); 

                         }else{
                                $scope.saveFlag = false;
                                $rootScope.shouldShowTyreImg = false;
                                $scope.modalTitle =dealerAudit_ConstantsConst.errorModalTitle;
                                $scope.modalContent = dealerAudit_ConstantsConst.noCountrySelectedErrorMs;
                                $scope.openModel("alertHandle");
                         }

                }  

            }catch(e){
                $scope.saveFlag = false;
                $rootScope.shouldShowTyreImg = false;
               
            }     
    };    
    

     /**
     * @function addLocationForManager
     * @description function add the country under manager    
     */
    $scope.addLocationForManager = function(){

      try{     
        
                $rootScope.shouldShowTyreImg = true;
                
                var locaId=getHeaderId();

                $scope.saved_locations_info={
                    country_id: $scope.selectedCountry.country_id,
                    country_name : $scope.selectedCountry.country_name,
                    location_id : locaId,
                    tam_name : $rootScope.UserName,
                    longitude: $scope.selectedCountry.longitude,
                    latitude: $scope.selectedCountry.latitude
                };
               
                $rootScope.session.getClientObjectType("saved_locations").then(function(clientObjectType){

                    clientObjectType.create($scope.saved_locations_info).then(function(response) {
                       
                        $scope.saveFlag = false;
                       
                        $scope.countryForTam.push($scope.selectedCountry);
                       
                        for (i = 0; i < $scope.countryForTam.length; i++){
                            $scope.createMarker($scope.countryForTam[i]);
                        }
                        $rootScope.shouldShowTyreImg = false;
                       $scope.$apply();
                       $state.reload();

                    },function(error){

                         $rootScope.shouldShowTyreImg = false;
                         $scope.$apply();
                        
                    });

                },function(error){
                    $rootScope.shouldShowTyreImg = false;
                    $scope.$apply();
                    $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                    $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                    $scope.openModel("connectionError"); 
                   
                });  
       
         }catch(e){  
            $rootScope.shouldShowTyreImg = false;         
          
        }                    
           
    }

     /**
     * @function deleteCountry
     * @description function delete the country under manager    
     */
    $scope.deleteCountry = function(){

        try{
       
        $scope.countryToDelete={};
        $scope.savedLocationForTam.forEach(function(o){        
        if (o.country_name ==  $scope.country){            
            $scope.countryToDelete = o;
           
                        $rootScope.session.getClientObjectType("saved_locations").then(function(clientObjectType){                               
                                clientObjectType.get($scope.countryToDelete.location_id).then(function(response) {
                                    
                                   response.remove().then(function(response){
                                     

                                        for(i = 0; i< $scope.markers.length ; i ++){
                                            if($scope.markers[i].location_id == $scope.countryToDelete.location_id){
                                                //$scope.refresh();
                                                $scope.markers.splice(i,1);                                           
                                                $scope.$apply();
                                            }
                                        }
                                        $scope.$apply();
                                        $state.reload();
                                   },function(error){
                                       
                                   });

                                },function(error){
                                    $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                    $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                    $scope.openModel("connectionError"); 
                                    
                                });

                            },function(error){
                                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                $scope.openModel("connectionError"); 
                                
                            });
            }
        });

        }catch(e){           
            
        }  

    }

    $scope.tempSavedLocationForTam = [];
    var locationPresentForTam= false;
    /**
     * @function getSaved_locationForTam
     * @description function get the saved countries under manager    
     */
    $scope.getSaved_locationForTam = function(){

        try{
 
                     $rootScope.session.getClientObjectType("saved_locations").then(function(clientObjectType){
                                
                                clientObjectType.list(0,100000,null,true).then(function(response) {
                                   
                                    for(i=0;i<response.length;i++){
                                        $scope.tempSavedLocationForTam.push(response[i].data);
                                    }
                                    
                                    $scope.tempSavedLocationForTam.forEach(function(o){
                                        if(o.tam_name == $rootScope.UserName){
                                            locationPresentForTam = true;
                                        }
                                    }); 
                                    if(!locationPresentForTam){
                                        $scope.addCOuntryModel();
                                    }
                                },function(error){
                                                                
                                });

                            },function(error){

                                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                $scope.openModel("connectionError"); 
                                
                            });

         }catch(e){           
            
        } 
    };

      /**
     * @function deleteCountrywithTemplate
     * @description function to find if templates present under the countries which user wish to delete    
     */

    $scope.deleteCountrywithTemplate= function(country){
      
        try{

        $scope.templateUnderCountry=[];       
        $scope.country= country;
       
            $rootScope.session.getClientObjectType('template_master').then(function(clientObjectType){

                var filterParams = {
                      "tam_name": $rootScope.UserName,
                      "country_name": $scope.country                  
                      };
                clientObjectType.search("template_master", 0, 10000, filterParams, null, null, null, null, null, true).then(function(response) {
                    if(response.length>0){
                       
                        for(i=0 ; i< response.length ; i ++){
                              $scope.templateUnderCountry.push(response[i].getData());                                                                 
                          }

                        $scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
                         $scope.modalContent = dealerAudit_ConstantsConst.deleteCountryWithTemplatesMsg;
                         $scope.open("deleteCountryHandle");                        
                    }else{
                        $scope.modalTitle =dealerAudit_ConstantsConst.confirmationModelTitle;
                         $scope.modalContent =dealerAudit_ConstantsConst.deleteCountryWithoutTemplatesMsg;
                         $scope.open("deleteCountry");
                    }

                },function(error){

                     $scope.modalTitle =dealerAudit_ConstantsConst.confirmationModelTitle;
                     $scope.modalContent =dealerAudit_ConstantsConst.deleteCountryWithoutTemplatesMsg;
                     $scope.open("deleteCountry");                   
                });

            },function(error){ 
                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                $scope.openModel("connectionError");  
                
            });

        }catch(e){           
           
        } 
      
    }

      /**
     * @function deleteTemplate
     * @description function delete the templates under the countries which user wish to delete    
     */
    $scope.deleteTemplate = function(){
        try{
           
         

            $rootScope.session.getClientObjectType('template_master').then(function(clientObjectType){             



                    for(i=0;i<$scope.templateUnderCountry.length;i++){ 
                        
                        (function(ind) { setTimeout(function(){                               
                                clientObjectType.get($scope.templateUnderCountry[ind].template_id).then(function(responseObject) {   
                                    
                                    var template_master_info={ 
                                                status:'Deleted',  
                                                template_id:$scope.templateUnderCountry[ind].template_id
                                    }

                                    responseObject.update(template_master_info).then(function(response){
                                               
                                                if($scope.templateUnderCountry.length == ind+1){

                                                    var filterParams = {
                                                      "tam_name": $rootScope.UserName                    
                                                    };
                                                    //after removing templates just calling search method so that cache updates with fresh data
                                                    clientObjectType.search("template_master", 0, 10000, filterParams, null, null, null, null, null, true).then(function(response) {
                                                        $rootScope.templateListItems =[];
                                                        for(j=0;j<response.length;j++){
                                                            $rootScope.templateListItems.push(response[j].data);
                                                        }
                                                        $rootScope.templateListItems = $filter('orderBy')($rootScope.templateListItems, 'creation_date', true);
                                                    });
                                                }

                                    },function(error){
                                       
                                    })
                                },function(error){
                                    
                                });
                        }, 1000 * (ind) ); })(i); 

                    };
                    
                    
            },function(error){  

                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                $scope.openModel("connectionError"); 
               
            });
        }catch(e){           
           
        } 
      
     
    };
     /**
     * @function saveMultipleCountries
     * @description function save the saveMultipleCountries selected from dropdown    
     */
    $scope.saveMultipleCountries = function(){

        try{      
            $rootScope.session.getClientObjectType('template_master').then(function(clientObjectType){

            $scope.templateUnderCountry.forEach(function(o){                 
                clientObjectType.get(o.template_id).then(function(response) {                    
                    response.remove().then(function(response){


                    },function(error){
                       
                    })
                },function(error){
                    
                });

            });

            },function(error){  

                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                $scope.openModel("connectionError"); 
                
            });

         }catch(e){           
            
        }
      
    };

    } catch(e) {        
       
    }
    
});



