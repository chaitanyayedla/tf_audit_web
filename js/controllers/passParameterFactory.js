angular = require('angular');

angular.module('dealerAudit').factory('passParameterFactory', function() {
	try{

		var navigateToLocation = "";
		//save the crn number
		function setNavigateToLocation(location) {
			navigateToLocation = location;
		}

		//fetch the saved crn number
		function getNavigateToLocation() {
			return navigateToLocation;
		}


		return{
			setNavigateToLocation : setNavigateToLocation,
			getNavigateToLocation : getNavigateToLocation,
		};

	}catch(e){
		
	}
});