/**
 * @controller: questionController
 * @description This module includes functionalities creating and deletion of templates,creation and deletion and edition of questions, question view, template view.
 */
angular = require('angular');

angular.module('dealerAudit').controller('questionController',function($scope,$state,$stateParams,$rootScope,$location,$window,$uibModal,$timeout,$filter,dealerAudit_ConstantsConst) {

try{
        // declaring local function with scope variables so that they can be accessible from UI
       $scope.templateList = templateList ;
       $scope.createTemplate = createTemplate ;
     
      
      if($rootScope.shouldShowTyreImg == null || typeof($rootScope.shouldShowTyreImg) == "undefined"){
        $rootScope.shouldShowTyreImg = false;
        $rootScope.questStatus = false;

      }
      if(typeof($rootScope.questionInformation) == 'undefined' || $rootScope.questionInformation == null){

       
        // Question information
        $rootScope.questionInformation = {
              "newHeader":"",
              "selectedHeader" : "",
              "newSubHeader":"",           
              "selectedSubHeader" :"",           
              "seletedQestTyp" : "",           
              "mandatoryField" : false,  
              "questionEntered": ""
        };        
        $rootScope.minMaxValues={
          "min":"",
          "max":""
        };

      }
      if(typeof($rootScope.templateInfo) == 'undefined' || $rootScope.templateInfo == null){
         // Template information
        $rootScope.templateInfo = {
          "selectedCountry":"",
          "selectedLanguage":"",
          "titelTemplate":""
        }; 

      }
      if(typeof($scope.countrySelected) == 'undefined' || $scope.countrySelected == null || $scope.countrySelected==undefined){

          $scope.countrySelected={
             'selected':false
          }
      }
      if($rootScope.spanlist == null || typeof($rootScope.spanlist) == "undefined"){
        $rootScope.spanlist = true;
      }
      if($rootScope.updateTemplate == null || typeof($rootScope.updateTemplate) == "undefined"){
        $rootScope.updateTemplate = false;
        
      } 
      if($rootScope.disablePublishBtn == null || typeof($rootScope.disablePublishBtn)=='undefined'){
        $rootScope.disablePublishBtn = false;
      }
      if($rootScope.disableSaveBtn == null || typeof($rootScope.disableSaveBtn)=='undefined'){
        $rootScope.disableSaveBtn = false;
      }
      if( $rootScope.templateListItems == null || typeof( $rootScope.templateListItems)== 'undefined'){
         $rootScope.templateListItems=[];
      }
      if($rootScope.templateInfoChanged == null || typeof($rootScope.templateInfoChanged)=='undefined'){
          $rootScope.templateInfoChanged= false;
      }
      if($rootScope.questionInfoChanged == null || typeof($rootScope.questionInfoChanged)=='undefined'){
          $rootScope.questionInfoChanged= false;
      }
      if($rootScope.questionupdateInfoChanged ==null|| typeof($rootScope.questionupdateInfoChanged)=='undefined'){
        $rootScope.questionupdateInfoChanged=false;
      }
      if($rootScope.questionupdateInfoChangedMinMax ==null|| typeof($rootScope.questionupdateInfoChangedMinMax)=='undefined'){
        $rootScope.questionupdateInfoChangedMinMax=false;
      }
      if($rootScope.editQuestion==null||typeof($rootScope.editQuestion)=='undefined'){
        $rootScope.editQuestion= false;
      }

      var currentLoc = $location.path();
     
      if(currentLoc == '/templateList'){
        $rootScope.spanlist = true;
      }else if(currentLoc == '/questionnaire'){
        $rootScope.spanlist = false; 
      }
      
        
        var languageExist=false;
        var subHeaderExist= false;
        var HeaderExist= false;
        $scope.langSavedForCountry = [];
        
             
       // $scope.selectedQuestionType ="";
        $scope.countrySelectedFrmDropDown = false;  
        $scope.saveFlag = false;
        $scope.isTemplateNotSaved= true;
        $scope.headerSelected = false;
        $scope.noTemplatesForManager = false;
        $rootScope.deleteTemplateBtnClicked = false;
        $rootScope.deleteQuestBtnClicked = false;

        $rootScope.noHeadersAvailable=false;
        $rootScope.noSubHeadersAvailable = false;
        var groupArray = require('group-array');
        //below flag is used to disable the sections in UI       

     //  $scope.questionTypesList=['Text','Numeric','Y/N']
       if($rootScope.savedtemplateId == null || $rootScope.savedtemplateId ==undefined || $rootScope.savedtemplateId =="undefined"){
        $rootScope.savedtemplateId ="";

       }
       if($rootScope.questionInformation.questionEntered== null ||$rootScope.questionInformation.questionEntered ==undefined || $rootScope.questionInformation.questionEntered=="undefined"){
         $rootScope.questionInformation.questionEntered ="";   

       }
        //$scope.templateInformation={};

        if($scope.templateInformation == null || typeof($scope.templateInformation) =="undefined"){
          $scope.templateInformation={};
        }
        if($scope.reverseSort == null || typeof($scope.reverseSort)== "undefined"){
          $scope.reverseSort = true;
        }
        if($rootScope.templateToUpdate == null || typeof($rootScope.templateToUpdate) == "undefined"){
          $rootScope.templateToUpdate = {
            "status":""
          };
        }
        if($rootScope.hiddens == null || typeof($rootScope.hiddens)=='undefined'){
          $rootScope.hiddens= true;
        }

        if($rootScope.countryLangForTam==null||$rootScope.countryLangForTam==undefined || typeof($rootScope.countryLangForTam)=='undefined'){
          $rootScope.countryLangForTam =[];
        }

        if($rootScope.slectedCountry==null||$rootScope.slectedCountry==undefined || typeof($rootScope.slectedCountry)=='undefined'){
          $rootScope.slectedCountry =[] ;
        }
      //deciding the number of records to be shown per page based on the screen size
       if(screen.width <=1000){
            $scope.numberOfrows =  6;
          }          
          if(screen.width >1000 && screen.width <1500){
            $scope.numberOfrows =  8;
          }
         
          if(screen.width >1500 ){
            $scope.numberOfrows =  11;
          }
      function templateList ()
      {
           $rootScope.spanlist = true;
           // $scope.getTemplateList();
           $state.go('templateList');

            $rootScope.updateTemplate= false;
            $rootScope.savedtemplateId ="";
            $rootScope.templateInfo = {
              "selectedCountry":"",
              "selectedLanguage":"",
              "titelTemplate":""
            }; 

            // Question information
            $rootScope.questionInformation = {
                  "newHeader":"",
                  "selectedHeader" : "",
                  "newSubHeader":"",           
                  "selectedSubHeader" :"",           
                  "seletedQestTyp" : "",           
                  "mandatoryField" : false,  
                  "questionEntered": ""
            };
            
            $rootScope.minMaxValues={
              "min":"",
              "max":""
            };
      };

      function createTemplate ()
      {

          if($rootScope.templateInfoChanged || $rootScope.questionInfoChanged ||$rootScope.questionupdateInfoChanged||$rootScope.questionupdateInfoChangedMinMax){
              $scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
              $scope.modalContent = dealerAudit_ConstantsConst.discardChangesMsg;
              $scope.open("overrideCreatetemplate");   

          }else{

              //$scope.getCountryLanguage(true);
              $rootScope.spanlist = false;
              $rootScope.updateTemplate = false;
              $rootScope.questStatus = false;
              $rootScope.disablePublishBtn = false;
              $rootScope.disableSaveBtn = false;
              $rootScope.savedtemplateId ="";
              $rootScope.templateId="";
              $rootScope.templateInfo = {
                "selectedCountry":"",
                "selectedLanguage":"",
                "titelTemplate":""
              }; 

              // Question information
              $rootScope.questionInformation = {
                    "newHeader":"",
                    "selectedHeader" : "",
                    "newSubHeader":"",           
                    "selectedSubHeader" :"",           
                    "seletedQestTyp" : "",           
                    "mandatoryField" : false,  
                    "questionEntered": ""
              };
                  
              $rootScope.minMaxValues={
                "min":"",
                "max":""
              };

              $rootScope.templateToUpdate = {
                "status":""
              };
              $rootScope.headersOfQuestions=[];
              $rootScope.subheaderOfQuestion = [];
              $rootScope.questionsOfTemplate=[];
              $rootScope.questionOptions=[];  
              $rootScope.groupedQuestionsArray=[];  
              localStorage.removeItem('templateInfo');
              localStorage.removeItem('savedtemplateId');
              localStorage.removeItem('templateToUpdate');
              localStorage.removeItem('updateTemplate');     
              
              if($location.path()!='/questionnaire'){
                $state.go('questionnaire');
              }else{
                $window.location.reload();
              }
              

             //$scope.getCountryLanguage(true);

          }

          

      }; 

      /**
     * @function signOut
     * @description function for logout.     
     */
  $scope.signOut = function() {
      try {
        
          $scope.navigateToOtherTab('login');
      } catch(e) {
      }
    }

      $scope.counter = 1;
      $scope.resetCounter = function(){
        return $scope.counter = 1;
      }

      $scope.incrementCounter = function() {
         return $scope.counter++;
      }

    /**
     * @function arrUnique
     * @description function make the array containing unique objects
     * @param {Array }  - holds the data related
     */
      function arrUnique(arr) {

        try{

          var cleaned = [];
          arr.forEach(function(itm) {
              var unique = true;
              cleaned.forEach(function(itm2) {
                  if (_.isEqual(itm, itm2)) unique = false;
              });
              if (unique)  cleaned.push(itm);
          });
          return cleaned;

           }catch(e){           
          
          }
      }
    /**
     * @function openModel
     * @description function to open the bootstrap alert window popup  
     */
        $scope.openModel = function(value) {
            $scope.modalInstance = $uibModal.open({
        
            animation: true,
            templateUrl: '/partials/alertpopup.html',
            controller: 'modelController',
            size: 'sm',
            scope: $scope,
            // Prevent closing by clicking outside or escape button.
            backdrop: 'static',
            keyboard: false
          });
      
          $scope.modalInstance.modalValue = value;
        };

    /**
     * @function open
     * @description function to open the bootstrap confirmation popup model 
     */
        $scope.open = function(value) {
            $scope.modalInstance = $uibModal.open({
        
                animation: true,
                templateUrl: '/partials/confirmationmodal.html',
                controller: 'questionController',
                size: 'sm',
                scope: $scope,
                // Prevent closing by clicking outside or escape button.
                backdrop: 'static',
                keyboard: false
            });
            
            $scope.modalInstance.modalValue = value;
        };

    /**
     * @function ok
     * @description function to perform actions when user ok  button on confirmation popup
     */
        $scope.ok = function() {

            if($scope.modalInstance.modalValue == "navigateToOtherTab"){
                    if($scope.navigateToLocation == 'login'){
                      $rootScope.logoutFlag = true;       
                      localStorage.removeItem('sessionObj');
                    }
                    $state.go($scope.navigateToLocation);

                     $rootScope.templateInfoChanged= false;
                     $rootScope.questionInfoChanged= false; 
                     $rootScope.editQuestion=false;
                     $rootScope.questionupdateInfoChanged=false;
                     $rootScope.questionupdateInfoChangedMinMax=false;
                     localStorage.setItem('templateInfoChanged',false);
                     localStorage.setItem('questionInfoChanged',false);     
                     localStorage.setItem('questionupdateInfoChanged',false); 
                       localStorage.setItem('questionupdateInfoChangedMinMax',false);                   
                    $scope.modalInstance.dismiss('cancel');                            
                   

             }if($scope.modalInstance.modalValue == "navigateToTemplateList"){
                    
                     templateList();
                     $rootScope.modelOpened = false;
                     $rootScope.templateInfoChanged= false;
                     $rootScope.questionInfoChanged= false; 
                     $rootScope.editQuestion=false;
                     $rootScope.questionupdateInfoChanged=false;
                     $rootScope.questionupdateInfoChangedMinMax=false;
                     localStorage.setItem('templateInfoChanged',false);
                     localStorage.setItem('questionInfoChanged',false); 
                     localStorage.setItem('questionupdateInfoChanged',false); 
                       localStorage.setItem('questionupdateInfoChangedMinMax',false);                    
                    $scope.modalInstance.dismiss('cancel');                            
                   

             }if($scope.modalInstance.modalValue == "deleteTemplateHandle"){
                    $rootScope.deleteTemplateBtnClicked = false;
                    $scope.deleteTemplateData($scope.templateToDelete);
                    $scope.modalInstance.dismiss('cancel');   
             }   
             if($scope.modalInstance.modalValue == "deleteQuestHandle"){
                    $rootScope.deleteQuestBtnClicked = false;
                    $scope.deleteQuestion($scope.questionToDelete);
                    $scope.modalInstance.dismiss('cancel');   
             }
             if($scope.modalInstance.modalValue == "overrideCreatetemplate"){
                    $rootScope.templateInfoChanged= false;
                    $rootScope.questionInfoChanged= false; 
                    $rootScope.questionupdateInfoChanged=false;
                     $rootScope.questionupdateInfoChangedMinMax=false;
                    $rootScope.editQuestion=false;
                    localStorage.setItem('templateInfoChanged',false);
                    localStorage.setItem('questionInfoChanged',false);
                    localStorage.setItem('questionupdateInfoChanged',false); 
                     localStorage.setItem('questionupdateInfoChangedMinMax',false);    
                    createTemplate();
                    $scope.modalInstance.dismiss('cancel');   
             }            
             else
             {    
                    $scope.modalInstance.dismiss('cancel');                               
                  
             }

        };

    /**
     * @function cancel
     * @description function to perform actions when user click cancel button on confirmation popup
     */
        $scope.cancel = function() {
                
             if($scope.modalInstance.modalValue == "deleteTemplateHandle"){
                    $rootScope.deleteTemplateBtnClicked = false;                   
                    $scope.modalInstance.dismiss('cancel');   
             } 
              if($scope.modalInstance.modalValue == "deleteQuestHandle"){
                    $rootScope.deleteQuestBtnClicked = false;
                    $scope.modalInstance.dismiss('cancel');   
             } 
               $rootScope.modelOpened = false;
                $scope.modalInstance.dismiss('cancel');                        
              
        };  

        $scope.optionList = [ "1", "2"];

        $scope.optionsArray={};

        
      /**
     * @function addOption
     * @description function to add field for entering more options for checkbox type of questions  
     */
        $scope.addOption=function(){
          $scope.optionList.push($scope.optionList.length+1);
        }

      /**
     * @function removeOption
     * @description function to remove field for entering less options for checkbox type of questions  
     */
        $scope.removeOption=function(option){                 
          try{
                for(i=0;i<$scope.optionList.length;i++){
                  
                  if($scope.optionList[i]==option){             
                    $scope.optionList.splice(i, 1);          
                    delete $scope.optionsArray[i+1];            
                        var tempArray=[];
                        var tempJson={};
                        
                        try{ 
                            for(i=0;i<$scope.optionList.length;i++){
                                tempArray.push(i+1);
                            }
                            var counter=0;
                            for(i in $scope.optionsArray){                 
                                counter++;
                                tempJson[counter]=$scope.optionsArray[i];

                            }

                            $scope.optionList = [];
                            $scope.optionsArray={};
                            $scope.optionList=tempArray;
                            $scope.optionsArray=tempJson;
                        }catch(e){
                        }
                        
                  }
                }

              }catch(e){
              }
        };

        $scope.radioOptionList = [ "1", "2"];

        $scope.radioOptionsArray={};

    /**
     * @function addRadioOption
     * @description function to add field for entering more options for multiple type of questions  
     */
        $scope.addRadioOption=function(){
          $scope.radioOptionList.push($scope.radioOptionList.length+1);        
        }
    /**
     * @function addRadioOption
     * @description function to remove field for entering less options for multiple type of questions  
     */
        $scope.removeRadioOption=function(option){
         
          try{

                for(i=0;i<$scope.radioOptionList.length;i++){
                
                if($scope.radioOptionList[i]==option){
                 
                  $scope.radioOptionList.splice(i, 1);          
                  delete $scope.radioOptionsArray[i+1];              
                      var tempArray=[];
                      var tempJson={};
                      
                      try{ 
                          for(i=0;i<$scope.radioOptionList.length;i++){
                              tempArray.push(i+1);
                          }
                          var counter=0;
                          for(i in $scope.radioOptionsArray){                 
                              counter++;
                              tempJson[counter]=$scope.radioOptionsArray[i];

                          }

                          $scope.radioOptionList = [];
                          $scope.radioOptionsArray={};
                          $scope.radioOptionList=tempArray;
                          $scope.radioOptionsArray=tempJson;
                      }catch(e){
                      }

                }
              }
            }catch(e){
            }
        };
        
     /**
     * @function sort
     * @description function to sort the list data based on perticular field/column
     */
        $scope.sort = function(sortBy){
           
            $scope.columnToOrder = sortBy; 
            $scope.reverseSort = !$scope.reverseSort;  

            $rootScope.templateListItems = $filter('orderBy')($rootScope.templateListItems, $scope.columnToOrder, $scope.reverseSort); 

        };
       

        $scope.tab = 1;

        $scope.setTab = function(newTab){
                  $scope.tab = newTab;
                  $scope.clearFieldsRelatedtoQuestion();
        };

        $scope.isSet = function(tabNum){
          return $scope.tab === tabNum;
        };

        $scope.questionType = function(id){
          
            $scope.setId = "";
            $scope.setId = $rootScope.questionInformation.seletedQestTyp;
            
        };

        function getHeaderId() {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)

                }
                  return s4() + s4();
         };

          $scope.clearFieldsRelatedtoQuestion = function(){

          $scope.setId="";
          $rootScope.questionInformation.newHeader="";
          $rootScope.questionInformation.newSubHeader="";
          $rootScope.questionInformation.selectedHeader="";
          $rootScope.questionInformation.selectedSubHeader="";
          $rootScope.questionInformation.seletedQestTyp="";
          $rootScope.questionInformation.mandatoryField=false;
          $rootScope.questionInformation.questionEntered="";         
           
         };
        
    /**
     * @function init
     * @description function to load the data on page load
     */
        $scope.init=function(){
         try{
          $rootScope.shouldShowTyreImg = true;
          ISession.getInstance().then(function(session) {
          
          $rootScope.session = session;
              $rootScope.shouldShowTyreImg = false;         
              $rootScope.headersOfQuestions=[];
              $rootScope.subheaderOfQuestion = [];
              $rootScope.questionsOfTemplate=[];
              $rootScope.questionOptions=[];  
              $rootScope.groupedQuestionsArray=[]; 

                if($rootScope.updateTemplate){    
                  $rootScope.spanlist = false;
                  $rootScope.questStatus = true; 
                  $rootScope.disableSaveBtn = true;
                  $scope.listData(true);                           
                  $scope.getQuestionUnderTemplate(true);  
                  //$scope.getCountryLanguage(true);        

                 }

            }, function(error) {        
            });

           }catch(e){
          }
      };
      /**
     * @function saveHeader
     * @description function to save the header information
     */
        
        $scope.saveHeader= function(){
          try{
            
            if($rootScope.templateToUpdate.status!= 'Published'){
          
                  if(!$scope.saveFlag){
                    $scope.saveFlag = true;               
                  
                    if($rootScope.questionInformation.newHeader==''){
                        $scope.saveFlag = false;                       
                        $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                        $scope.modalContent = dealerAudit_ConstantsConst.enterHeaderTextMsg;
                        $scope.openModel("alertHandle");
                    }else{

                  
                    if(typeof($rootScope.headerList)!="undefined"){
                      $rootScope.headerList.forEach(function(o){ 
                                                         
                              if(o.header_name.toLowerCase() == $rootScope.questionInformation.newHeader.toLowerCase()){
                                  HeaderExist = true;
                                  $scope.saveFlag = false;                               ;   
                                  return false;                
                              }                                                 
                        });
                    }
                    if(!HeaderExist){

                    var headerId = getHeaderId();
                    $scope.headerInfo = {
                        header_id : headerId,
                        header_name : $rootScope.questionInformation.newHeader,     
                        language : $rootScope.templateInfo.selectedLanguage        
                    }

                                   
                    $rootScope.session.getClientObjectType("header").then(function(clientObjectType){
                                        clientObjectType.create($scope.headerInfo).then(function(response) {
                                           
                                            $scope.saveFlag = false;                                            
                                            $scope.clearFieldsRelatedtoQuestion();                                            
                                            $rootScope.questionInformation.newHeader="";
                                            $scope.listData(true);
                                            $scope.$apply();
                                        },function(error){
                                            $scope.saveFlag = false;                                      
                                            $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                            $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                            $scope.openModel("connectionError");
                                        });

                          },function(error){
                              $scope.saveFlag = false; 
                              $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                              $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                              $scope.openModel("connectionError");                          
                          });           
               
                      }else{
                       
                        HeaderExist= false;
                        $scope.saveFlag = false;
                        $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                        $scope.modalContent = dealerAudit_ConstantsConst.duplicateHeaderMsg;
                        $scope.openModel("alertHandle");
                    }
                    }

                  }
             }else{
                  $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                  $scope.modalContent = dealerAudit_ConstantsConst.unpublishTemplateToAddHeader;
                  $scope.openModel("alertHandle");
             }
        }catch(e){
          $rootScope.shouldShowTyreImg = false;
          HeaderExist = false;
          $scope.saveFlag = false;
          return ;
        }
      }


    /**
     * @function saveSubHeader
     * @description function to sub save the header information
     */

        $scope.saveSubHeader= function(){
          try{
           if($rootScope.templateToUpdate.status!= 'Published'){
           if(!$scope.saveFlag){ 
          
            $scope.saveFlag = true;
            if($rootScope.questionInformation.newSubHeader =='' || $rootScope.questionInformation.selectedHeader==''){
                $scope.saveFlag = false;
             
                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                if($rootScope.questionInformation.newSubHeader =='' && typeof($rootScope.questionInformation.selectedHeader.header_name) != "undefined"){
                  $scope.modalContent =dealerAudit_ConstantsConst.enterSubHeader;
                  $scope.openModel("alertHandle");
                  return ;
                }else if($rootScope.questionInformation.newSubHeader !='' && typeof($rootScope.questionInformation.selectedHeader.header_name) == "undefined"){
                  $scope.modalContent =dealerAudit_ConstantsConst.selectHeaderMsg;
                  $scope.openModel("alertHandle");
                  return ;
                }else{
                  $scope.modalContent =dealerAudit_ConstantsConst.enterDetailsMsg;
                  $scope.openModel("alertHandle");
                  return ;
                } 
                
            }else{

            if(typeof($rootScope.subHeaderList)!="undefined"){
            $scope.subHeadedOfSelectedHeader.forEach(function(o){ 
                   
                                                             
                    if(o.sub_header_name.toLowerCase() == $rootScope.questionInformation.newSubHeader.toLowerCase()){
                        subHeaderExist = true;
                        $scope.saveFlag = false;
                   
                        return false;                
                    }                                                 
              });
          }
            if(!subHeaderExist){

             var subHeaderId = getHeaderId();           
              $scope.subHeaderInfo = {
                sub_header_id : subHeaderId,
                sub_header_name : $rootScope.questionInformation.newSubHeader,             
                header_id   : $rootScope.questionInformation.selectedHeader.header_id
               }

                    $rootScope.session.getClientObjectType("sub_header").then(function(clientObjectType){
                        clientObjectType.create($scope.subHeaderInfo).then(function(response) {
                           
                             $scope.saveFlag = false;                           
                            $scope.clearFieldsRelatedtoQuestion();                           
                            $rootScope.questionInformation.newSubHeader="";                           
                            $scope.listData(true);
                            $scope.$apply();
                        },function(error){
                            $scope.saveFlag = false;  
                            $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                            $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                            $scope.openModel("connectionError");                        
                        });

                    },function(error){
                        $scope.saveFlag = false;  
                        $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                        $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                        $scope.openModel("connectionError");                     
                    });
                }else{
                
                  $scope.saveFlag = false;
                  subHeaderExist = false;
                  $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                  $scope.modalContent = dealerAudit_ConstantsConst.duplicateSubHeaderMsg;
                  $scope.openModel("alertHandle");
                  return false;
            }
            
          }
          }
        }else{
              $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
              $scope.modalContent = dealerAudit_ConstantsConst.unpublishTemplateToAddSubHeader;
              $scope.openModel("alertHandle");
        }
        }catch(e){
          $rootScope.shouldShowTyreImg = false;
          $scope.saveFlag = false;
          subHeaderExist = false;
          return ;
        }
        }   


    /**
     * @function resetFields
     * @description function to reser the fields
     */
        $scope.resetFields = function(){

              $rootScope.questionInformation.selectedHeader ="";
              $rootScope.questionInformation.selectedSubHeader ="";
              $rootScope.questionInformation.seletedQestTyp ="";
              $rootScope.minMaxValues.min="";
              $rootScope.minMaxValues.max="";
             // $scope.optionsArray={};
             // $scope.radioOptionsArray={};
              $rootScope.questionInformation.newHeader="";
              $rootScope.questionInformation.newSubHeader="";
              $rootScope.questionInformation.questionEntered="";
              $rootScope.questionInformation.mandatoryField= false;

              $scope.form.$setPristine();
              $scope.form.$setUntouched();
              //$scope.$apply();
        }
        
        /**
         * @function validateMultipleChoiceOptions
         * @description function to check for duplicate options entered for multiple type or checkbox type questions
         */
        $scope.validateMultipleChoiceOptions = function(){

                
                 if($rootScope.questionInformation.seletedQestTyp == 'Multiple Choice'){
                    
                       var count=0;
                       for(ind in $scope.optionsArray){
                           count=0;
                           var entry = $scope.optionsArray[ind];                          
                           for(i=1;i<=Object.keys($scope.optionsArray).length;i++){     
                         
                               if($scope.optionsArray[i].toLowerCase() == entry.toLowerCase()){
                                  count=count+1;                                  
                               }                               
                           } 
                           if(count>1){                               
                                return false;                                
                           }
                       } 

                       return true;

                 }else if($rootScope.questionInformation.seletedQestTyp == 'CheckBox'){
                    
                       var count=0;
                       for(ind in $scope.radioOptionsArray){
                           count=0;
                           var entry = $scope.radioOptionsArray[ind];                        
                           for(i=1;i<=Object.keys($scope.radioOptionsArray).length;i++){       
                       
                               if($scope.radioOptionsArray[i].toLowerCase() == entry.toLowerCase()){
                                  count=count+1;                                  
                               }                               
                           } 
                           if(count>1){                             
                                return false;                                
                           }
                       } 

                       return true;

                 }else{

                  return true;
                 }

        }
    /**
     * @function saveQuestion
     * @description function to save the question information
     */
        $scope.saveQuestion = function(){
          
         
          try{
           // steps to save the question created
           // 1. Get the number of questions created in table, next number wil be position id of this question.          
           // 2. Save the question by passing all params.
          
           if($rootScope.templateToUpdate.status!= 'Published'){
           if(!$scope.saveFlag){ 
           $scope.saveFlag =true;
          // $rootScope.shouldShowTyreImg = true;            
       
           if(!$scope.validateMultipleChoiceOptions()){
             // $rootScope.shouldShowTyreImg = false; 
              $scope.saveFlag =false;
              $scope.modalTitle =dealerAudit_ConstantsConst.errorModalTitle;
              $scope.modalContent =dealerAudit_ConstantsConst.duplicateOptionsMsg;
              $scope.openModel("alertHandle");
              
              return;
            }
         /**
         * if blocks : handling if any of the mandatory fields are not entered, alerting for only non entered field        
         */
           if($rootScope.questionInformation.questionEntered == ''|| typeof($rootScope.questionInformation.selectedSubHeader.sub_header_name) == "undefined" || typeof($rootScope.questionInformation.selectedHeader.header_name) == "undefined"){
                $scope.saveFlag =false;
               // $rootScope.shouldShowTyreImg = false; 
                $scope.modalTitle =dealerAudit_ConstantsConst.errorModalTitle;
               
                if($rootScope.questionInformation.questionEntered == '' && typeof($rootScope.questionInformation.selectedSubHeader.sub_header_name) != "undefined" && typeof($rootScope.questionInformation.selectedHeader.header_name) != "undefined"){
                  $scope.modalContent = dealerAudit_ConstantsConst.enterQuestionTxtMsg;
                  $scope.openModel("alertHandle");
                   return ;
                }else if($rootScope.questionInformation.questionEntered != '' && typeof($rootScope.questionInformation.selectedSubHeader.sub_header_name) == "undefined" && typeof($rootScope.questionInformation.selectedHeader.header_name) == "undefined"){
                  $scope.modalContent = dealerAudit_ConstantsConst.selectHeaderSubHeaderMsg;
                  $scope.openModel("alertHandle");
                   return ;
                }
                else if($rootScope.questionInformation.questionEntered != '' && typeof($rootScope.questionInformation.selectedSubHeader.sub_header_name) == "undefined" && typeof($rootScope.questionInformation.selectedHeader.header_name) != "undefined"){
                  $scope.modalContent = dealerAudit_ConstantsConst.selectSubHeaderMsg;
                  $scope.openModel("alertHandle");
                   return ;
                }
                else if($rootScope.questionInformation.questionEntered != '' && typeof($rootScope.questionInformation.selectedSubHeader.sub_header_name) != "undefined" && typeof($rootScope.questionInformation.selectedHeader.header_name) == "undefined"){
                  $scope.modalContent = dealerAudit_ConstantsConst.selectHeaderMsg;
                  $scope.openModel("alertHandle");
                    return ;
                }else{
                  $scope.modalContent = dealerAudit_ConstantsConst.enterDetailsMsg;
                  $scope.openModel("alertHandle");
                    return ;
                }
               
           }else{   

            $scope.positionId =0;
            $scope.questionId =0;
            var quesId = getHeaderId();
            $scope.questionId = quesId; 

            $scope.questionInfo={
                header_text : $rootScope.questionInformation.selectedHeader.header_name,
                mandatory_field : $rootScope.questionInformation.mandatoryField,
                maximum_value : $rootScope.minMaxValues.max,
                minimum_value : $rootScope.minMaxValues.min,
                question_id : $scope.questionId,
                question_text : $rootScope.questionInformation.questionEntered,
                question_typ : $rootScope.questionInformation.seletedQestTyp,
                question_version : 1.0,
                status : 'new',
                sub_header_text : $rootScope.questionInformation.selectedSubHeader.sub_header_name,                     
                template_id : $rootScope.savedtemplateId,
                impact:'NOCHANGE'
            }
                                           
        
                  // getting records length of question
              $rootScope.session.getClientObjectType("question").then(function(clientObjectType){
                                        clientObjectType.getCount().then(function(response) {
                                           
                                            var questCount = parseInt(response);
                                            $scope.positionId = questCount+1;                                                                                      
                                            $scope.questionInfo['position_id'] = $scope.positionId;

                                            $rootScope.session.getClientObjectType("question").then(function(clientObjectType){
                                                clientObjectType.create($scope.questionInfo).then(function(response){
                                                          $scope.saveFlag =false;
                                                         
                                                          $rootScope.questionInfoChanged = false;
                                                          localStorage.setItem('questionInfoChanged',false);                                           
                                                          $scope.resetFields();
                                                          $scope.getQuestionUnderTemplate(true);
                                                          $scope.clearFieldsRelatedtoQuestion();                                                      
                          
                                                },function(error){
                                                $scope.saveFlag =false;   
                                                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                                $scope.openModel("connectionError");                                          
                                            });

                                            },function(error){
                                                $scope.saveFlag =false; 
                                                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                                $scope.openModel("connectionError");                                              
                                            });

                                        },function(error){
                                                $scope.saveFlag =true;
                                               // $rootScope.shouldShowTyreImg = true; 
                                                $scope.positionId = 1;
                                                $scope.questionInfo['position_id'] = $scope.positionId;                                             

                                               $rootScope.session.getClientObjectType("question").then(function(clientObjectType){
                                                    clientObjectType.create($scope.questionInfo).then(function(response){
                                                              $scope.saveFlag =false;
                                                             // $rootScope.shouldShowTyreImg = false;                                                         
                                                        
                                                              $scope.resetFields();
                                                              $scope.getQuestionUnderTemplate(true);
                                                              $scope.clearFieldsRelatedtoQuestion();
                                                        
                                                    },function(error){
                                                      $scope.saveFlag =false;
                                                      $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                                      $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                                      $scope.openModel("connectionError");
                                                });  

                                                },function(error){
                                                  $scope.saveFlag =false; 
                                                  $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                                  $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                                  $scope.openModel("connectionError");                                               
                                                });
                                           
                                        });

                                    },function(error){
                                        $scope.saveFlag =false;  
                                        $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                        $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                        $scope.openModel("connectionError");                                      
                                   });                                  
        
                }
            }
            }else{
              $scope.modalTitle =dealerAudit_ConstantsConst.errorModalTitle;
              $scope.modalContent =dealerAudit_ConstantsConst.unpublishTemplateToAddQuest;
              $scope.openModel("alertHandle");
        }
          }catch(e){
            $rootScope.shouldShowTyreImg = false;
            $scope.saveFlag= false;
            return ;
          }
        }

    /**
     * @function saveTemplate
     * @description function to save the template information
     */  
        $scope.saveTemplate= function(){ 

        try{
    
        if(!$scope.saveFlag ){ 
            if($rootScope.templateInfo.titelTemplate!='' && $rootScope.templateInfo.selectedCountry != "" &&$rootScope.templateInfo.selectedCountry != null && $rootScope.templateInfo.selectedLanguage!= "" && $rootScope.templateInfo.selectedLanguage!= null){
            $scope.saveFlag =true;
            languageExist = false;          
            $rootScope.shouldShowTyreImg = true;

            /*
             *  Before saving the template , checking whether tempate already saved for selected country, if not then saving the template
             */
            $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){ 
                     
                     var filterParams = {
                      "tam_name": $rootScope.UserName,    
                      "country_name":$rootScope.templateInfo.selectedCountry                              
                      };
                clientObjectType.search("template_master", 0, 10000, filterParams, null, null, null, null, null, true).then(function(response) {
                                       
                                        
                                        $scope.savedTemplate=[];
                                        for(i=0;i<response.length;i++){
                                            $scope.savedTemplate.push(response[i].data);
                                        }
                                     

                                        for(i=0;i<$scope.savedTemplate.length;i++){
                                          
                                            if($scope.savedTemplate[i].language_name == $rootScope.templateInfo.selectedLanguage && $scope.savedTemplate[i].status !='Deleted'){
                                                    $scope.saveFlag =false;
                                                    languageExist = true;    
                                                    $rootScope.shouldShowTyreImg = false;                                                                      
                                                    $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                                    $scope.modalContent = dealerAudit_ConstantsConst.duplicateTemplateWithLanguage;
                                                    $scope.openModel("alertHandle");
                                                    return ;                                        
                                            }
                                        }
                                          if(!languageExist){

                                                $rootScope.templateId = getHeaderId();
                                                var date = new Date();
                                                date = $filter('date')(new Date(), 'dd-MM-yyyy');
                                               
                                                $scope.template_master_info={           
                                                    country_name:$rootScope.templateInfo.selectedCountry,           
                                                    language_name: $rootScope.templateInfo.selectedLanguage,
                                                    status:'saved',          
                                                    tam_name: $rootScope.UserName,
                                                    template_name: $rootScope.templateInfo.titelTemplate,
                                                    template_id:$rootScope.templateId,
                                                    creation_date:date
                                               }
                                              $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){ 
                                                          clientObjectType.create($scope.template_master_info).then(function(response){
                                                              //alert('Template information saved succesfully');
                                                                $scope.saveFlag =false;
                                                                $rootScope.shouldShowTyreImg = false;                                                                              
                                                                $rootScope.templateInfoChanged= false;

                                                                localStorage.setItem('templateInfoChanged',false);
                                                                $rootScope.templateToUpdate.status= 'saved'; 
                                                                $rootScope.disablePublishBtn = false;
                                                                $rootScope.disableSaveBtn = false;
                                                                $scope.countrySelected.selected=false;
                                                                $rootScope.savedtemplateId = response.data.template_id;

                                                                $scope.modalTitle = dealerAudit_ConstantsConst.successModelTitle;
                                                                $scope.modalContent = dealerAudit_ConstantsConst.templateSavedMsg;
                                                                $scope.openModel("alertHandle");

                                                                $rootScope.questStatus = true;                                                                          
                                                                $scope.getTemplateMasterDataAfterUpdating();                    
                                                                $scope.listData(true); 
                                                               
                                                               //savedtemplateId is required to update template so preserving it even after refresh
                                                               localStorage.setItem('savedtemplateId',$rootScope.savedtemplateId);
                                                               localStorage.setItem('templateToUpdate',JSON.stringify($rootScope.templateToUpdate));
                                                              //template information(country,language,templateName)
                                                               localStorage.setItem('templateInfo',JSON.stringify($rootScope.templateInfo)); 
                                                              //flag indiating controll is in edit mode
                                                               localStorage.setItem('updateTemplate',$rootScope.updateTemplate);  

                                                               // $rootScope.selectedCountryChanged= false;
                                                               // $rootScope.selectedLanguageChanged= false;
                                                             
                                                              $scope.$apply();
                                                          },function(error){
                                                            $scope.saveFlag =false;
                                                            $rootScope.shouldShowTyreImg = false; 
                                                            $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                                            $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                                            $scope.openModel("connectionError"); 
                                                            return false;
                                                          });
                                                  },function(error){
                                                    $scope.saveFlag =false;
                                                    $rootScope.shouldShowTyreImg = false; 
                                                    $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                                    $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                                    $scope.openModel("connectionError");   
                                                    return false;
                                                  });
                                            }                           

                        },function(error){
                                               

                             
                              $scope.saveFlag =true;
                              if($rootScope.templateInfo.titelTemplate=='' || typeof($rootScope.templateInfo.selectedCountry) == "undefined" || typeof($rootScope.templateInfo.selectedLanguage) == "undefined"){
                                    $scope.saveFlag =false;    
                                    $rootScope.shouldShowTyreImg = false;                   
                                    $scope.modalTitle =dealerAudit_ConstantsConst.errorModalTitle;
                                    $scope.modalContent = dealerAudit_ConstantsConst.enterDetailsMsg;
                                    $scope.openModel("alertHandle");                                  
                              }else{                                              

                              
                              $rootScope.templateId = getHeaderId();                              
                              
                              var date = new Date();
                              date = $filter('date')(new Date(), 'dd-MM-yyyy');

                                
                              $scope.template_master_info={           
                                  country_name:$rootScope.templateInfo.selectedCountry,           
                                  language_name: $rootScope.templateInfo.selectedLanguage,
                                  status:'saved',          
                                  tam_name: $rootScope.UserName,
                                  template_name: $rootScope.templateInfo.titelTemplate,
                                  template_id:$rootScope.templateId,
                                  creation_date:date
                              }
                              
                              $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){ 
                                          clientObjectType.create($scope.template_master_info).then(function(response){
                                             // alert('Template information saved succesfully');
                                              $scope.saveFlag =false;  
                                              $rootScope.shouldShowTyreImg = false; 
                                              $rootScope.savedtemplateId = response.data.template_id;
                                              
                                              $rootScope.templateToUpdate.status= 'saved'; 
                                              $rootScope.disablePublishBtn = false;
                                              $rootScope.disableSaveBtn = false;

                                              //savedtemplateId is required to update template so preserving it even after refresh      
                                              localStorage.setItem('savedtemplateId',$rootScope.savedtemplateId);

                                              $scope.modalTitle = dealerAudit_ConstantsConst.successModelTitle;
                                              $scope.modalContent =dealerAudit_ConstantsConst.templateSavedMsg;
                                              $scope.openModel("alertHandle");

                                               localStorage.setItem('templateInfo',JSON.stringify($rootScope.templateInfo));
                                              
                                              $rootScope.questStatus = true;
                                              $scope.getTemplateMasterDataAfterUpdating();                    
                                              $scope.listData(true); 
                                              
                                              $rootScope.templateInfoChanged= false;
                                              localStorage.setItem('templateInfoChanged',false);
                                             // $rootScope.selectedCountryChanged= false;
                                             // $rootScope.selectedLanguageChanged= false;
                                              $scope.$apply();
                                          },function(error){
                                              $scope.saveFlag =false;  
                                              $rootScope.shouldShowTyreImg = false;
                                              $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                              $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                              $scope.openModel("connectionError"); 
                                               return false;
                                          });
                                  },function(error){
                                      $scope.saveFlag =false;  
                                      $rootScope.shouldShowTyreImg = false; 
                                      $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                      $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                      $scope.openModel("connectionError");
                                       return false;
                                  });                                            
                            }                                           
                  });   

            },function(error){
                $scope.saveFlag =false; 
                $rootScope.shouldShowTyreImg = false;  
            });
            }else{             

                      $scope.saveFlag =false;
                      $rootScope.shouldShowTyreImg = false; 
                      $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                   
                      if($rootScope.templateInfo.titelTemplate == '' && $rootScope.templateInfo.selectedCountry != "" && $rootScope.templateInfo.selectedCountry != "null" && $rootScope.templateInfo.selectedLanguage != "" && $rootScope.templateInfo.selectedLanguage != null){
                        $scope.modalContent =dealerAudit_ConstantsConst.enterTemplateTitleMsg;
                        $scope.openModel("alertHandle");                                                       
                       return ;
                      }else if( ($rootScope.templateInfo.selectedCountry == "" || $rootScope.templateInfo.selectedCountry == null) && ($rootScope.templateInfo.selectedLanguage == null || $rootScope.templateInfo.selectedLanguage== "") ){
                          $rootScope.templateInfo.selectedCountry="";
                          $rootScope.templateInfo.selectedLanguage="";
                        $scope.modalContent = dealerAudit_ConstantsConst.selectCountryLanguageMsg;
                        $scope.openModel("alertHandle");                                                       
                        return ;
                      }else if( ($rootScope.templateInfo.selectedCountry == "" || $rootScope.templateInfo.selectedCountry == null) && ($rootScope.templateInfo.selectedLanguage != null || $rootScope.templateInfo.selectedLanguage!= "") ){
                        $rootScope.templateInfo.selectedCountry="";
                        $scope.modalContent =dealerAudit_ConstantsConst.selectCountryMsg;
                        $scope.openModel("alertHandle");                                                       
                        return ;
                      }else if(($rootScope.templateInfo.selectedCountry != "" || $rootScope.templateInfo.selectedCountry != null)&& ($rootScope.templateInfo.selectedLanguage == null || $rootScope.templateInfo.selectedLanguage== "")){
                         $rootScope.templateInfo.selectedLanguage="";
                        $scope.modalContent = dealerAudit_ConstantsConst.selectLanguageMsg;
                        $scope.openModel("alertHandle");                                                       
                        return ;
                      }else{
                        $rootScope.templateInfo.selectedCountry="";
                        $rootScope.templateInfo.selectedLanguage="";
                        $rootScope.templateInfo.titelTemplate='';
                        $scope.modalContent =dealerAudit_ConstantsConst.enterDetailsMsg;
                        $scope.openModel("alertHandle");                                                       
                        return ;
                      }
                                                          
            }           
          }    
        }catch(e){
          $rootScope.shouldShowTyreImg = false;
           return false;
        }
        }

    /**
     * @function saveCompleteTemplate
     * @description function to update the template information to saved state/published state/unpublished state
     */  
        $scope.saveCompleteTemplate = function(value){

           
          try{
          var val= value; 
          if(!$rootScope.updateTemplate){ 

            if(!$scope.saveFlag ){

              if(typeof($rootScope.templateId)!="undefined" && $rootScope.templateId !=""){
                    $rootScope.shouldShowTyreImg = true; 
                    $scope.saveFlag = true;           

                    if(val == 'Save'){
                     
                        var date = new Date();
                        date = $filter('date')(new Date(), 'dd-MM-yyyy');
                        $scope.template_master_info={                     
                            status:'saved', 
                            template_id:$rootScope.savedtemplateId,
                            creation_date:date
                        }

                        
                             $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){ 
                                    clientObjectType.get($rootScope.savedtemplateId).then(function(responseObject){

                                        responseObject.update($scope.template_master_info).then(function(response){


                                              $rootScope.templateToUpdate.status= 'saved'; 

                                              $rootScope.disablePublishBtn = false;
                                              $rootScope.disableSaveBtn = false;
                                              $rootScope.shouldShowTyreImg = false; 
                                              $scope.saveFlag = false;
                                              $scope.getTemplateMasterDataAfterUpdating();   
                                              $scope.modalTitle =dealerAudit_ConstantsConst.successModelTitle;
                                              $scope.modalContent =dealerAudit_ConstantsConst.templateUpdatedToSaved;
                                              $scope.openModel("alertHandle");                                 
                                            
                                      
                                        },function(error){  
                                                 $scope.saveFlag = false;
                                                 $rootScope.shouldShowTyreImg = false; 
                                                 $scope.$apply(); 
                                                 $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                                  $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                                  $scope.openModel("connectionError");                                   
                                        });
                                        
                                        $scope.$apply();
                                    },function(error){ 
                                         $scope.saveFlag = false;
                                         $rootScope.shouldShowTyreImg = false; 
                                         $scope.$apply(); 
                                         $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                         $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                         $scope.openModel("connectionError");                               
                                        return false;
                                    });
                            },function(error){   
                                 $scope.saveFlag = false;
                                 $rootScope.shouldShowTyreImg = false; 
                                 $scope.$apply();     
                                 $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                $scope.openModel("connectionError");                  
                            });
                       
                    }       

                    if(val == 'Publish'){
                         
                       var date = new Date();
                       date = $filter('date')(new Date(), 'dd-MM-yyyy');
                       var filterParams = {
                                template_id:$rootScope.templateId                               
                              };

                        $rootScope.session.getClientObjectType('question').then(function(clientObjectType){
                         clientObjectType.search("question", 0, 10000, filterParams, null, null, null, null, null, true).then(function(response) {
                              if(response.length>0){                        

                                           $scope.template_master_info={           
                                              country_name:$rootScope.templateInfo.selectedCountry,           
                                              language_name: $rootScope.templateInfo.selectedLanguage,
                                              status:'Published',          
                                              tam_name: $rootScope.UserName,
                                              template_name: $rootScope.templateInfo.titelTemplate,
                                              template_id:$rootScope.templateId,
                                              creation_date:date
                                            }


                                         $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){ 
                                                clientObjectType.get($rootScope.templateId).then(function(responseObject){

                                                    responseObject.update($scope.template_master_info).then(function(response){

                                                        $rootScope.templateToUpdate.status= 'Published'; 
                                                        $rootScope.disablePublishBtn = true;
                                                        $rootScope.disableSaveBtn = true;

                                                        $scope.saveFlag = false;
                                                        $rootScope.shouldShowTyreImg = false; 
                                                       //  $rootScope.questStatus = false;
                                                        $scope.getTemplateMasterDataAfterUpdating();   
                                                       // $scope.resetFields();
                                                       // $rootScope.templateInfo.selectedCountry="";
                                                       // $rootScope.templateInfo.selectedLanguage="";
                                                       // $rootScope.templateInfo.titelTemplate="";

                                                       // $rootScope.templateId="";
                                                        $scope.modalTitle = dealerAudit_ConstantsConst.successModelTitle;
                                                        $scope.modalContent = dealerAudit_ConstantsConst.templateUpdatedToPublished;
                                                        $scope.openModel("alertHandle");                           
                                                  
                                                      //  $rootScope.headersOfQuestions=[];
                                                      //   $rootScope.subheaderOfQuestion = [];
                                                      //  $rootScope.questionsOfTemplate=[];
                                                      //  $rootScope.questionOptions=[];
                                                      //  $rootScope.groupedQuestionsArray=[]; 
                                                  
                                                  

                                                  
                                                    },function(error){
                                                       $scope.saveFlag = false;
                                                       $rootScope.shouldShowTyreImg = false; 
                                                       $scope.$apply();
                                                        $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                                        $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                                        $scope.openModel("connectionError");
                                                           return false;
                                                    });
                                                    
                                                    $scope.$apply();
                                                },function(error){
                                                 
                                                   $scope.saveFlag = false;
                                                   $rootScope.shouldShowTyreImg = false; 
                                                   $scope.$apply();
                                                   $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                                    $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                                    $scope.openModel("connectionError");
                                                     return false;
                                                });
                                        },function(error){
                                           $scope.saveFlag = false;
                                           $rootScope.shouldShowTyreImg = false; 
                                           $scope.$apply();
                                           $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                            $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                            $scope.openModel("connectionError");
                                             return false;
                                        });
                                   
                              }else{
                                   $scope.saveFlag = false;
                                   $rootScope.shouldShowTyreImg = false; 
                                   $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                   $scope.modalContent = dealerAudit_ConstantsConst.publishTemplateWithoutQuestErrorMsg;
                                   $scope.openModel("publishTemplateHandle");
                              }

                          },function(error){
                               $scope.saveFlag = false;
                               $rootScope.shouldShowTyreImg = false; 
                               $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                               $scope.modalContent =dealerAudit_ConstantsConst.publishTemplateWithoutQuestErrorMsg;
                               $scope.openModel("publishTemplateHandle");
                          });

                        },function(error){
                        });
                         
                             
                    }  

                    if(val == 'Unpublish'){

                        var date = new Date();
                        date = $filter('date')(new Date(), 'dd-MM-yyyy');
                        $scope.template_master_info={                     
                            status:'Unpublished', 
                            template_id:$rootScope.savedtemplateId,
                            creation_date:date
                        }
                        $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){ 
                                    clientObjectType.get($rootScope.savedtemplateId).then(function(responseObject){

                                        responseObject.update($scope.template_master_info).then(function(response){


                                              $rootScope.templateToUpdate.status= 'Unpublished';                                        

                                              $rootScope.disablePublishBtn = false;
                                              $rootScope.disableSaveBtn = true;
                                              $rootScope.shouldShowTyreImg = false;   
                                              $scope.getTemplateMasterDataAfterUpdating();                                          
                                              $scope.modalTitle = dealerAudit_ConstantsConst.successModelTitle;
                                              $scope.modalContent = dealerAudit_ConstantsConst.templateUpdatedToUnPublished;
                                              $scope.openModel("alertHandle");                                 
                                            
                                      
                                        },function(error){  
                                                 $scope.saveFlag = false;
                                                 $rootScope.shouldShowTyreImg = false; 
                                                 $scope.$apply(); 
                                                 $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                                  $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                                  $scope.openModel("connectionError");                                   
                                        });
                                        
                                        $scope.$apply();
                                    },function(error){ 
                                         $scope.saveFlag = false;
                                         $rootScope.shouldShowTyreImg = false; 
                                         $scope.$apply(); 
                                         $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                         $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                         $scope.openModel("connectionError");                               
                                        return false;
                                    });
                            },function(error){   
                                 $scope.saveFlag = false;
                                 $rootScope.shouldShowTyreImg = false; 
                                 $scope.$apply();     
                                 $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                $scope.openModel("connectionError");                  
                            });


                    }
          
              }else{
                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                if(val == 'Save'){
                    $scope.modalContent = dealerAudit_ConstantsConst.saveTemplateBeforeSaving;
                }else{
                    $scope.modalContent = dealerAudit_ConstantsConst.saveTemplateBeforePublishing;
                }
                $scope.openModel("alertHandle");    
                
              }
            }
          }if($rootScope.updateTemplate){
            $rootScope.shouldShowTyreImg = true;              
              
           if(val == 'Unpublish'){
                 
                  var date = new Date();
                  date = $filter('date')(new Date(), 'dd-MM-yyyy');
                  $scope.template_master_info={                     
                    status:'Unpublished', 
                    template_id:$rootScope.savedtemplateId,
                    creation_date:date
                   }
                   
                      
                         $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){ 
                                clientObjectType.get($rootScope.savedtemplateId).then(function(responseObject){

                                    responseObject.update($scope.template_master_info).then(function(response){
                                      
                                          $rootScope.templateToUpdate.status= 'Unpublished';
                                          $rootScope.disablePublishBtn = false;
                                          $rootScope.disableSaveBtn = true;
                                          $rootScope.shouldShowTyreImg = false;   
                                          $scope.getTemplateMasterDataAfterUpdating();                                          
                                          $scope.modalTitle = dealerAudit_ConstantsConst.successModelTitle;
                                          $scope.modalContent = dealerAudit_ConstantsConst.templateUpdatedToUnPublished;
                                          $scope.openModel("alertHandle");                                 
                                        
                                  
                                    },function(error){   
                                             $scope.saveFlag = false;
                                             $rootScope.shouldShowTyreImg = false; 
                                             $scope.$apply(); 
                                             $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                             $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                             $scope.openModel("connectionError");                                   
                                    });
                                    
                                    $scope.$apply();
                                },function(error){  
                                     $scope.saveFlag = false;
                                     $rootScope.shouldShowTyreImg = false; 
                                     $scope.$apply();
                                     $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                     $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                     $scope.openModel("connectionError");                                  
                                    return false;
                                });
                        },function(error){  
                               $scope.saveFlag = false;
                               $rootScope.shouldShowTyreImg = false; 
                               $scope.$apply(); 

                               $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                               $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                               $scope.openModel("connectionError");                          
                        });
                   
          }
          if(val == 'Publish'){
                 
              var date = new Date();                 
              date = $filter('date')(new Date(), 'dd-MM-yyyy');                  

              var filterParams = {
                    template_id:$rootScope.savedtemplateId                               
                  };

               $rootScope.session.getClientObjectType('question').then(function(clientObjectType){
               clientObjectType.search("question", 0, 10000, filterParams, null, null, null, null, null, true).then(function(response) {
                  if(response.length>0){
                
                      $scope.template_master_info={                     
                        status:'Published', 
                        template_id:$rootScope.savedtemplateId,
                        creation_date : date
                       }                                   
                       
                       $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){ 
                              clientObjectType.get($rootScope.savedtemplateId).then(function(responseObject){

                                  responseObject.update($scope.template_master_info).then(function(response){
                                
                                        $rootScope.templateToUpdate.status= 'Published';
                                        $rootScope.disablePublishBtn = true;
                                        $rootScope.disableSaveBtn = true;
                                        $rootScope.shouldShowTyreImg = false;   
                                        $scope.getTemplateMasterDataAfterUpdating();                                     
                                        $scope.modalTitle =dealerAudit_ConstantsConst.successModelTitle;
                                        $scope.modalContent =dealerAudit_ConstantsConst.templateUpdatedToPublished;
                                        $scope.openModel("alertHandle");                                 
                                       
                                
                                  },function(error){   
                                         $scope.saveFlag = false;
                                         $rootScope.shouldShowTyreImg = false; 
                                         $scope.$apply();
                                         $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                         $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                         $scope.openModel("connectionError");                                      
                                  });
                                  
                                  $scope.$apply();
                              },function(error){   
                                   $scope.saveFlag = false;
                                   $rootScope.shouldShowTyreImg = false; 
                                   $scope.$apply();
                                   $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                   $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                   $scope.openModel("connectionError");                                 
                                  return false;
                              });
                      },function(error){  
                           $scope.saveFlag = false;
                           $rootScope.shouldShowTyreImg = false; 
                           $scope.$apply(); 
                           $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                           $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                           $scope.openModel("connectionError");                          
                      });

                }else{
                       $scope.saveFlag = false;
                       $rootScope.shouldShowTyreImg = false; 
                       $scope.modalTitle =dealerAudit_ConstantsConst.errorModalTitle;
                       $scope.modalContent =dealerAudit_ConstantsConst.publishTemplateWithoutQuestErrorMsg;
                       $scope.openModel("publishTemplateHandle");
                }

              },function(error){
                   $scope.saveFlag = false;
                   $rootScope.shouldShowTyreImg = false; 
                   $scope.modalTitle =dealerAudit_ConstantsConst.errorModalTitle;
                   $scope.modalContent = dealerAudit_ConstantsConst.publishTemplateWithoutQuestErrorMsg;
                   $scope.openModel("publishTemplateHandle");
              });
            },function(error){
          });
                   
          }

          }
          }catch(e){
            $rootScope.shouldShowTyreImg = false;
             return false;
          }  
      }
     /**
     * @function getTemplateMasterDataAfterUpdating
     * @description function to collect template master data from db after updating(publish/unpublish/saving) template/
     */     
    $scope.getTemplateMasterDataAfterUpdating = function(){

      try{

              var filterParams = {
                    "tam_name": $rootScope.UserName                    
                  };
              $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){
                  clientObjectType.search("template_master", 0, 10000, filterParams, null, null, null, null, null, true).then(function(response) {
                 
                   $rootScope.templateListItems =[];
                    for(i=0;i<response.length;i++){
                        $rootScope.templateListItems.push(response[i].data);
                    }                   
                  
                    $rootScope.templateListItems = $filter('orderBy')($rootScope.templateListItems, 'creation_date', true);                   
                    $scope.$apply();
                  },function(error){                   
                 
                    if(error.code == 55 && error.message == "No Content"){                           
                            }
                            else{
                            }
                  })
     
              },function(error){ 
                 $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                 $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                 $scope.openModel("connectionError");      
              });
      }catch(e){

      }

    }


    /**
     * @function listData
     * @description function to collect headers and subheaders related to country
     */ 
        $scope.listData = function(cacheFlag){
         try{
                $rootScope.headerList = [];
                $rootScope.subHeaderList=[];
                $rootScope.questionTypesList=[];
                $rootScope.noHeadersAvailable=false;
                $rootScope.noSubHeadersAvailable = false;
                $rootScope.session.getClientObjectType("sub_header").then(function(clientObjectType){
                        clientObjectType.list(0,100000,null,cacheFlag).then(function(response) {                           
                            
                            for(i=0 ; i< response.length ; i ++){
                                $rootScope.subHeaderList.push(response[i].getData());                         

                            }                           
                                                                  
                        },function(error){
                            $rootScope.noSubHeadersAvailable = false;
                            if(error.code == 55 && error.message == "No Content"){
                          
                            }
                            
                        });
                    },function(error){
                         $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                         $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                         $scope.openModel("connectionError");  
                        
                    });
               
                  var filterParams={
                    language : $rootScope.templateInfo.selectedLanguage
                  }
                  
                 $rootScope.session.getClientObjectType("header").then(function(clientObjectType){
                       // clientObjectType.list(0,10000000).then(function(response) {
                        clientObjectType.search("header", 0, 10000, filterParams, null, null, null, null, null, cacheFlag).then(function(response) { 
                            
                            for(i=0 ; i< response.length ; i ++){
                                $rootScope.headerList.push(response[i].getData());                             
                            }  
                            $scope.$apply();                         
                            
                        },function(error){                          
                             $rootScope.noHeadersAvailable=true;
                            if(error.code == 55 && error.message == "No Content"){
                            
                            }                            
                            
                        });
                    },function(error){
                         $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                         $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                         $scope.openModel("connectionError");  
                        
                    });       


                    $rootScope.session.getClientObjectType("question_type").then(function(clientObjectType){
                        clientObjectType.list(0,100,null,false).then(function(response) {                           
                          var tempQustTypes=[];
                          
                            for(i=0 ; i< response.length ; i ++){
                                tempQustTypes.push(response[i].getData().question_type_text);                         

                            }
                           
                              $rootScope.questionTypesList=tempQustTypes;
                       
                        },function(error){
                          
                            if(error.code == 55 && error.message == "No Content"){
                          
                            }
                            
                        });
                    },function(error){
                         $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                         $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                         $scope.openModel("connectionError");  
                        
                    });           

         
        }catch(e){
        
          return false;
        }
        };

    /**
     * @function generateHashPropertyForCountryLan
     * @description function to get country language comination for which templates are created
     */ 
      var generateHashPropertyForCountryLan = function(array){
     
        var hash = [];
        for (var i=0; i<array.length; i++){
          hash.push({'country_name':array[i].country_name,'language_name':array[i].language_name});
        }
        return hash;
      };

    /**
     * @function getCountryLanguage
     * @description function to collect country language combinations present under logged in manager
     */ 
         $scope.getCountryLanguage= function(cacheFlag){

            try{
              $rootScope.shouldShowTyreImg = true;
              
              $rootScope.countryLangForTam = [];
              $scope.dataOfCountryLang=[];
              $scope.dataOfSavedLocForTam =[];
              $rootScope.slectedCountry=[];
              ISession.getInstance().then(function(session) {
                    
                    $rootScope.session = session;
                    $rootScope.session.getClientObjectType("country_language").then(function(clientObjectType){
                        clientObjectType.list(0,100000,null,cacheFlag).then(function(response){
                                
                            $scope.dataOfCountryLang=[];
                            for(i=0;i<response.length;i++){
                                $scope.dataOfCountryLang.push(response[i].getData());
                            }

                              
                            var filterParams = {
                              "tam_name": $rootScope.UserName,                                              
                            };
                            $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){
                              clientObjectType.search("template_master", 0, 10000, filterParams, null, null, null, null, null, true).then(function(response) {
                                
                                var templates=[];
                                var tempArray=[];
                                for(i=0;i<response.length;i++){
                                    tempArray.push(response[i].getData());
                                } 

                                for(j=0;j<tempArray.length;j++){
                                  if(tempArray[j].status!='Deleted'){
                                      templates.push(tempArray[j]);
                                  }
                                }
                                
                                var filterParams = {
                                  "tam_name": $rootScope.UserName                    
                                };
                                
                                // getting saved_locations and filtering based on tam
                                $rootScope.session.getClientObjectType("saved_locations").then(function(clientObjectType){
                                       clientObjectType.search("saved_locations", 0, 10000, filterParams, null, null, null, null, null, cacheFlag).then(function(response) {
                                            
                                            $scope.dataOfSavedLocForTam=[];
                                            for(i=0;i<response.length;i++){
                                               $scope.dataOfSavedLocForTam.push(response[i].getData());
                                            }
                                           
                                            for(i=0;i<$scope.dataOfSavedLocForTam.length;i++){
                                              $scope.dataOfCountryLang.forEach(function(o){        
                                                if (o.country_name == $scope.dataOfSavedLocForTam[i].country_name){                                                   
                                                    $rootScope.countryLangForTam.push(o);
                                                    $rootScope.slectedCountry.push(o);    
                                                  }                                               
                                              });
                                            }
                                           
                                           if(!$rootScope.updateTemplate){                                             
                                                var temps = generateHashPropertyForCountryLan(templates);                                                 
                                                temps.forEach(function(o){                                                     
                                                    $rootScope.countryLangForTam.forEach(function(x){
                                                      if(o.language_name == x.language_name && o.country_name == x.country_name){
                                                          var index =$rootScope.countryLangForTam.indexOf(x);                                                         
                                                          $rootScope.countryLangForTam.splice(index, 1);     
                                                      }
                                                    });                                                  
                                                });    
                                            }                                      
                                           
                                           $rootScope.shouldShowTyreImg = false;
                                           $scope.$apply();

                                        },function(error){
                                          $rootScope.shouldShowTyreImg = false;
                                           $scope.$apply();
                                        });

                                },function(error){
                                  $rootScope.shouldShowTyreImg = false;
                                   $scope.$apply();
                                });
                            },function(error){

                               //$rootScope.shouldShowTyreImg = false;
                               var filterParams = {
                                  "tam_name": $rootScope.UserName                    
                                };
                                
                                // getting saved_locations and filtering based on tam
                                $rootScope.session.getClientObjectType("saved_locations").then(function(clientObjectType){
                                       clientObjectType.search("saved_locations", 0, 10000, filterParams, null, null, null, null, null, cacheFlag).then(function(response) {
                                            
                                            $scope.dataOfSavedLocForTam=[];
                                            for(i=0;i<response.length;i++){
                                               $scope.dataOfSavedLocForTam.push(response[i].getData());
                                            }
                                           
                                            for(i=0;i<$scope.dataOfSavedLocForTam.length;i++){
                                              $scope.dataOfCountryLang.forEach(function(o){        
                                                if (o.country_name == $scope.dataOfSavedLocForTam[i].country_name){                                                   
                                                    $rootScope.countryLangForTam.push(o);
                                                    $rootScope.slectedCountry.push(o);    
                                                  }                                               
                                              });
                                            }                                          
                                                                             
                                           
                                           $rootScope.shouldShowTyreImg = false;
                                           $scope.$apply();

                                        },function(error){
                                          $rootScope.shouldShowTyreImg = false;
                                           $scope.$apply();
                                        });

                                },function(error){
                                  $rootScope.shouldShowTyreImg = false;
                                   $scope.$apply();
                                });
                               $scope.$apply();
                            });

                      },function(error){
                        $rootScope.shouldShowTyreImg = false;
                         $scope.$apply();
                      });

                      },function(error){
                           $rootScope.shouldShowTyreImg = false;
                           $scope.$apply();                               
                            if(error.code == 55 && error.message == "No Content"){
                              $rootScope.shouldShowTyreImg = false;
                              $scope.$apply();
                          }
                          
                      });

                    },function(error){
                        $rootScope.shouldShowTyreImg = false;
                        $scope.$apply();
                    }); 

                    }, function(error) {        
                  });  

            
              }catch(e){
                $rootScope.shouldShowTyreImg = false;
                 //$scope.$apply();
                return false;
              }

        };

    /**
     * @function deleteQuestionClicked
     * @description function to handle the delete question event    
     */ 
        $scope.deleteQuestionClicked = function(question){

            try{ 
              if(!$rootScope.deleteQuestBtnClicked){

                if($rootScope.templateToUpdate.status== 'Published'){
                    $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                    $scope.modalContent =dealerAudit_ConstantsConst.unPublishTemplateToDeleteQuest;
                    $scope.openModel("alertHandle"); 
                }else{
                  $scope.questionToDelete =[];
                  $scope.questionToDelete = question;
                  $rootScope.deleteQuestBtnClicked = true;

                  $scope.modalTitle =dealerAudit_ConstantsConst.confirmationModelTitle;
                  $scope.modalContent = dealerAudit_ConstantsConst.deleteQuestConfirmMsg;
                  $scope.open("deleteQuestHandle");  
                }
              }

            }catch(e){
            }
        }

    /**
     * @function deleteQuestionClicked
     * @description function to delete the question
     * @params question id
     */ 
        $scope.deleteQuestion = function(question){

          try{

        //  $rootScope.shouldShowTyreImg = true;
          var questionToDelete = question;
          
          $rootScope.session.getClientObjectType("question").then(function(clientObjectType){
              clientObjectType.get(questionToDelete.question_id).then(function(response){
                  response.remove().then(function(response){
                      
                      for(i = 0; i< $rootScope.questionsOfTemplate.length ; i ++){
                          if($rootScope.questionsOfTemplate[i].question_id == questionToDelete.question_id){
                            
                              $rootScope.questionsOfTemplate.splice(i,1);                                           

                          }
                      }

                      $rootScope.groupedQuestionsArray = groupArray($rootScope.questionsOfTemplate, 'header_text', 'sub_header_text');
                      $rootScope.hiddens = false;                      
                      $rootScope.editQuestion= false;                                                              
                      $scope.getQuestionUnderTemplate(true);
                      $state.reload();
                      
                      
                  },function(error){
                     $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                     $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                     $scope.openModel("connectionError");  
                  });
              },function(error){
                 $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                 $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                 $scope.openModel("connectionError");  
              })
 
          },function(error){
           $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
           $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
           $scope.openModel("connectionError");  
          })
 
        }catch(e){
         // $rootScope.shouldShowTyreImg = false;
           $scope.$apply();
        }
      }

    /**
     * @function getTemplateList
     * @description function to collect the templates created by logged in manager    
     */  
      $scope.getTemplateList=function(cacheFlag){
        try{

          $rootScope.templateListItems =[];
          $rootScope.shouldShowTyreImg = true;
          $rootScope.spanlist = true;

          ISession.getInstance().then(function(session) {

                $rootScope.session = session;
        
               var filterParams = {
                      "tam_name": $rootScope.UserName                    
                      };
              $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){
                  clientObjectType.search("template_master", 0, 10000, filterParams, null, null, null, null, null, cacheFlag).then(function(response) {
                 
                    $rootScope.templateListItems =[];
                    for(i=0;i<response.length;i++){
                        $rootScope.templateListItems.push(response[i].data);
                    }
                   
                     $rootScope.shouldShowTyreImg = false;
                     $rootScope.templateListItems = $filter('orderBy')($rootScope.templateListItems, 'creation_date', true);
                    if($rootScope.templateListItems.length<0){
                      $scope.noTemplatesForManager = true;
                    }else{
                      $scope.noTemplatesForManager = false;
                    }
                    $scope.$apply();
                  },function(error){                   
                 
                    if(error.code == 55 && error.message == "No Content"){
                            $scope.noTemplatesForManager = true;
                             $rootScope.shouldShowTyreImg = false;
                              $scope.$apply();
                            }
                            else{
                            }
                  })
     
              },function(error){
                $rootScope.shouldShowTyreImg = false;
                              $scope.$apply();
              });

            }, function(error) {        
            });
          }catch(e){

          }

      }
     
    /**
     * @function actionOnOptionSelected
     * @description function to watch which all fields have been changed, required for back button handling   
     */  
     $scope.actionOnOptionSelected = function(){  
      
      try{    
        
        $scope.subHeadedOfSelectedHeader=[];
        if(typeof($rootScope.subHeaderList)!="undefined"){
        $rootScope.subHeaderList.forEach(function(o){
            if(o.header_id == $rootScope.questionInformation.selectedHeader.header_id){
                $scope.subHeadedOfSelectedHeader.push(o);
            }
        });
        
      }
      }catch(e){
        
      }

      }
      
      function sortByKey(array, key) {
          return array.sort(function(a, b) {
              var x = a[key]; var y = b[key];
              return ((x < y) ? -1 : ((x > y) ? 1 : 0));
          });
      }  



    /**
     * @function getQuestionUnderTemplate
     * @description function to collect the questions created for preticular template 
     */
      $scope.getQuestionUnderTemplate= function(cacheFlag){

       try{

        //$rootScope.hiddens= !$rootScope.hiddens;
        $rootScope.headersOfQuestions=[];
        $rootScope.subheaderOfQuestion = [];
        $rootScope.questionsOfTemplate=[];
        $rootScope.tempQuestionsOfTemplate =[];
        $rootScope.questionOptions=[];
        $rootScope.groupedQuestionsArray = [];
              $rootScope.session.getClientObjectType("question").then(function(clientObjectType){                                     
             
                        var filterParams = {
                        "template_id": $rootScope.savedtemplateId                    
                        };                      
                     
                            clientObjectType.search("question", 0, 10000, filterParams, null, null, null, null, null, cacheFlag).then(function(response) {


                                  $scope.questionsOfTemplateWithoutOrder =[];
                                  $rootScope.groupedQuestionsArray = [];
                                  for(i=0 ; i< response.length ; i ++){
                                      $scope.questionsOfTemplateWithoutOrder.push(response[i].getData());                                                                 
                                  }
                                  $rootScope.questionsOfTemplate = sortByKey($scope.questionsOfTemplateWithoutOrder, 'position_id');

                                  //$rootScope.tempQuestionsOfTemplate = $rootScope.questionsOfTemplate;
                                  angular.copy($rootScope.questionsOfTemplate,$rootScope.tempQuestionsOfTemplate);

                                  $rootScope.groupedQuestionsArray = groupArray($rootScope.questionsOfTemplate, 'header_text', 'sub_header_text');

                                  $rootScope.headersOfQuestions = Object.keys($rootScope.groupedQuestionsArray);                            
                        
                                  $scope.$apply();                                 
                                 
                               
                             },function(error){               
                            })


              },function(error){               
              })

          }catch(e){
          }

      }
      $scope.gotoHome= function(){
        $scope.navigateToOtherTab('home');
      }   
    /**
     * @function navigateToOtherTab
     * @description function to handle navigation to other pages
     */
      $scope.navigateToOtherTab = function(navigateTo){
        $scope.navigateToLocation='';
        $scope.navigateToLocation = navigateTo;
        localStorage.setItem('currentyearpagination',1);
        localStorage.setItem('prevyearpagination',1);
        localStorage.setItem('dealerlistPagination',1);
       
        if(($rootScope.templateInfoChanged || $rootScope.questionInfoChanged) || ($rootScope.editQuestion && $rootScope.questionupdateInfoChanged)|| ($rootScope.editQuestion && $rootScope.questionupdateInfoChangedMinMax)){
            $scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
            $scope.modalContent = dealerAudit_ConstantsConst.discardChangesMsg;
            $scope.open("navigateToOtherTab");   

        }else{
          if($scope.navigateToLocation == 'login'){
            $rootScope.logoutFlag = true;       
            localStorage.removeItem('sessionObj');
          }
          $state.go($scope.navigateToLocation);
        }

      }

    /**
     * @function navigateToOtherTab
     * @description function to handle navigation to template list tab
     */
       $scope.navigateToTemplateList = function(){          
      
        if(($rootScope.templateInfoChanged || $rootScope.questionInfoChanged) || ($rootScope.editQuestion && $rootScope.questionupdateInfoChanged)|| ($rootScope.editQuestion && $rootScope.questionupdateInfoChangedMinMax)){
            $scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
            $scope.modalContent =dealerAudit_ConstantsConst.discardChangesMsg;
            $scope.open("navigateToTemplateList");   

        }else{
          templateList();
        }


      }


     var generateHashProperty = function(array){
     
        var hash = {};
        for (var i=0; i<array.length; i++){
          hash[ array[i].language_name ] = true;
        }
        return hash;
      }
    

    /**
     * @function countrySelected
     * @description function filter the language list based on country selected. If template created for this country and any language combination,the language will not be listed again.
     */
      $scope.countrySelected = function(){
       
            try{
                  if($rootScope.templateInfo.selectedCountry){

                    $rootScope.shouldShowTyreImg = true;
                    $scope.countrySelected.selected=true;
                    var filterParams = {
                      "tam_name": $rootScope.UserName,
                      "country_name": $rootScope.templateInfo.selectedCountry                 
                    };
                    $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){
                        clientObjectType.search("template_master", 0, 10000, filterParams, null, null, null, null, null, true).then(function(response) {
                 
                                    
                          $rootScope.templateListItems =[];
                          var tempArray=[];
                          for(i=0;i<response.length;i++){
                              tempArray.push(response[i].data);
                              
                          }

                          for(j=0;j<tempArray.length;j++){
                            if(tempArray[j].status!='Deleted'){
                                $rootScope.templateListItems.push(tempArray[j]);
                            }
                          }
                 
                          var filterPars = {
                            "country_name": $rootScope.templateInfo.selectedCountry                    
                          };

                          $rootScope.session.getClientObjectType("country_language").then(function(clientObjectType){
                              clientObjectType.search("country_language", 0, 10000, filterPars, null, null, null, null, null, false).then(function(response) {

                                  $rootScope.slectedCountry = [] ;
                                  //country-language combination data

                                  var tempCountryLangs=[];

                                  for(i=0;i<response.length;i++){
                                    tempCountryLangs.push(response[i].getData());
                                  } 
                                  var hash = generateHashProperty($rootScope.templateListItems);

                                  //getting the country-language combinations for which templates are not created
                                  for (var i=0; i<tempCountryLangs.length; i++){
                                    var value = tempCountryLangs[i].language_name;
                                    if(!hash[value]){
                                        $rootScope.slectedCountry.push(tempCountryLangs[i]);
                                    }
                                  }
                                  $rootScope.shouldShowTyreImg = false;
                                  $scope.$apply();
                                           
                                

                          },function(error){
                         
                         
                          if(error.code == 55 && error.message == "No Content"){
                                   $rootScope.shouldShowTyreImg = false;
                                    $scope.$apply();
                                  }
                                  else{
                                  }
                        })
           
                    },function(error){
                       $rootScope.shouldShowTyreImg = false;
                    })

                  },function(error){
                   
                     $rootScope.shouldShowTyreImg = false;  
                    if(error.code == 55 && error.message == "No Content"){
                             $rootScope.shouldShowTyreImg = false;
                              $scope.$apply();
                            }
                            else{
                            }
                  })
     
              },function(error){
                 $rootScope.shouldShowTyreImg = false;
              })
            }

            }catch(e){
               $rootScope.shouldShowTyreImg = false;
            }

        }

    /**
     * @function templateFieldValuesChanged
     * @description function to watch the changes done to template section
     */
    
      $scope.templateFieldValuesChanged= function(){


        if($rootScope.templateInfo.titelTemplate!='' || typeof($rootScope.templateInfo.selectedCountry)!='undefined'||typeof($rootScope.templateInfo.selectedLanguage)!='undefined'){  
          $rootScope.templateInfoChanged= true;  
          localStorage.setItem('templateInfoChanged',true);     
        }else{
          
          $rootScope.templateInfoChanged= false;
          localStorage.setItem('templateInfoChanged',false);  
        }     


      }

    /**
     * @function questionsFieldValuesChanged
     * @description function to watch the changes done to questionnaire section
     */
      
      $scope.questionsFieldValuesChanged= function(){      

        if($rootScope.questionInformation.newHeader!='' || $rootScope.questionInformation.newSubHeader!=''||$rootScope.questionInformation.questionEntered!='' || typeof($rootScope.questionInformation.selectedSubHeader) != "undefined" || typeof($rootScope.questionInformation.selectedHeader) != "undefined"||typeof($rootScope.questionInformation.seletedQestTyp) != "undefined"){         
          $rootScope.questionInfoChanged= true;
          localStorage.setItem('questionInfoChanged',true);
        }else{        
          $rootScope.questionInfoChanged= false;
          localStorage.setItem('questionInfoChanged',false);
        }
     
      }

      /**
     * @function updatequestionsFieldValuesChanged
     * @description function to watch the changes done to questionnaire section while editing
     */
      
      $scope.updatequestionsFieldValuesChanged= function(){      
        var question = JSON.parse(localStorage.getItem('questionBeforeEditing'));        
        if(question.question_text!=$rootScope.questionToUpdateInfo.question || question.mandatory_field!=$rootScope.questionToUpdateInfo.mandatory){         
          $rootScope.questionupdateInfoChanged= true;
          localStorage.setItem('questionupdateInfoChanged',true);
        }else{        
          $rootScope.questionupdateInfoChanged= false;
          localStorage.setItem('questionupdateInfoChanged',false);
        }
     
      }

      $scope.minMaxAltered = function(min,max){
        var question = JSON.parse(localStorage.getItem('questionBeforeEditing'));         
        if(parseInt(min)==parseInt(question.minimum_value) && parseInt(max)==parseInt(question.maximum_value)){
            $rootScope.questionupdateInfoChanged= false;
            localStorage.setItem('questionupdateInfoChanged',false);
        }else{
          $rootScope.questionupdateInfoChanged= true;
          localStorage.setItem('questionupdateInfoChanged',true);
        }
      }
    /**
     * @function updateTemplateData
     * @description function to save the changes done to template in edit mode
     */
      $rootScope.updateTemplateData = function(template){
          
         try{


            localStorage.removeItem('templateInfo');
            $rootScope.templateToUpdate = template;  
            $rootScope.updateTemplate= true;  

            if($rootScope.templateToUpdate.status =='Published'){           
              $rootScope.disablePublishBtn = true;
              $rootScope.disableSaveBtn = true;
              
            }if($rootScope.templateToUpdate.status =='Unpublished' || $rootScope.templateToUpdate.status =='saved'){     
              $rootScope.disablePublishBtn = false;
              $rootScope.disableSaveBtn = true;
            }

            $rootScope.savedtemplateId= $rootScope.templateToUpdate.template_id; 

            $rootScope.templateInfo = {
              selectedCountry: $rootScope.templateToUpdate.country_name,
              selectedLanguage: $rootScope.templateToUpdate.language_name,
              titelTemplate : $rootScope.templateToUpdate.template_name
            };

             localStorage.setItem('templateToUpdate',JSON.stringify($rootScope.templateToUpdate));
            //template information(country,language,templateName)
             localStorage.setItem('templateInfo',JSON.stringify($rootScope.templateInfo)); 
             //template id
             localStorage.setItem('savedtemplateId',$rootScope.savedtemplateId);
             //flag indiating controll is in edit mode
             localStorage.setItem('updateTemplate',$rootScope.updateTemplate);  
            
                  
            $state.go('questionnaire');

          }catch(e){
          }
      }


    /**
     * @function deleteTemplateClicked
     * @description function to handle the template deletion event
     */
       $scope.deleteTemplateClicked = function(template){

        try{

        $scope.templateToDelete = template;

        if(!$rootScope.deleteTemplateBtnClicked){

          if($scope.templateToDelete.status == 'Published'){
              $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
              $scope.modalContent = dealerAudit_ConstantsConst.unPublishTemplateToDeleteMsg;
              $scope.openModel("alertHandle"); 
          }else{            
            $rootScope.deleteTemplateBtnClicked = true;
            $scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
            $scope.modalContent = dealerAudit_ConstantsConst.deletTemplateConfirmMsg;
            $scope.open("deleteTemplateHandle");  
          }
        }

        }catch(e){
        }
       }

    /**
     * @function deleteTemplateData
     * @description function to delete the template data
     */
       $scope.deleteTemplateData = function(template){
         
        try{
            var templateToDelete = template;
          
            var template_master_info={ 
                        status:'Deleted',  
                        template_id:templateToDelete.template_id
            }
          
           $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){ 
                          clientObjectType.get(templateToDelete.template_id).then(function(responseObject){

                              responseObject.update(template_master_info).then(function(response){  
                                                                  
                                  $scope.getTemplateList(true);

                                  $scope.$apply();
                                  $state.reload();
                                 
                                 
                              },function(error){ 

                                    $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                                     $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                                     $scope.openModel("connectionError");                                  
                              });
                              
                             
                          },function(error){ 

                              $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                              $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                              $scope.openModel("connectionError");                  
                          });
                  },function(error){  
                      $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                       $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                       $scope.openModel("connectionError");                     
                      
                  });

          }catch(e){
          }

      }
      
      if($rootScope.questionToUpdateInfo == 'null'||typeof($rootScope.questionToUpdateInfo)=='undefined'){
          $rootScope.questionToUpdateInfo = {
                  "header":"",             
                  "subHeader":"",                        
                  "qestTyp" : "",           
                  "mandatory" : false,  
                  "question": "",
                  "quesID":"",
                  "majorChange":false,
                  "version":"",
                  "min":"",
                  "max":""
            };
        } 
   
    
      /**
     * @function headerChangedInEditMode
     * @description function to handle the edition of question
     */

      $scope.updateQuestion = function(question){     
       try{

        if($rootScope.editQuestion){
          //$rootScope.editQuestion= false;
         
        }else{
          
        if($rootScope.templateToUpdate.status== 'Published'){
            $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
            $scope.modalContent = dealerAudit_ConstantsConst.unPublishTemplateToEdit;
            $scope.openModel("alertHandle"); 
        }else{
        $rootScope.editQuestion= true;       
        $rootScope.shouldShowTyreImg = true;
        var questionToUpdate = question; 
        localStorage.setItem('questionBeforeEditing',JSON.stringify(question));
        $rootScope.questionToUpdateInfo = {
              "header":"",
              "selectedHeader" : {},
              "subHeader":"",           
              "selectedSubHeader" :{},           
              "qestTyp" : "",           
              "mandatory" : false,  
              "question": "",
              "quesID":"",
              "majorChange":false,
              "version":"",
              "min":"",
              "max":""
        };   
      
                              
                    $rootScope.headerLists = [];     
                    //$rootScope.groupedQuestionsArray=[];                        
                    $rootScope.headerLists = $rootScope.headerList;
                              
                    $rootScope.subHeaderLists =[];                                       
                    $rootScope.subHeaderLists= $rootScope.subHeaderList                                   
                    for(i=0;i<$rootScope.headerLists.length;i++){

                      if($rootScope.headerLists[i].header_name==questionToUpdate.header_text){                       

                        $rootScope.questionToUpdateInfo.selectedHeader = $rootScope.headerLists[i];
                        $rootScope.headSelected = $rootScope.headerLists[i].header_id;                                                                 
                      }
                    }

                     for(i=0;i<$rootScope.subHeaderLists.length;i++){

                      if($rootScope.subHeaderLists[i].sub_header_name==questionToUpdate.sub_header_text){                        

                       $rootScope.questionToUpdateInfo.selectedSubHeader=  $rootScope.subHeaderLists[i];
                       
                      }
                    }
                                                      
                
                    $rootScope.questionToUpdateInfo.header= questionToUpdate.header_text;                                          
                    $rootScope.questionToUpdateInfo.subHeader=  questionToUpdate.sub_header_text;    
                    $rootScope.questionToUpdateInfo.qestTyp = questionToUpdate.question_typ;           
                    $rootScope.questionToUpdateInfo.mandatory = questionToUpdate.mandatory_field? true : false;  
                    $rootScope.questionToUpdateInfo.question= questionToUpdate.question_text;                                  
                    $rootScope.questionToUpdateInfo.quesID = questionToUpdate.question_id;
                    $rootScope.questionToUpdateInfo.version= questionToUpdate.question_version;

                    if(questionToUpdate.question_typ == 'Numeric'){
                        $rootScope.questionToUpdateInfo.min=questionToUpdate.minimum_value;

                        $rootScope.questionToUpdateInfo.max=questionToUpdate.maximum_value;
                    }                    
                  //  $rootScope.questionInformation.seletedQestTyp = questionToUpdate.question_typ; 
                   
                    $rootScope.shouldShowTyreImg = false;                                    
                    //$scope.$apply();
                                      

        }
      }
      }catch(e){
      }
      }
     
    /**
     * @function checkForTemplateStatus
     * @description function to check for the template status, if template is published before then changing the status of question when edited
     */
      $scope.checkForTemplateStatus = function(quest){
        var questionsToUpdate = quest;
         $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){ 
            
            clientObjectType.get(questionsToUpdate.template_id).then(function(responseObject){

                var templateInformation = responseObject.data;
                if(templateInformation.status=='saved'){
                  return false;
                }else{  
                  return true;
                }
                return false;
            },function(error){                                
                 
            });
          },function(error){     

              $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
              $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
              $scope.openModel("connectionError");                            
               
          });
      }
    /**
     * @function headerChangedInEditMode
     * @description function to save changes done in the edit mode of question
     */
     $scope.updateSaveQuestion = function(quest){

      try{


            if(!$scope.saveFlag){ 
            $scope.saveFlag = true;
            var questionUpdate = quest; 
            var impactColumn='';
            var questVersion='';       
        
            $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){ 
            
            clientObjectType.get(questionUpdate.template_id).then(function(responseObject){

                var templateInformation = responseObject.data;

                $rootScope.session.getClientObjectType("question").then(function(clientObjectType){ 
                    clientObjectType.get($rootScope.questionToUpdateInfo.quesID).then(function(responseObject){
                 
                          if(questionUpdate.maximum_value == null || questionUpdate.minimum_value == null){
                            questionUpdate.maximum_value ="";
                            questionUpdate.minimum_value= "";
                          }                          

                          $scope.questionInfo={

                                    header_text : $rootScope.questionToUpdateInfo.header,
                                    mandatory_field : $rootScope.questionToUpdateInfo.mandatory,
                                    maximum_value : questionUpdate.maximum_value,
                                    minimum_value : questionUpdate.minimum_value,                         
                                    question_id : $rootScope.questionToUpdateInfo.quesID,
                                    question_text : $rootScope.questionToUpdateInfo.question,
                                    question_typ : $rootScope.questionToUpdateInfo.qestTyp,                                   
                                    status : 'modified',
                                    sub_header_text : $rootScope.questionToUpdateInfo.subHeader,                     
                                    template_id : questionUpdate.template_id,

                                }
                            
                           if(templateInformation.status!='saved'){
                               $scope.questionInfo['impact'] = $rootScope.questionToUpdateInfo.majorChange? 'Major':'Minor';
                               $scope.questionInfo['question_version']= questionUpdate.question_version+1.0;
                            }
                              responseObject.update($scope.questionInfo).then(function(response){
                                  
                                  $rootScope.questionToUpdateInfo = {
                                        "header":"",
                                        "selectedHeader" : {},
                                        "subHeader":"",           
                                        "selectedSubHeader" :{},           
                                        "qestTyp" : "",           
                                        "mandatory" : false,  
                                        "question": "",
                                        "quesID":"",
                                        "version":"",
                                        "majorChange":false,
                                        "min":"",
                                        "max":""
                                  }; 
                                  $rootScope.editQuestion= false; 
                                  $scope.saveFlag = false;
                                  $rootScope.questionInfoChanged = false;
                                  $rootScope.questionupdateInfoChanged = false;
                                  $rootScope.questionupdateInfoChangedMinMax =false;
                                  localStorage.setItem('questionInfoChanged',false); 
                                  localStorage.setItem('questionupdateInfoChanged',false);
                                   localStorage.setItem('questionupdateInfoChangedMinMax',false);                                 
                                  $scope.getQuestionUnderTemplate(true);
                                  // $scope.modalTitle = "Success";
                                  // $scope.modalContent = "Question is updated succesfully";
                                  // $scope.openModel("alertHandle");                                
                                 
                                 
                              },function(error){                                            
                              });
                                    
                                  
                              },function(error){                                       
                               
                                   
                              });
                        },function(error){                                
                             
                        });

               },function(error){    
                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                $scope.openModel("connectionError");                             
                     
                });
              },function(error){  
                  $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                  $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
                  $scope.openModel("connectionError");                               
                   
              });
    
          }

          }catch(e){
            $rootScope.shouldShowTyreImg = false;
          }
      }


      $scope.cancelEditing = function(question_id){


        for(index1 in $rootScope.groupedQuestionsArray){
          var obj = $rootScope.groupedQuestionsArray[index1];
          for(index2 in obj){
            var obj1= obj[index2];
            for(i=0;i<obj1.length;i++){
              if(obj1[i].question_id==question_id){
                  obj1[i].minimum_value=$rootScope.questionToUpdateInfo.min;
                  obj1[i].maximum_value=$rootScope.questionToUpdateInfo.max;
              }
            }
          }
        }
        $rootScope.questionToUpdateInfo = {
              "header":"",
              "selectedHeader" : {},
              "subHeader":"",           
              "selectedSubHeader" :{},           
              "qestTyp" : "",           
              "mandatory" : false,  
              "question": "",
              "quesID":"",
              "version":"",
              "majorChange":false,
              "min":"",
              "max":""
        }; 
        $rootScope.editQuestion= false; 
        $rootScope.questionupdateInfoChanged= false;
        $rootScope.questionupdateInfoChangedMinMax= false;
        
        localStorage.setItem('questionupdateInfoChanged',false);
             localStorage.setItem('questionupdateInfoChangedMinMax',false);
        //if user dont save changed question data we are resetting original data back
        $rootScope.questionsOfTemplate= $rootScope.tempQuestionsOfTemplate;

                  

      };

    }catch(e){
      $rootScope.shouldShowTyreImg = false;
    }

      
});
angular.module('dealerAudit').directive('lowerThan', [
  function() {

    var link = function($scope, $element, $attrs, ctrl) {

      var validate = function(viewValue) {
        var comparisonModel = $attrs.lowerThan;
        
        if(!viewValue || !comparisonModel){
          // It's valid because we have nothing to compare against
          ctrl.$setValidity('lowerThan', true);
        }

        // It's valid if model is lower than the model we're comparing against
        ctrl.$setValidity('lowerThan', parseInt(viewValue) < parseInt(comparisonModel) || parseFloat(viewValue) < parseFloat(comparisonModel));
        return viewValue;
      };

      ctrl.$parsers.unshift(validate);
      ctrl.$formatters.push(validate);

      $attrs.$observe('lowerThan', function(comparisonModel){
        return validate(ctrl.$viewValue);
      });
      
    };

    return {
      require: 'ngModel',
      link: link
    };

  }
]);


    