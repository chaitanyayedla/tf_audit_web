
/**
 * @controller: dealerAudit
 * @description This module watch for autologout .
 */

angular = require('angular');

angular.module('dealerAudit').run(function($rootScope, $timeout, $document,$state,$location,$uibModal,dealerAudit_ConstantsConst) {  
		// Timeout timer value
		//var TimeOutTimerValue = 60000;	
		var TimeOutTimerValue = 900000;	
		//var TimeOutTimerValue = 6000;	
		// Start a timeout
		var TimeOut_Thread = $timeout(function(){ LogoutByTimer() } , TimeOutTimerValue);
		var bodyElement = angular.element($document);	
		/// Keyboard Events
		bodyElement.bind('keydown', function (e) { TimeOut_Resetter(e) });	
		bodyElement.bind('keyup', function (e) { TimeOut_Resetter(e) });		
		/// Mouse Events	
		bodyElement.bind('click', function (e) { TimeOut_Resetter(e) });
		bodyElement.bind('mousemove', function (e) { TimeOut_Resetter(e) });	
		bodyElement.bind('DOMMouseScroll', function (e) { TimeOut_Resetter(e) });
		bodyElement.bind('mousewheel', function (e) { TimeOut_Resetter(e) });	
		bodyElement.bind('mousedown', function (e) { TimeOut_Resetter(e) });				
		/// Touch Events
		bodyElement.bind('touchstart', function (e) { TimeOut_Resetter(e) });		
		bodyElement.bind('touchmove', function (e) { TimeOut_Resetter(e) });			
		/// Common Events
		bodyElement.bind('scroll', function (e) { TimeOut_Resetter(e) });		
		bodyElement.bind('focus', function (e) { TimeOut_Resetter(e) });	

		$rootScope.openModel = function(value) {
		    $rootScope.modalInstance = $uibModal.open({
		
				animation: true,
				templateUrl: '/partials/alertpopup.html',
				controller: 'modelController',
				size: 'sm',
				scope: $rootScope,
				// Prevent closing by clicking outside or escape button.
				backdrop: 'static',
				keyboard: false
			});
			
			$rootScope.modalInstance.modalValue = value;
		};
		function LogoutByTimer()
		{
			
			var path=$location.path();
			ISession.getInstance().then(function(session) {
					session.logout();
					$rootScope.logoutFlag = true;					
					localStorage.removeItem('sessionObj');
					$state.go('login');
				});
			$rootScope.autoLogoutFlag = true;
			
			//below statement is required for only browser button handling .
			localStorage.removeItem('sessionObj');
				// to not to show auto logged out popup if he is already in login page
				if(path!='/login'){
					$rootScope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
					$rootScope.modalContent = dealerAudit_ConstantsConst.autoLogoutMsg;
					$rootScope.openModel('AutoLogout');
				}
			///////////////////////////////////////////////////
			/// redirect to another page(eg. Login.html) here
			///////////////////////////////////////////////////
		}	
		function TimeOut_Resetter(e)
		{				
			/// Stop the pending timeout
			$timeout.cancel(TimeOut_Thread);		
			/// Reset the timeout
			TimeOut_Thread = $timeout(function(){ LogoutByTimer() } , TimeOutTimerValue);
		}	
});












































 