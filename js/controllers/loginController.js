/**
 * @controller: loginController
 * @description This module includes login functionalities  
 */

angular = require('angular');

angular.module('dealerAudit').controller('loginController',function($scope,$window, $state, $cookies,$cookieStore, $rootScope, $uibModal,dealerAudit_ConstantsConst,dealerAudit_AssetsConst,$q) {
	
	try {

		$scope.logo = dealerAudit_AssetsConst.logo;

		$scope.username = "";
		$scope.password = "";
		$scope.username = $cookies.userame;
		$scope.password = $cookies.password;
		$rootScope.session = {};
		$scope.appKey = dealerAudit_ConstantsConst.AppKey;
		$scope.secretKey = dealerAudit_ConstantsConst.Secret;
		$scope.hybrisLocalSonataInstance = dealerAudit_ConstantsConst.HybrisLocalSonataInstance;

		$scope.ttsUsersInDB=[];
		$scope.ttsUsersFromService=[];
		var ttsUsersInDB=[];
		var ttsUsersFromService =[];

		//angular.element('#loading').hide();
		$scope.shouldShowTyreImg = false;
		if($scope.password != 'null' || $scope.password != undefined) {
			$scope.rememberCookie = false;
		}
		var checkForNullOrUndefined = function(variableCheck) {
			if(variableCheck != null && variableCheck != undefined && variableCheck != '') {
				return true;
			} else {
				return false;
			}

		};

	/**
     * @function sessionInitialize
     * @description function to get the instance on page load, instance is used for api calls.     
     */

	    $scope.sessionInitialize= function(){
		 
		 	ISession.getInstance().then(function(session) {

		 		$rootScope.session = session;
	 		}, function(error) {	

	 			$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
				$scope.modalContent = dealerAudit_ConstantsConst.connectionError;
				$scope.openModel("Login");			
							
			});
		};

	/**
     * @function open
     * @description function to open the bootstrap confirmation popup model .     
     */

		$scope.open = function(value) {
			$scope.modalInstance = $uibModal.open({
				animation: true,
				templateUrl: '/partials/confirmationmodal.html',
				//controller: 'ModalController',
				size: 'sm',
				scope: $scope,
				// Prevent closing by clicking outside or escape button.
				backdrop: 'static',
				keyboard: false
			});

			$scope.modalInstance.modalValue = value;
		};

	/**
     * @function openModel
     * @description function to open the bootstrap alert window model .     
     */
		$scope.openModel = function(value) {
		    $scope.modalInstance = $uibModal.open({
		
				animation: true,
				templateUrl: '/partials/alertpopup.html',
				controller: 'modelController',
				size: 'sm',
				scope: $scope,
				// Prevent closing by clicking outside or escape button.
				backdrop: 'static',
				keyboard: false
			});
			
			$scope.modalInstance.modalValue = value;
		};

	/**
     * @function login
     * @description function to validate the credentials and allow to login to application.     
     */
		
		$scope.login = function(username, password) {
		
		try{
			
				//angular.element('#loading').show();
				$scope.shouldShowTyreImg = true;
				$rootScope.logoutFlag = false;
				$scope.password = password;	
				$cookies.userame = username;							
				if($scope.rememberCookie) {							
					
					$cookies.password = password;											
				}
				
				if(checkForNullOrUndefined(username) && checkForNullOrUndefined(password)) {
					
					//ISession.getInstance().then(function(session) {	
						
						
						$rootScope.session.login(username, password, null, null, null, null, $scope.appKey).then(function(response) {
							$scope.status = 'sucess';
							$rootScope.UserName = username;
							localStorage.setItem('sessionObj',JSON.stringify($rootScope.session));
							$rootScope.checkFlag=undefined;
							$rootScope.autoLogoutFlag = undefined;
							//even after refreshing browser to get the user name maintaining in localStorage
							localStorage.setItem('userName',$rootScope.UserName);								

								 var filterParams = {
			                      "tam_name": $rootScope.UserName		                              
			                      };
								 $rootScope.session.getClientObjectType("tam_master").then(function(clientObjectType){
	                                clientObjectType.search("tam_master",0,10000, filterParams, null, null, null, null, null, false).then(function(response) {
	                                	
	                                	 //$scope.shouldShowTyreImg = false
	                                	 //$state.go('home');
	                                	 localStorage.setItem('loginFirstTime',1);

		                                	var filtParams = {
												"endpoint" : "v2/auditapp/TF_AUDIT_USER",
												"query" : "{}",
												"method" : "GET"
											};
											$rootScope.session.getClientObjectType("ttsUsersList").then(function(ttsUsersListclientObjectType){
											 		
						                            ttsUsersListclientObjectType.search("ttsUsersList",0,1000, filtParams, null, null, null, null, null, true).then(function(response) {
						                            	
						                            	
						                            	var responseData = response[0].data.users
						                            	//TTS webservice response data
						                            	var ttsUsersListData  = [];
						                            	for(i=0;i<responseData.length;i++){
						                            		ttsUsersListData.push(responseData[i]);
						                            	}
														

					                            		var ttsRecords=[];
														for(i=0;i<ttsUsersListData.length;i++){

															var ttsUserInfo = {
																				email: "",
																				first_name: "",
																				last_name: "",
																				full_name:"",																	
																				tts_name: ""																	
																			  }; 

															if(ttsUsersListData[i].email){
																ttsUserInfo.email = ttsUsersListData[i].email;
															}	
															if(ttsUsersListData[i].fullName){
																ttsUserInfo.full_name = ttsUsersListData[i].fullName;
															}
															if(ttsUsersListData[i].userId){
																ttsUserInfo.tts_name = ttsUsersListData[i].userId
															}																						
															
															ttsRecords.push(ttsUserInfo);												

														}
														$scope.getTTS_UsersFromDB(ttsRecords);
														
														
														$scope.shouldShowTyreImg = false;
	                                					$state.go('home');
														

						                            },function(error){	
						                            	$scope.shouldShowTyreImg = false;
														$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
														$scope.modalContent = "TTS users are not available";
														$scope.openModel("Login");	
						                            	
						                            });
					                         },function(error){		
					                         		$scope.shouldShowTyreImg = false;
													$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
													$scope.modalContent = dealerAudit_ConstantsConst.connectionError;
													$scope.openModel("Login");	
					                         		

					                         });

	                                	
	                                },function(error){	
	                                	//angular.element('#loading').hide();
	                                	$scope.shouldShowTyreImg = false
	                                	$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
										$scope.modalContent = dealerAudit_ConstantsConst.loginFailedUnAuthorizedMsg;
										$scope.openModel("Login");
	                                	
	                                });
	                             },function(error){	
	                             	//angular.element('#loading').hide();
	                             	$scope.shouldShowTyreImg = false;
	                             	$scope.$apply();
	                             	$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
									$scope.modalContent = dealerAudit_ConstantsConst.connectionError;
									$scope.openModel("Login");	
			                    	
			                    });
								
							
						}, function(error) {
							//angular.element('#loading').hide();
							$scope.shouldShowTyreImg = false;
							$scope.password = "";
							$scope.status = 'failed';
							var errorCode='';
							if (typeof(error) === "object") {

				                if (typeof(error.code) != "undefined" || error.code != null) {
				                    errorCode = error.code;
				                }

				                if (typeof(error.data) != "undefined" || error.data != null) {
				                    errorCode = error.data.code;
				                }

				                if (typeof(error.status) != "undefined" || error.status != null) {
				                    errorCode = error.status;
				                }

				            } else {
				                errorCode = error;
				            }
				            $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
							$scope.modalContent = dealerAudit_ConstantsConst.generalLoginError;
							if (errorCode.toString().length) {

								if(errorCode == 0){

								}else if(errorCode == 3){
									$scope.modalContent = dealerAudit_ConstantsConst.loginFailedUnAuthorizedMsg;
								}else if(errorCode == 5){
									$scope.modalContent =dealerAudit_ConstantsConst.loginFailedDueToCode5;
								}else if(errorCode == 11){
									$scope.modalContent =dealerAudit_ConstantsConst.loginFailedDueToCode11;
								}else if(errorCode == 13){
									$scope.modalContent =dealerAudit_ConstantsConst.loginFailedDueToCode13;
								}else if(errorCode == 51){

								}else if(errorCode == 400){

								}else if(errorCode == 401){
									$scope.modalContent =dealerAudit_ConstantsConst.loginFailedDueToCode401;
								}else if(errorCode == 503 ||errorCode == 504){
									$scope.modalContent =dealerAudit_ConstantsConst.loginFailedDueToCode501;
								}else{
									$scope.modalContent =dealerAudit_ConstantsConst.loginFailedDueToSomeError;
								}

						    }
						
							$scope.openModel("Login");

						});

						/*}, function(error) {	
							$scope.shouldShowTyreImg = false;
				 			$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
							$scope.modalContent = dealerAudit_ConstantsConst.connectionError;
							$scope.openModel("Login");			
								
						});*/
					
				} else {
					//angular.element('#loading').hide();
					$scope.shouldShowTyreImg = false;
					if(checkForNullOrUndefined(username)) {
						$scope.buttonDisabled = false;
					} else if(checkForNullOrUndefined(password)) {
						$scope.buttonDisabled = false;
					}
				}

			} catch(e) {
				//angular.element('#loading').hide();
				$scope.shouldShowTyreImg = false;
				
			}
		};



		 var generateHashPropertyForTTSRecords = function(array){
		 
			  var hash = {};
			  for (var i=0; i<array.length; i++){
			    hash[ array[i].tts_name ] = true;
			  }

			 
			  return hash;
		}

		/**
		 *
		 */

		 $scope.getTTS_UsersFromDB = function(ttsUsersFromService){

		 	
		 	ttsUsersInDB=[];
		 	ttsUsersFromService = ttsUsersFromService;


		 	$rootScope.session.getClientObjectType("TTS_master").then(function(clientObjectType){
	                clientObjectType.list(0,100000, null, true).then(function(response) {

	                	 for(i=0;i<response.length;i++){

							ttsUsersInDB.push(response[i].getData());
	                	 }  

	                	$scope.insert_deltaData(ttsUsersInDB,ttsUsersFromService);
	                	

	               	},function(error){

	               		$scope.insert_deltaData(ttsUsersInDB,ttsUsersFromService);
	               		
	               		
	               	});	
	        },function(error){
	        	$scope.shouldShowTyreImg = false
	        	//return error;
	        });
		 	
			
					
		 }

		 $scope.insert_deltaData = function(ttsUsersInDB,ttsUsersFromService){
		 		try{

		 				  //contains tts users available in DB
		 	              $scope.ttsUsersInDB = ttsUsersInDB; 

		 	              //contains tts users returned from WS
		 	              $scope.ttsUsersFromService = ttsUsersFromService;

	                	  

	                	  //Delta gives the new TTS users coming from webservice which needs to be added in DB too
						  var hash = generateHashPropertyForTTSRecords($scope.ttsUsersInDB);
						  var newUsersInService = [];
						  for (var i=0; i<$scope.ttsUsersFromService.length; i++){
						    var value = $scope.ttsUsersFromService[i].tts_name;
						    if ( !hash[value]){
						      newUsersInService.push($scope.ttsUsersFromService[i]);
						    }
						  }

						  //below objects have been added in webservice
						  

						  //Delta gives the TTS users deleted from rest api which needs to be deleted in DB too
						  var hash = generateHashPropertyForTTSRecords($scope.ttsUsersFromService);
						  var deletedUsers = [];
						  for (var i=0; i<$scope.ttsUsersInDB.length; i++){
						    var value = $scope.ttsUsersInDB[i].tts_name;
						    if ( !hash[value]){
						      deletedUsers.push($scope.ttsUsersInDB[i]);
						    }
						  }

						  //Below objects have been reomved in webservice
						 
						
						  //Deleting records from DB which are removed in WS
						 if(deletedUsers.length>0){
						  for(i=0;i<deletedUsers.length;i++){
						  	(function(ind) { setTimeout(function(){	

						  		var ttsToDelete={
				              		'tts_id':deletedUsers[ind].tts_id,				              		
				              	} 
				              	$rootScope.session.getClientObjectType("TTS_master").then(function(clientObjectType){
				              		clientObjectType.get(ttsToDelete.tts_id).then(function(responseObject){ 

					              		    responseObject.remove().then(function(response){
					              			

					              		},function(error){
					              			//return error;
					              			$scope.shouldShowTyreImg = false
					              			
					              		});
					              	},function(error){
					              		//return error;
					              		$scope.shouldShowTyreImg = false

				              		});
				              	},function(error){
				              		//return error;
				              		
				              		$scope.shouldShowTyreImg = false
				              	});

						  	}, 500 * (ind) ); })(i); 
						  }
						}
						/*else{
							return true;
						}*/

						   //Inserting new records into DB which are newly added in WS
						if(newUsersInService.length>0){
						  for(j=0;j<newUsersInService.length;j++){
						  	(function(index) { setTimeout(function(){	

						  		var ttsRecodeToCreate={
				              		'first_name':newUsersInService[index].first_name,
				              		'last_name':newUsersInService[index].last_name,
				              		'full_name':newUsersInService[index].full_name,
				              		'email':newUsersInService[index].email,
				              		'tts_name':newUsersInService[index].tts_name,				              					              		
				              	} 
				              	
				              	$rootScope.session.getClientObjectType("TTS_master").then(function(clientObjectType){				              		
				              		clientObjectType.create(ttsRecodeToCreate).then(function(response){ 

					              		
					              		
					              		//After adding all the TTS users reuring flag
					              		/*if(index+1 == newUsersInService.length){
					              			return true;
					              		}*/
					              	},function(error){
					              		//return error;
					              		
					              		$scope.shouldShowTyreImg = false
				              		});
				              	},function(error){
				              		//return error;
				              		$scope.shouldShowTyreImg = false

				              	});

						  	}, 500 * (index) ); })(j); 
						  }
 
						}
						/*else{
							return true;
						}*/  

			}catch(e){

				$scope.shouldShowTyreImg = false
				//return e;
				
			}
		 }


	} catch(e) {
		//angular.element('#loading').hide();
		$scope.shouldShowTyreImg = false;
	
	}
});




