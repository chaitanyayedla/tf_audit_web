/**
 * @controller: dealerController
 * @description This module includes functionalities for creating dealers and listing all the dealers created by logged in manager.
 */

angular = require('angular');

angular.module('dealerAudit').controller('dealerController',function($scope,$state,$stateParams,$rootScope,$location,$window,$uibModal,$timeout,dealerAudit_ConstantsConst) {

  try{


	/**
     * @function dealer
     * @description function to navigate to Truckforce dealers page.     
     */
    $scope.dealer = function() {
			
			$state.go('dealer');
	};
	/**
     * @function audit
     * @description function to navigate to Audit's page.     
     */
	$scope.audit = function() {			
			$state.go('currentyearaudits');
	};
	/**
     * @function gotoHome
     * @description function to navigate to homepage(Dashboard) page.     
     */
	/*$scope.gotoHome = function(){
		$state.go('home');
    };*/
   /**
     * @function question
     * @description function to navigate to Manage Audit templates page.     
     */    
     $scope.question = function(){
       $state.go('questionnaire');
    }; 
   
	/**
     * @function template
     * @description function to navigate to template list page.     
     */
    $scope.template = function(){
       $state.go('templateList');
    }; 

    /**
     * @function goTolocation
     * @description function to navigate to Manage locations page.     
     */
    $scope.goTolocation = function(){
    	 $state.go('location');
    }

 	/**
     * @function navigateToHome
     * @description function checkk for all the fields whether they have been modified, if so ask for confirmation before navigating to other page.     
     */
    $scope.navigateToHome = function(){
     	
		 
		$state.go('home');
		  
    };

    /**
     * @function signOut
     * @description function for logout.     
     */
	$scope.signOut = function() {
			try {
				
					/*$rootScope.session.logout();
					$rootScope.logoutFlag = true;				
					localStorage.removeItem('sessionObj');

					$state.go('login');*/
					$scope.navigateToDifferentTab('login');
				
			} catch(e) {
				
			}
		
		}
    //based on the window size showing the number of records per page for dealer list
     if(screen.width <=1000){
	    $scope.numberOfrows =  6;
	  }          
	  if(screen.width >1000 && screen.width <1500){
	    $scope.numberOfrows =  8;
	  }
	 
	  if(screen.width >1500 ){
	    $scope.numberOfrows =  11;
	  }

	  $rootScope.active=false;
	  var currentLoc = $location.path();
    
      if(currentLoc == '/dealer/createdealerform/create' || currentLoc == '/dealer/createdealerform/update'){
        $rootScope.active=false;       
      }else{
        $rootScope.active=true;         
      }
		$scope.tempdealerList=[];
		$scope.dealerList =[];
		$scope.noDetails = false;

		var editModeFlag = false;
		$scope.saveDealerFlag = false;
		$scope.searchNotFound = false;	
		$scope.noDealerAddedForManager = false;	
		$scope.userName = $rootScope.UserName;
	    
	    if($rootScope.networkValues == null ||$rootScope.networkValues ==undefined){
	    	$rootScope.networkValues = [];
	    }
	     if($rootScope.tts_nameValues == null ||$rootScope.tts_nameValues ==undefined){
	    	$rootScope.tts_nameValues = [];
	    }

	    
	  
		$scope.emailFormat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.?)+[a-zA-Z]*))$/;
		
		
		//$scope.phoneFormat = /^[\+\-]?[(]?[\+\-]?[\s\.\/\,]?[0-9\/]{1,4}[\s\.\/\,]?[\+\-]?[\s\.\/\,]?[)]?[\+\-]?[\s\.\/\,]?[(]?[\+\-]?[\s\.\/\,]?[0-9\/]{1,4}[\s\.\/\,]?[\+\-]?[\s\.\/\,]?[)]?[\s\.\/\,]?[\+\-]?[\s\.\/\,]?[0-9\/]{3,6}[\s\.\/\,]?[\+\-]?[\s\.\/\,]?[0-9\/]{1,12}$/;
		
		$scope.phoneFormat = /^[(]?[\+\-]?[\s\.\/\,\(\)0-9]{10,30}[)]?$/;

		if($rootScope.isEditDealer == null || typeof($rootScope.isEditDealer) == "undefined"){
			$rootScope.isEditDealer = false;
		}
		if($rootScope.isCreateDealer == null || typeof($rootScope.isCreateDealer) == "undefined"){
			$rootScope.isCreateDealer = false;
		}
		if($rootScope.isUpdateDealer == null || typeof($rootScope.isUpdateDealer) == "undefined"){
			$rootScope.isUpdateDealer = false;
		}
		if($rootScope.shouldShowTyreImg == null || typeof($rootScope.shouldShowTyreImg) == "undefined"){
			$rootScope.shouldShowTyreImg = false;
		}
		if($rootScope.popUpActive == null || typeof($rootScope.popUpActive) == "undefined"){
			$rootScope.popUpActive = false;
		}
		if($scope.checkBox == null || typeof($scope.checkBox) == "undefined"){
			$scope.checkBox= false;
		}
		if($scope.fosChecked == null || typeof($scope.fosChecked) == "undefined"){
			$scope.fosChecked= true;
		}
		if($rootScope.isCreate == null || typeof($rootScope.isCreate)=='undefined'){
			$rootScope.isCreate = false;
		}
		if($rootScope.tempArrayofDealerInfo == null || typeof($rootScope.tempArrayofDealerInfo)=='undefined'){
		
			$rootScope.tempArrayofDealerInfo={
						"holding_name": "",
						"address": "",
						"pos_code": "",
						"holding_code": null,
						"modified_date": "",
						"tts_name": "",
						"network": "",
						"city_name": "",
						"payer_code": "",
						"modified_time": "",						
						"province": "",
						"phone": "",
						"post_code": "",
						"country_name": "",
						"dealer_name": "",
						"email": "",
						"dealer_id": ''
					};
		}

		if($rootScope.dealerListElements==null || typeof($rootScope.dealerListElements)=='undefined'){
			
			$rootScope.dealerListElements={
						"holding_name": "",
						"address": "",
						"pos_code": "",
						"holding_code": null,
						"modified_date": "",
						"tts_name": "",
						"network": "",
						"city_name": "",
						"payer_code": "",
						"modified_time": "",						
						"province": "",
						"phone": "",
						"post_code": "",
						"country_name": "",
						"dealer_name": "",
						"email": "",
						"dealer_id": ''
					};
		}
		/*//creating json object , required to bind elements to view
		if($scope.createFlag == null || typeof($scope.createFlag) == "undefined"){
					$scope.createFlag = true;	
					
					$rootScope.dealerListElements={
						"holding_name": "",
						"address": "",
						"pos_code": "",
						"holding_code": null,
						"modified_date": "",
						"tts_name": "",
						"network": "",
						"city_name": "",
						"payer_code": "",
						"modified_time": "",						
						"province": "",
						"phone": "",
						"post_code": "",
						"country_name": "",
						"dealer_name": "",
						"email": "",
						"dealer_id": ''
					};

					 $rootScope.tempArrayofDealerInfo={
						"holding_name": "",
						"address": "",
						"pos_code": "",
						"holding_code": null,
						"modified_date": "",
						"tts_name": "",
						"network": "",
						"city_name": "",
						"payer_code": "",
						"modified_time": "",						
						"province": "",
						"phone": "",
						"post_code": "",
						"country_name": "",
						"dealer_name": "",
						"email": "",
						"dealer_id": ''
					};
					
			}*/
			
	function getuid() {
	        function s4() {
	            return Math.floor((1 + Math.random()) * 0x10000)
	        }
	          return s4() + s4();
	 }	
	
	 var pagenum = localStorage.getItem('dealerlistPagination');

		// tracking pagination page number in current year audits tab
		if(pagenum!=null){
		     $scope.dealerlistpagination = {
		       current: pagenum
		     };
		}else{
		     $scope.dealerlistpagination = {
		       current: 1
		     };
		}

	 $scope.paginationChangedinDealerlist = function(newPage){              
	      localStorage.setItem('dealerlistPagination',newPage);            
	  }


	/**
     * @function googleAPIforCountry
     * @description function to autocomplete the cities which user enter in form     
     */	
	 $scope.googleAPIforCountry =function(){			
	  		var input =document.getElementById('autocompleteCountry'); 
			var options = {
				types: ['(regions)'],
			    componentRestrictions: {}
			};

			$scope.autocomplete = new google.maps.places.Autocomplete(input, options);			
	  	}
	/**
     * @function googleAPIforCity
     * @description function to autocomplete the cities which user enter in form     
     */	
		$scope.googleAPIforCity = function(){

	  		var input =document.getElementById('autocompleteCity'); 
			var options = {
				types: ['(cities)'],
			    componentRestrictions: {}
			};

			$scope.autocomplete = new google.maps.places.Autocomplete(input, options);
	  	}


	  if($rootScope.formChanged==null || typeof($rootScope.formChanged)=='undefined'){
	  	$rootScope.formChanged = false;	
	  }

	 $scope.inputFormChanged = function(){

	 		$rootScope.formChanged = false;	
	 		if(!$rootScope.isEditDealer){

							
				for (var key in $rootScope.dealerListElements){
			        var attrName = key;
			        var attrValue = $rootScope.dealerListElements[key];
			        if(attrValue != '' && attrValue != null && attrValue != false){
			        	$rootScope.formChanged = true;
			        }
			    }			
				
			}else{
				    var tempArrayLength = Object.keys($rootScope.tempArrayofDealerInfo).length;									
					delete $rootScope.dealerListElements['$$hashKey'];				
					var dealerInfoLength = Object.keys($rootScope.dealerListElements).length;
					
					for(i=0; i<tempArrayLength; i++){				
						if(($rootScope.tempArrayofDealerInfo[i] == undefined) || ($rootScope.tempArrayofDealerInfo[i] == null)){
							$rootScope.tempArrayofDealerInfo[i] == '';
						}
					}						
					
					for(i=0; i<dealerInfoLength; i++){			
						if(($rootScope.dealerListElements[i] == undefined) || ($rootScope.dealerListElements[i] == null)){
							$rootScope.dealerListElements[i] == '';
						}
					}

					if(JSON.stringify($rootScope.dealerListElements) === JSON.stringify($rootScope.tempArrayofDealerInfo)){
						
						$rootScope.formChanged = false;	
					}else{
						
						$rootScope.formChanged = true;	
					}	
			}

	 }
	/**	
     * @function open
     * @description function to open the bootstrap modal popup containing ok and cancel buttons for user confirmations    
     */
		$scope.open = function(value) {
		    $scope.modalInstance = $uibModal.open({
		
				animation: true,
				templateUrl: '/partials/confirmationmodal.html',
				controller: 'dealerController',
				size: 'sm',
				scope: $scope,
				// Prevent closing by clicking outside or escape button.
				backdrop: 'static',
				keyboard: false
			});
			
			$scope.modalInstance.modalValue = value;
		};

	/**
     * @function openModel
     * @description function to open the bootstrap modal popup containing ok  buttons to alert user   
     */
		$scope.openModel = function(value) {
		    $scope.modalInstance = $uibModal.open({
		
				animation: true,
				templateUrl: '/partials/alertpopup.html',
				controller: 'modelController',
				size: 'sm',
				scope: $scope,
				// Prevent closing by clicking outside or escape button.
				backdrop: 'static',
				keyboard: false
			});
			
			$scope.modalInstance.modalValue = value;
		};
	/**
     * @function ok
     * @description function to perform actions on click of ok button on confirmation popup   
     */
		$scope.ok = function() {
			
			if($scope.modalInstance.modalValue == "Confirmation"){
					
					
				    $scope.clearAllFields();
					
					$rootScope.isEditDealer = false;
					$scope.modalInstance.dismiss();
					
			}
			else if($scope.modalInstance.modalValue == "Logout"){
				
					$rootScope.session.logout();
					$rootScope.logoutFlag = true;
					$state.go('login');					
					//below statement is required for only browser button handling .
					localStorage.removeItem('sessionObj');
				
				
			}
			
			else if($scope.modalInstance.modalValue == "Error"){
					$scope.modalInstance.dismiss();
					$scope.saveDealerFlag = false;
			}
			
			else if($scope.modalInstance.modalValue == "DeleteConfirm"){
					$scope.deleteDealer($scope.dealer_idToDelete);
					$scope.modalInstance.dismiss();
			}
			else if($scope.modalInstance.modalValue == "Login"){
						$scope.modalInstance.dismiss();
			}
			else if($scope.modalInstance.modalValue == "handleEditMode"){
				
				$rootScope.isEditDealer = false;
				$state.go('home');
				
			}
			else if($scope.modalInstance.modalValue == "handleCreateNewDealer"){
				$rootScope.formChanged=false;
				$scope.clearAllFields();	
			//	$rootScope.active=false;			
				$state.go('dealer.createdealer', { 'createOrUpdate': "create", 'dealerId': '' });
				$scope.modalInstance.dismiss();
			}else if($scope.modalInstance.modalValue == "handlecreatePageToDealerPage"){
				$rootScope.formChanged=false;  
				$rootScope.modelOpened = false;
				$rootScope.isEditDealer = false;
				$scope.clearAllFields();
				$scope.listdealer(false);
				//$state.go('dealer.listdealer');								
				$scope.modalInstance.dismiss();
			}
			else if($scope.modalInstance.modalValue == "logoutForBackButn"){
			//	$rootScope.checkFlag=true;				
				$scope.modalInstance.dismiss();
				$state.go('login');
				
			}
			else if($scope.modalInstance.modalValue == "AutoLogout"){
				//$rootScope.autoLogoutFlag = undefined;
				$scope.modalInstance.dismiss();
				
				
			}else if($scope.modalInstance.modalValue == "handleSavebtnInerror"){
				//$rootScope.autoLogoutFlag = undefined;
				$scope.modalInstance.dismiss();				
				
			}if($scope.modalInstance.modalValue == "navigateToOtherTab"){
					$rootScope.formChanged=false;
                    if($scope.navigateToLocation == 'login'){
                      $rootScope.logoutFlag = true;       
                      localStorage.removeItem('sessionObj');
                    }
					$scope.clearAllFields();
                    $state.go($scope.navigateToLocation);
                                        
                    $scope.modalInstance.dismiss('cancel');                            
                   

             }if($scope.modalInstance.modalValue == "navigateToBackTab"){
					$rootScope.formChanged=false;  
					$rootScope.modelOpened = false;                 
					$scope.clearAllFields();                                                          
                    $scope.modalInstance.dismiss('cancel');                            
                   

             }
			else{
				 return;
			}
			
			
		};
		/**
     * @function cancel
     * @description function to perform actions on click of cancel button on confirmation popup   
     */
		 $scope.cancel = function() {
			
		    
			 if($scope.modalInstance.modalValue == "Confirmation"){
				 	$scope.modalInstance.dismiss('cancel');
					$rootScope.isEditDealer = true;
					
					//return false;
			 }
			 else if($scope.modalInstance.modalValue == "Logout"){
 				
 				
 			}else if($scope.modalInstance.modalValue == "logoutForBackButn"){
 				$rootScope.checkFlag=undefined;	
				$scope.modalInstance.dismiss();
				$state.go('home');
				
			}
				$rootScope.modelOpened = false;
			 $scope.modalInstance.dismiss('cancel');	 
			//alert("Cancel from main controller");
			
		 };
		 
	/**
     * @function applyFilter
     * @description function to filter the data based on entered characters in search bar .  
     */
		 $scope.search = {}; 
		 $scope.applyFilter = function(dealer){		 
			 	
			 	if(typeof($rootScope.userName) == "undefined"){
							$rootScope.userName = "";
				}
				if(dealer.city_name == null){
						dealer.city_name ="";
				}			 	
					
			 	if (!$scope.search.searchFilter ||(dealer.dealer_name.indexOf($scope.search.searchFilter) != -1)||(dealer.dealer_name.toLowerCase().indexOf($scope.search.searchFilter.toLowerCase()) != -1)||(dealer.dealer_name.toUpperCase().indexOf($scope.search.searchFilter.toUpperCase()) != -1)||
			 	 (dealer.city_name.indexOf($scope.search.searchFilter) != -1)|| (dealer.city_name.toLowerCase().indexOf($scope.search.searchFilter.toLowerCase()) != -1)|| (dealer.city_name.toUpperCase().indexOf($scope.search.searchFilter.toUpperCase()) != -1)||
			 	 (dealer.country_name.indexOf($scope.search.searchFilter) != -1) || (dealer.country_name.toLowerCase().indexOf($scope.search.searchFilter.toLowerCase()) != -1)|| (dealer.country_name.toUpperCase().indexOf($scope.search.searchFilter.toUpperCase()) != -1) ){
				 		$scope.searchNotFound = false;
				 		return true;

				}else{
					$scope.noDetails = true;
				}

				return false;
				
				//$state.reload();
		 }
		 
	/**
     * @function fosVendorCode
     * @description function to disable the check box when fosVendorCode is selected from dropdown .  
     */ 
		 $scope.fosVendorCode = function(){
		 		 	
		 	if($rootScope.dealerListElements.pos_code ==''||$rootScope.dealerListElements.pos_code == 'undefined'||$rootScope.dealerListElements.pos_code ==null){
		 		$scope.fosChecked= true;
		 	}else{
		 		$scope.fosChecked= false;
		 	}
		 }
	/**
     * @function fosVendorCodeClicked
     * @description function to clear the data when fosVendorCode checkbox clicked  
     */ 
	 $scope.fosVendorCodeClicked = function(){
	 		 	
	 	
	 	if($scope.fosChecked){
	 		$rootScope.dealerListElements.pos_code ='';
	 	}
	 }
    /**
     *@function getTtsAndNetworkListFromDb
     *@description function get the list of tts and networks required while creating the dealer
     */

     $scope.getTtsAndNetworkListFromDb = function(){
     	try{
     		ISession.getInstance().then(function(session) {
     			$rootScope.session = session;
     			if($rootScope.networkValues.length==0){
	     			$rootScope.networkValues=[];
	     			$rootScope.session.getClientObjectType("network").then(function(clientObject){
	     				clientObject.list(0,100000).then(function(response){

								for(i=0;i<response.length;i++){
									$rootScope.networkValues.push(response[i].data.network_name);
								}
								localStorage.setItem('networkValues', JSON.stringify($rootScope.networkValues));
								
	     				}, function(error) {				
										
						});

	     			}, function(error) {	
	     				$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
		                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
		                $scope.openModel("connectionError");			
								
					});
				}
     			if($rootScope.tts_nameValues.length==0){
	     			$rootScope.tts_nameValues=[];
	     			$rootScope.session.getClientObjectType("TTS_master").then(function(clientObjectType){
	     				
	     				var filterParams = {
	                      	"tam_name": $rootScope.UserName		                              
	                      };
	     				clientObjectType.search("TTS_master",0,10000, filterParams, null, null, null, null, null, true).then(function(response) {

								for(i=0;i<response.length;i++){
									$rootScope.tts_nameValues.push(response[i].data.tts_name);
								}
								
								localStorage.setItem('tts_nameValues', JSON.stringify($rootScope.tts_nameValues));
	     				}, function(error) {				
								
						});

	     			}, function(error) {	

	     				$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
		                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
		                $scope.openModel("connectionError");			
									
					});
	     		}
     	    }, function(error) {				
					
			});
				

     	}catch(e){
     		
     	}
     }

     
	/**
     * @function initializeDealer
     * @description function to initialize few functions on load based on edit dealer mode or create dealer mode .  
     */ 
		$scope.initializeDealer = function () {
			try{				
			
				ISession.getInstance().then(function(session) {
			 		
			 		$rootScope.session = session;

					// after refreshing the browser from any other page,again it should not navigate to dealerlist page so keeping a flag in localstorage to check
					var isFirstTime = localStorage.getItem('isFirstTime');
					var isIndealerListPage=localStorage.getItem('isIndealerListPage');
					var currentLocation = $location.path();
					if(isFirstTime == null || typeof(isFirstTime) == "undefined"){
						localStorage.setItem('isFirstTime', true);						
						
						$scope.listdealer(false);

						//$state.go('dealer.listdealer');

				    }//after refreshing if user is in dealer list page then call listdealer method again else no, so checking for location
				    else if(isIndealerListPage){
				    	if(currentLocation =='/dealer/dealerlist'||currentLocation =='/dealer'){				    		    		
				   		
				    	$scope.listdealer(false);
				    	//$state.go('dealer.listdealer');

				   		 }
				    }
				    if($rootScope.isCreateDealer){			    	
				    	
				    	$scope.listdealer(true);
				    	//$state.go('dealer.listdealer');

				    	$rootScope.isCreateDealer= false;
				    }	
				    if($rootScope.isUpdateDealer){				    	
				   		
				    	$scope.listdealer(true);
				    	//$state.go('dealer.listdealer');

				    	$rootScope.isUpdateDealer = false;
				    }	

				}, function(error) {	

					$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
	                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
	                $scope.openModel("connectionError");			
									
				});
		    } catch(error){			
				$rootScope.shouldShowTyreImg = false;
				
			}	    
			
		}
		$scope.dealer = function() {
				$state.go('dealer');
		};	


	/**
     * @function createdealer
     * @description function to create the dealer.
     * @params dealerlist elements 
     */ 	
		$scope.createdealer = function(dlrList) {

		try{
			$rootScope.isCreateDealer = true;
			$rootScope.isCreate= true;
			
			/*if($state.params.createOrUpdate == "create"){
		    	$scope.clearArray();
			}*/

			if(!$rootScope.isEditDealer){

				var createFlag = false;
				
				for (var key in $rootScope.dealerListElements){
			        var attrName = key;
			        var attrValue = $rootScope.dealerListElements[key];
			        if(attrValue != '' && attrValue != null && attrValue != false){
			        	createFlag = true;
			        }
			    }

				
				if(createFlag){							
						$scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
						$scope.modalContent = dealerAudit_ConstantsConst.discardChangesMsg;
						$scope.open("handleCreateNewDealer");							
				}else{
					//	$rootScope.active=false;
						$scope.clearArray();
						$state.go('dealer.createdealer', { 'createOrUpdate': "create", 'dealerId': '' });
							
				}
			}
			// Toggle the focus on the button.
			angular.element("listDealer").css('active','false');
			angular.element("addDealer").css('active','true');
			
			if($rootScope.isEditDealer){
				
					var tempArrayLength = Object.keys($rootScope.tempArrayofDealerInfo).length;	
								
					delete $rootScope.dealerListElements['$$hashKey'];
				
					var dealerInfoLength = Object.keys($rootScope.dealerListElements).length;
					
					for(i=0; i<tempArrayLength; i++){				
						if(($rootScope.tempArrayofDealerInfo[i] == undefined) || ($rootScope.tempArrayofDealerInfo[i] == null)){
							$rootScope.tempArrayofDealerInfo[i] == '';
						}
					}						
					
					for(i=0; i<dealerInfoLength; i++){			
						if(($rootScope.dealerListElements[i] == undefined) || ($rootScope.dealerListElements[i] == null)){
							$rootScope.dealerListElements[i] == '';
						}
					}

					if(JSON.stringify($rootScope.dealerListElements) === JSON.stringify($rootScope.tempArrayofDealerInfo)){
						
						editModeFlag = false;
					}else{
						
						editModeFlag = true;
					}		
						

					if(editModeFlag == false){	

					    $rootScope.isEditDealer = false;
						$scope.clearArray();			
						$scope.clearAllFields();
						$state.go('dealer.createdealer', { 'createOrUpdate': "create", 'dealerId': '' });
					}else{

						$scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
						$scope.modalContent = dealerAudit_ConstantsConst.discardChangesMsg;
						$scope.open("Confirmation");			
					}
							
			}

		 } catch(error){			
				$rootScope.shouldShowTyreImg = false;
			
		}	
		};
		
	/**
     * @function gotoHome
     * @description function to handle the changes done in creare dealer form if user wish to navigate to home page.  
     */ 
		$scope.gotoHome = function(){

	    try{
	    	

		 	/*if($rootScope.isCreate || $rootScope.isEditDealer){
				var tempArrayLength = Object.keys($rootScope.tempArrayofDealerInfo).length;				
					
					if($rootScope.isEditDealer){
					delete $rootScope.dealerListElements['$$hashKey'];
					
					}
					var dealerInfoLength = Object.keys($rootScope.dealerListElements).length;
					
					for(i=0; i<tempArrayLength; i++){				
						if(($rootScope.tempArrayofDealerInfo[i] == undefined) || ($rootScope.tempArrayofDealerInfo[i] == null)){
							$rootScope.tempArrayofDealerInfo[i] == '';
						}
					}						
					
					for(i=0; i<dealerInfoLength; i++){			
						if(($rootScope.dealerListElements[i] == undefined) || ($rootScope.dealerListElements[i] == null)){
							$rootScope.dealerListElements[i] == '';
						}
					}

				
					if(JSON.stringify($rootScope.dealerListElements) === JSON.stringify($rootScope.tempArrayofDealerInfo)){
					
						editModeFlag = false;
					}else{
						
						editModeFlag = true;
					}		
						

					if(editModeFlag == false){	
					    $scope.selectedTab = $scope.navigateToLocation;							
						$state.go('home');
						$rootScope.isEditDealer = false;

					}else{	
						angular.element("listDealer").blur();
						$scope.selectedTab = 'truckforcedealers';					
						$scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
						$scope.modalContent = dealerAudit_ConstantsConst.discardChangesMsg;

						$scope.open("navigateToOtherTab");			
					}

				}else{					
					$state.go('home');
					
				} 
*/


				/*if($rootScope.formChanged){
						event.preventDefault();						
						$scope.selectedTab = 'truckforcedealers';					
						$scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
						$scope.modalContent = dealerAudit_ConstantsConst.discardChangesMsg;

						$scope.open("navigateToOtherTab");
				}else{
					$scope.selectedTab = $scope.navigateToLocation;							
					$state.go('home');
					$rootScope.isEditDealer = false;
				}*/

				$scope.navigateToDifferentTab('home')

		} catch(error){			
				$rootScope.shouldShowTyreImg = false;
				
		}	
			
	};
	//calling from app.js
	$rootScope.browserBackbtnClicked = function(){
		$scope.gotoHome();
	}		

	/**
     * @function ValidateForm
     * @description function to validate the create dealer form before saving data 
     */ 
	 $scope.ValidateForm = function(){
		
	 	try{
			//if check box checkd for fos code then do not validate for fos required 		
	    	if($scope.fosChecked){
				if(($rootScope.dealerListElements.dealer_name == "" || typeof($rootScope.dealerListElements.dealer_name) == "undefined")||($rootScope.dealerListElements.address=="" || typeof($rootScope.dealerListElements.address) == "undefined")||($rootScope.dealerListElements.post_code=="" || typeof($rootScope.dealerListElements.post_code) == "undefined")||($rootScope.dealerListElements.city_name=="" || typeof($rootScope.dealerListElements.city_name) == "undefined")||($rootScope.dealerListElements.tts_name=="" || typeof($rootScope.dealerListElements.tts_name) == "undefined")){
					$rootScope.shouldShowTyreImg = false;					
					return false;
				}
				else{
					return true;
				}

		   	}else{

		   		if(($rootScope.dealerListElements.dealer_name == "" || typeof($rootScope.dealerListElements.dealer_name) == "undefined")||($rootScope.dealerListElements.address=="" || typeof($rootScope.dealerListElements.address) == "undefined")||($rootScope.dealerListElements.post_code=="" || typeof($rootScope.dealerListElements.post_code) == "undefined")||($rootScope.dealerListElements.city_name=="" || typeof($rootScope.dealerListElements.city_name) == "undefined")||($rootScope.dealerListElements.pos_code=="" || typeof($rootScope.dealerListElements.pos_code) == "undefined")||($rootScope.dealerListElements.tts_name=="" || typeof($rootScope.dealerListElements.tts_name) == "undefined")){
					$rootScope.shouldShowTyreImg = false;					
					return false;
				}
				else{
					return true;
				}


		   	}

		   	} catch(error){			
				$rootScope.shouldShowTyreImg = false;
				

			}	
	};
	/**
     * @function updateDealer
     * @description function to collect the selected dealer information when clicked on edit dealer
     */ 
	$scope.updateDealer = function(dlrList) {
	
	try{	
			$rootScope.isUpdateDealer = true;
			
			$rootScope.dealerListElements = dlrList;

			localStorage.setItem('dealerListElements',JSON.stringify($rootScope.dealerListElements)); 

			angular.copy($rootScope.dealerListElements, $rootScope.tempArrayofDealerInfo);
			$rootScope.isEditDealer = true;
			
			$scope.dealerId=dlrList.dealer_id;		
			
			$scope.dealer_id = dlrList.dealer_id;			

			
			if($rootScope.dealerListElements.pos_code ==''){				
				$scope.fosChecked = true;
			}else{
				$scope.fosChecked = false;
			}	
			//$rootScope.active=false;		
			$state.go('dealer.createdealer', { 'createOrUpdate': "update", 'dealerId': $scope.dealer_id });			
		} catch(error){			
				$rootScope.shouldShowTyreImg = false;
				
		}	
	}


	/**
     * @function deleteDealerConfirmation
     * @description function handel the delete dealer with confirmation
     */ 
		$scope.deleteDealerConfirmation = function(dealer_id){
			
			$scope.dealer_idToDelete = dealer_id;
			
			$scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
			$scope.modalContent = dealerAudit_ConstantsConst.deleteDealerMsg;
			$scope.open("DeleteConfirm");
							

		}
	/**
     * @function deleteDealer
     * @description function delete the dealer from list 
     * @params dealer_id
     */ 
		$scope.deleteDealer = function(dealer_id){
		
		try{
			$rootScope.shouldShowTyreImg = true;
			
				$rootScope.session.getClientObjectType("dealers").then(function(clientObject){
					
					clientObject.get(dealer_id).then(function(response) {
						
						$scope.deleteId = response.id;
						response.remove().then(function(response){
							
						$rootScope.shouldShowTyreImg = false;
						for(i = 0; i< $scope.dealerList.length ; i ++){
							if($scope.dealerList[i].dealer_id == $scope.deleteId){
								//$scope.refresh();
								$scope.dealerList.splice(i,1);
								$scope.deleteId = "";

							}
						}
						$scope.$apply();
						$state.reload();
						$scope.listdealer(true);
						 },
						function(error){
							$rootScope.shouldShowTyreImg = false;
						
						});
					},
					function(error)
					{	$rootScope.shouldShowTyreImg = false;
						$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
		                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
		                $scope.openModel("connectionError");
						
					});
				},
				function(error){
					$rootScope.shouldShowTyreImg = false;
					$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
	                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
	                $scope.openModel("connectionError");
					
				});
			
			} catch(error){			
				$rootScope.shouldShowTyreImg = false;
			
		}	
		}
	/**
     * @function saveDealerInformation
     * @description function save the dealer information when update or create dealer
     * @params form: contains dealer information
     */ 
		$scope.saveDealerInformation = function(form){
		
		try{
			$rootScope.shouldShowTyreImg = true;
			$rootScope.dealerlistClk = true;
			if(!$scope.saveDealerFlag){
		    $scope.saveDealerFlag = true;
			$scope.form = form;			
			angular.forEach($scope.form.$error.required, function(field) {
			field.$setDirty();
			});
			if(!$scope.ValidateForm()){
				$scope.saveDealerFlag = false;
				return;
			}
						

			
			// Populate the dealer information.
			$scope.dealerInformation = {
				address: $rootScope.dealerListElements.address,				
				city_name : (typeof($rootScope.dealerListElements.city_name) == "undefined" || $rootScope.dealerListElements.city_name == null) ? $rootScope.dealerListElements.city_name = ""  : $rootScope.dealerListElements.city_name,				
				country_name : (typeof($rootScope.dealerListElements.country_name) == "undefined" || $rootScope.dealerListElements.country_name == null) ? $rootScope.dealerListElements.country_name = ""  : $rootScope.dealerListElements.country_name.toLowerCase(),
				dealer_name : (typeof($rootScope.dealerListElements.dealer_name) == "undefined" || $rootScope.dealerListElements.dealer_name == null) ? $rootScope.dealerListElements.dealer_name = ""  : $rootScope.dealerListElements.dealer_name.toLowerCase(),
				email: (typeof($rootScope.dealerListElements.email) == "undefined" || $rootScope.dealerListElements.email == null) ? $rootScope.dealerListElements.email = ""  : $rootScope.dealerListElements.email,
				holding_name: (typeof($rootScope.dealerListElements.holding_name) == "undefined" || $rootScope.dealerListElements.holding_name == null) ? $rootScope.dealerListElements.holding_name = ""  : $rootScope.dealerListElements.holding_name,
				network: (typeof($rootScope.dealerListElements.network) == "undefined" || $rootScope.dealerListElements.network == null || $rootScope.dealerListElements.network == '') ? $rootScope.dealerListElements.network = ""  : $rootScope.dealerListElements.network,
				// participating_tf: (typeof($scope.participant.value) == "" || $scope.participant.value == null || $scope.participant.value == "No") ? $scope.participant.value = false  : $scope.participant.value = true,
				payer_code: (typeof($rootScope.dealerListElements.payer_code) == "undefined" || $rootScope.dealerListElements.payer_code == null) ? $rootScope.dealerListElements.payer_code = ""  : $rootScope.dealerListElements.payer_code,
				phone: (typeof($rootScope.dealerListElements.phone) == "undefined" || $rootScope.dealerListElements.phone == null) ? $rootScope.dealerListElements.phone = ""  : $rootScope.dealerListElements.phone,
				pos_code: (typeof($rootScope.dealerListElements.pos_code) == "undefined" || $rootScope.dealerListElements.pos_code == null) ? $rootScope.dealerListElements.pos_code = ""  : $rootScope.dealerListElements.pos_code,
				post_code: (typeof($rootScope.dealerListElements.post_code) == "undefined" || $rootScope.dealerListElements.post_code == null) ? $rootScope.dealerListElements.post_code = ""  : $rootScope.dealerListElements.post_code,
				//province: (typeof($rootScope.dealerListElements.country_name) == "undefined" || $rootScope.dealerListElements.country_name == null) ? $rootScope.dealerListElements.country_name = ""  : $rootScope.dealerListElements.country_name,				
				tts_name :(typeof($rootScope.dealerListElements.tts_name) == "undefined" || $rootScope.dealerListElements.tts_name == null || $rootScope.dealerListElements.tts_name == '') ? $rootScope.dealerListElements.tts_name = ""  : $rootScope.dealerListElements.tts_name,
				created_by :$rootScope.UserName
			};
			
				
				// Get the client object.
				$rootScope.session.getClientObjectType("dealers").then(function(clientObjectType){
					
					if($state.params.createOrUpdate == "create"){

						var uid = getuid();
						
						$scope.dealerInformation['dealer_id']=uid;						

						clientObjectType.create($scope.dealerInformation).then(function(response){
							if(response != null && typeof(response) != "undefined"){
															
								$rootScope.shouldShowTyreImg = false;
								$scope.saveDealerFlag = false;
								$rootScope.formChanged=false; 
								$scope.modalTitle = dealerAudit_ConstantsConst.successModelTitle;
								$scope.modalContent = dealerAudit_ConstantsConst.dealerSavedMsg;
								$scope.openModel("CreateDealer");
								
							}
							else{
								$scope.saveDealerFlag = false;
								$rootScope.shouldShowTyreImg = false;
								
							}
						},
						function(error){
							$scope.saveDealerFlag = false;
							$rootScope.shouldShowTyreImg = false;
							$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;							
							
							if(error.code == 18){
								$scope.modalContent = dealerAudit_ConstantsConst.duplicareDealerNotAllowedMsg;
							}else{
							$scope.modalContent = dealerAudit_ConstantsConst.couldNotCreateDealerErrorMsg;
							}
							$scope.openModel("Error");
						})
					}
					else if($state.params.createOrUpdate == "update"){
						clientObjectType.get($scope.dealer_id).then(function(response) {
							
							response.update($scope.dealerInformation).then(function(success){
								if(success){
									$scope.saveDealerFlag = false;
									$rootScope.shouldShowTyreImg = false;
									$rootScope.formChanged=false; 
									$scope.modalTitle = dealerAudit_ConstantsConst.successModelTitle;
									$scope.modalContent = dealerAudit_ConstantsConst.updateDealerMsg;
									$scope.openModel("UpdateDealer");
									
								}
								else{
									$scope.saveDealerFlag = false;
									$rootScope.shouldShowTyreImg = false;
									$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
									$scope.modalContent = dealerAudit_ConstantsConst.couldNotUpdateDealerMsg;
									$scope.openModel("Error");

									
								}
							},
							function(error){
								$scope.saveDealerFlag = false;
								$rootScope.shouldShowTyreImg = false;
								$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
								if(error.code == 400){
									$scope.modalContent = dealerAudit_ConstantsConst.duplicareDealerNotAllowedMsg;
								}else{
								$scope.modalContent = dealerAudit_ConstantsConst.updateDealerFailedMsg ;
						    	}
								$scope.openModel("Error");								
							})
						},
					function(error)
					{	
						$scope.saveDealerFlag = false;
						$rootScope.shouldShowTyreImg = false;
						$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
		                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
		                $scope.openModel("connectionError");
						
					});
					}
				},
				function(error){
					$scope.saveDealerFlag = false;
					$rootScope.shouldShowTyreImg = false;
					$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
	                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
	                $scope.openModel("connectionError");
					
				});
			
		}

		} catch(error){		
				$scope.saveDealerFlag = false;	
				$rootScope.shouldShowTyreImg = false;
				
		}	
		};
	


	/**
     * @function listDealerClicked
     * @description function to handle tab change from create dealer to list dealer  
     */
		$scope.listDealerClicked = function(){		

			try{		
				
				$rootScope.dealerlistClk = true;
				if($rootScope.isCreate || $rootScope.isEditDealer){
				var tempArrayLength = Object.keys($rootScope.tempArrayofDealerInfo).length;				
					
					if($rootScope.isEditDealer){
					delete $rootScope.dealerListElements['$$hashKey'];
					}
					var dealerInfoLength = Object.keys($rootScope.dealerListElements).length;
					
					for(i=0; i<tempArrayLength; i++){				
						if(($rootScope.tempArrayofDealerInfo[i] == undefined) || ($rootScope.tempArrayofDealerInfo[i] == null)){
							$rootScope.tempArrayofDealerInfo[i] == '';
						}
					}						
					
					for(i=0; i<dealerInfoLength; i++){			
						if(($rootScope.dealerListElements[i] == undefined) || ($rootScope.dealerListElements[i] == null)){
							$rootScope.dealerListElements[i] == '';
						}
					}

				
					if(JSON.stringify($rootScope.dealerListElements) === JSON.stringify($rootScope.tempArrayofDealerInfo)){
					
						editModeFlag = false;
					}else{
						
						editModeFlag = true;
					}		
						

					if(editModeFlag == false){							
						$scope.listdealer(false);
						//$state.go('dealer.listdealer');

						$rootScope.isEditDealer = false;
					}else{	
						angular.element("listDealer").blur();					
						$scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
						$scope.modalContent = dealerAudit_ConstantsConst.discardChangesMsg;
						$scope.open("handlecreatePageToDealerPage");			
					}

				}else{					
					$scope.listdealer(false);
					//$state.go('dealer.listdealer');

				}
				
				
				
		} catch(error){			
				$rootScope.shouldShowTyreImg = false;
				
		}

		}
	/**
     * @function listDealerClicked
     * @description function to list all the dealers created by logged in manager  
     */
		$scope.listdealer = function(cacheFlag){
			try{
			
				$rootScope.shouldShowTyreImg = true;
				$scope.fosChecked = true;
				$scope.checkBox= false;				

				localStorage.removeItem('dealerListElements');
				$rootScope.dealerListElements ={};
				
				localStorage.setItem('isIndealerListPage', true);
				$rootScope.active= true;
				//angular.element("listDealer").css('active','true');
				//angular.element("addDealer").css('active','false');
				// Initialize the array.
				$scope.tempdealerList = [];
				$scope.dealerList =[];				
				$scope.clearArray();
				$rootScope.isEditDealer = false;

				//$rootScope.shouldShowTyreImg = true;
				
					$rootScope.session.getClientObjectType("dealers").then(function(clientObjectType){
						clientObjectType.list(0,100000,null,cacheFlag).then(function(response) {
														
							$scope.tempdealerList = [];
							$scope.dealerList =[];
							for(i=0 ; i< response.length ; i ++){
								$scope.tempdealerList.push(response[i].getData());
								
							}
							
							$scope.dealerList = $scope.tempdealerList;
							if($scope.dealerList.length<0){
								$scope.noDealerAddedForManager = true;
							}else{
								$scope.noDealerAddedForManager = false;
							}
							//angular.element('#loading').hide();
							
							
							$rootScope.shouldShowTyreImg = false;
							
							$scope.tempdealerList = [];
							$scope.$apply();
							// Once the data is populated go to list dealer.
							
							$state.go('dealer.listdealer');
						},
						function(error)
						{
							$state.go('dealer.listdealer');
							//angular.element('#loading').hide();
							$rootScope.shouldShowTyreImg = false;							
							$scope.noDealerAddedForManager = true;
							if(error.code == 55 && error.message == "No Content"){
								$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
								$scope.modalContent = dealerAudit_ConstantsConst.dealerNotAvailableMsg;
								if(!$rootScope.popUpActive){
									$rootScope.popUpActive=  true;
									$scope.openModel("Error");
								}
								/*if(!window.location.href.endsWith("dealer")){
									$rootScope.active=false;
								}
								*/
							}
							else{
								
							}
						});
					},
					function(error){
					//angular.element('#loading').hide();
						$rootScope.shouldShowTyreImg = false;
						$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
		                $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
		                $scope.openModel("connectionError");
						
					});
				
			
			} catch(error){			
				$rootScope.shouldShowTyreImg = false;
				
			}
		}
		/**
     * @function navigateToOtherTab
     * @description function to handle navigation to other pages
     */
      $scope.navigateToDifferentTab = function(navigateTo){
        $scope.navigateToLocation='';
        $scope.navigateToLocation = navigateTo;            


        /*if($rootScope.isCreate || $rootScope.isEditDealer){
				var tempArrayLength = Object.keys($rootScope.tempArrayofDealerInfo).length;				
					
					if($rootScope.isEditDealer){
					delete $rootScope.dealerListElements['$$hashKey'];
					}
					var dealerInfoLength = Object.keys($rootScope.dealerListElements).length;
					
					for(i=0; i<tempArrayLength; i++){				
						if(($rootScope.tempArrayofDealerInfo[i] == undefined) || ($rootScope.tempArrayofDealerInfo[i] == null)){
							$rootScope.tempArrayofDealerInfo[i] == '';
						}
					}						
					
					for(i=0; i<dealerInfoLength; i++){			
						if(($rootScope.dealerListElements[i] == undefined) || ($rootScope.dealerListElements[i] == null)){
							$rootScope.dealerListElements[i] == '';
						}
					}

				
					if(JSON.stringify($rootScope.dealerListElements) === JSON.stringify($rootScope.tempArrayofDealerInfo)){
					
						editModeFlag = false;
					}else{
						
						editModeFlag = true;
					}		
						

					if(editModeFlag == false){	
					    $scope.selectedTab = $scope.navigateToLocation;
					    if($scope.navigateToLocation == 'login'){
							$rootScope.logoutFlag = true;				
							localStorage.removeItem('sessionObj');
						}							
						$state.go($scope.navigateToLocation);
						$rootScope.isEditDealer = false;

					}else{	
						angular.element("listDealer").blur();
						$scope.selectedTab = 'truckforcedealers';					
						$scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
						$scope.modalContent = dealerAudit_ConstantsConst.discardChangesMsg;

						$scope.open("navigateToOtherTab");			
					}

				}else{					
					
					if($scope.navigateToLocation == 'login'){
						$rootScope.logoutFlag = true;				
						localStorage.removeItem('sessionObj');
					}
					$state.go($scope.navigateToLocation);
				}
*/
				if($rootScope.formChanged){
						event.preventDefault();						
						$scope.selectedTab = 'truckforcedealers';					
						$scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
						$scope.modalContent = dealerAudit_ConstantsConst.discardChangesMsg;

						$scope.open("navigateToOtherTab");
				}else{
						$scope.selectedTab = $scope.navigateToLocation;
					    if($scope.navigateToLocation == 'login'){
							$rootScope.logoutFlag = true;				
							localStorage.removeItem('sessionObj');
						}							
						$state.go($scope.navigateToLocation);
						$rootScope.isEditDealer = false;
				}       
        }
	
		$scope.clearAllFields = function(){

			
			$rootScope.dealerListElements.city_name="";
			$rootScope.dealerListElements.country_name="";
			$rootScope.dealerListElements.dealer_name="";
			$rootScope.dealerListElements.email="";
			$rootScope.dealerListElements.holding_name="";
		    $rootScope.dealerListElements.network="";
			// $scope.participant={};
			$rootScope.dealerListElements.payer_code="";
			$rootScope.dealerListElements.phone="";
			$rootScope.dealerListElements.pos_code="";
			$rootScope.dealerListElements.post_code="";
			$rootScope.dealerListElements.country_name="";		
			$rootScope.dealerListElements.address="";
			$scope.dealer_id = "";		
			$rootScope.dealerListElements.tts_name = {};	
			

			/*for (var key in $rootScope.dealerListElements){
			        var attrName = key;

			        var attrValue = $rootScope.dealerListElements[key];
			        if(attrValue != '' && attrValue != null && attrValue != false && attrValue != undefined){
			        	$rootScope.dealerListElements[key] ="";
			        }
			   }*/
			$state.reload();	
		}

		$scope.clearArray = function(){

			
			$rootScope.dealerListElements={
						"holding_name": "",
						"address": "",
						"pos_code": "",
						"holding_code": null,
						"modified_date": "",
						"tts_name": "",
						"network": "",
						"city_name": "",
						"payer_code": "",
						"modified_time": "",
						"participating_tf": false,
						"province": "",
						"phone": "",
						"post_code": "",
						"country_name": "",
						"dealer_name": "",
						"email": "",
						"dealer_id": ''
					};

				 $rootScope.tempArrayofDealerInfo={
						"holding_name": "",
						"address": "",
						"pos_code": "",
						"holding_code": null,
						"modified_date": "",
						"tts_name": "",
						"network": "",
						"city_name": "",
						"payer_code": "",
						"modified_time": "",
						"participating_tf": false,
						"province": "",
						"phone": "",
						"post_code": "",
						"country_name": "",
						"dealer_name": "",
						"email": "",
						"dealer_id": ''
					};
		}
	

	}catch(e){
	    $rootScope.shouldShowTyreImgImg = false;	  
	    
    }
});

angular.module('dealerAudit').directive('validateAlphaNumericSpaces', function() {
	try {
		

		return {

			require : 'ngModel',

			link : function(scope, element, attr, ngModelCtrl) {

				function fromUser(text) {

					if (text) {
						//Bug fix-MOB_117(Special characters)

						var transformedInput = text.replace(/[a-zA-Z]/gi, '');

						if (transformedInput !== text) {

							ngModelCtrl.$setViewValue(transformedInput);

							ngModelCtrl.$render();

						}

						return transformedInput;

					}

					return undefined;

				}


				ngModelCtrl.$parsers.push(fromUser);

			}
		};

	} catch(e) {
	
	}
	
});
	
	
