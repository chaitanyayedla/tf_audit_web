/**
 * @controller: auditController
 * @description This module includes functionalities for displaying and downloading Audits, audit details.
 */

angular = require('angular');

angular.module('dealerAudit').controller('auditController',function($scope,$state,$stateParams,$rootScope,$location,$window,$uibModal,$timeout ,$filter,dealerAudit_ConstantsConst,passParameterFactory) {

try {

        
          //below flags are to identify and highlight the tab the control is in
          $rootScope.tabOne = false;
          $rootScope.tabTwo = false; 
          $rootScope.tabThree = false;  

          var currentLoc = $location.path();         
          if(currentLoc == '/audits/currentyearaudits'){
            $rootScope.tabOne = true;
          }else if(currentLoc == '/audits/previousaudits'){
            $rootScope.tabTwo = true; 
          }else if(currentLoc == '/audits/uploadaudits'){
            $rootScope.tabThree = true;  
          }
          
          $scope.orderByField = 'AuditDate';

          var pageNumincurrentyrpage = localStorage.getItem('currentyearpagination');
          var pageNuminprevyrpage = localStorage.getItem('prevyearpagination');
           
           // tracking pagination page number in current year audits tab
            if(pageNumincurrentyrpage!=null){
                 $scope.paginationCurrYr = {
                   current: pageNumincurrentyrpage
                 };
            }else{
                 $scope.paginationCurrYr = {
                   current: 1
                 };
            }

            // tracking pagination page number in previous year audits tab
            if(pageNuminprevyrpage!=null){
                 $scope.paginationPrevYr = {
                   current: pageNuminprevyrpage
                 };
            }else{
                 $scope.paginationPrevYr = {
                   current: 1
                 };
            }

                   
          $scope.paginationChangedinCurrentYeartab = function(newPage){              
              localStorage.setItem('currentyearpagination',newPage);            
          }

           $scope.paginationChangedinPrevYeartab = function(newPage){             
              localStorage.setItem('prevyearpagination',newPage);            
          }
          // declaring required variables, flags, json, arrays..          
          $scope.countrySelected = false;
          if( $scope.countryLangSelected== null || typeof($scope.countryLangSelected)== 'undefined'){
            $scope.countryLangSelected= {
            "enableFlag" : false
          }
          }                  

          if(screen.width <=1000){
            $scope.numberOfrows =  6;
          }          
          if(screen.width >1000 && screen.width <1500){
            $scope.numberOfrows =  8;
          }
         
          if(screen.width >1500 ){
            $scope.numberOfrows =  11;
          }
          $scope.templateSelected = false;
          if(typeof($rootScope.templateInfoForAudit) == 'undefined' || $rootScope.templateInfoForAudit == null){
                   // Template information
                  $rootScope.templateInfoForAudit = {
                    "selectedCountry":"",
                    "selectedLanguage":"",
                    "templateId" : ""          
                  }; 

                }          

          if($rootScope.countryLangForTam == null || typeof($rootScope.countryLangForTam)=='undefined'){
            $rootScope.countryLangForTam=[];
          }

          if($rootScope.templateListItems==null || typeof($rootScope.templateListItems)=='undefined'){
            $rootScope.templateListItems=[];
          }
         
          $rootScope.currentYearAudits = [];
          $rootScope.previousYearsAudits= [];
          $scope.hiddens = true;     
          var groupArray = require('group-array');
          

    /**
     * @function openModel
     * @description function to open the bootstrap modal popup containing ok  buttons to alert user   
     */
    $scope.openModel = function(value) {
        $scope.modalInstance = $uibModal.open({
    
        animation: true,
        templateUrl: '/partials/alertpopup.html',
        controller: 'modelController',
        size: 'sm',
        scope: $scope,
        // Prevent closing by clicking outside or escape button.
        backdrop: 'static',
        keyboard: false
      });
      
      $scope.modalInstance.modalValue = value;
    };


     /**
     * @function open
     * @description function to open the bootstrap confirmation popup model 
     */
      $scope.open = function(value) {
            $scope.modalInstance = $uibModal.open({
        
                animation: true,
                templateUrl: '/partials/confirmationmodal.html',
                controller: 'auditController',
                size: 'sm',
                scope: $scope,
                // Prevent closing by clicking outside or escape button.
                backdrop: 'static',
                keyboard: false
            });
            
            $scope.modalInstance.modalValue = value;
      };

      $scope.ok = function() {
     
         if($scope.modalInstance.modalValue == 'confirmNavigation'){

                $rootScope.selected_files=[];
           
                if($scope.navigateToLocation == 'login'){
                  $rootScope.logoutFlag = true;       
                  localStorage.removeItem('sessionObj');
                }
                $scope.selectedTab=$scope.navigationLocation;
                
                $state.go($scope.navigationLocation);
            
         }
         if($scope.modalInstance.modalValue == "navigateToBackTab"){
                 
                $rootScope.modelOpened = false;                 
                $rootScope.selected_files =[];                                                        
                $scope.modalInstance.dismiss('cancel'); 
                $state.go('audits.'+passParameterFactory.getNavigateToLocation())                           
                   

        }else{

            $scope.modalInstance.dismiss('cancel'); 
        }
      }

    /**
     * @function cancel
     * @description function to perform actions when user click cancel button on confirmation popup
     */
     $scope.cancel = function() {
        $rootScope.modelOpened = false;

        $scope.modalInstance.dismiss('cancel'); 

     }

     $scope.gotoHome=function(){
        $scope.navigateToOtherTabs('home');
     }

    /**
     * @function signOut
     * @description function for logout.     
     */
    $scope.signOut = function() {
      try {
        
          $scope.navigateToOtherTabs('login');
      } catch(e) {
      }
    }
   
     
      /**
     * @function sort
     * @description function to sort the table data based on perticular field
     */
    
      $scope.sort = function(sortBy){
              //$scope.resetAll();  
        try{      
              $scope.columnToOrder = sortBy; 
              $scope.Header = [];
                   
              //$Filter - Standard Service
              $scope.auditDetailsPerDealer = $filter('orderBy')($scope.auditDetailsPerDealer, $scope.columnToOrder, $scope.reverse); 

              if($scope.reverse)
                   iconName = 'glyphicon glyphicon-chevron-up';
               else
                   iconName = 'glyphicon glyphicon-chevron-down';

              $scope.reverse = !$scope.reverse;   

            }catch(e){
            $rootScope.shouldShowTyreImg = false;
            //$scope.$apply();
           
          }
              
          };
          
    /**
     * @function initLoad
     * @description function to load the data on page load
     */

      $scope.initLoad = function ()
          {
          $rootScope.shouldShowTyre = true;
          ISession.getInstance().then(function(session) {

              $rootScope.session = session;
              $rootScope.auditDetailsPerDealer = [];
              $rootScope.countryLangForTam=[];
              $rootScope.shouldShowTyreImg = true;
              $scope.getTemplateList();
              //$scope.getDealerList();
              $scope.getCountryLanguage();
              $scope.Auditdate ="";
          }, function(error) {        
              
        });
         // $rootScope.callAtTimeout();
         
       };

      
      /**
       * @function audit
       * @description function to navigate to correct tabs when click on back to audit list button in audit details page.     
       */
        $scope.audit = function() {

            var previousLocation = localStorage.getItem('previousLocation');
            if(previousLocation == 'currentYear'){
                $state.go('audits.currentyearaudits');
            }else if(previousLocation == 'previousYear'){
                $state.go('audits.previousaudits');
            }else{
                $state.go('audits.currentyearaudits');
            }     
          
        }; 


    /**
     * @function getDealerList
     * @description function to get all the dealers created by logged in manager
     */

      $rootScope.DealerListItems=[];
      $scope.getDealerList=function(enableDropDown){

        try{
               
               $scope.dt='';
                $scope.df='';              
                  
                    $rootScope.session.getClientObjectType("dealers").then(function(clientObjectType){
                        clientObjectType.list(0, 100000, null, false).then(function(response) {
                       
                         $rootScope.DealerListItems =[];
                          for(i=0;i<response.length;i++){
                              $rootScope.DealerListItems.push(response[i].data);
                          }                        
                                                
                          $scope.getAuditDetails(enableDropDown);
                       
                        },function(error){
                          $scope.noDetails=true;
                         $rootScope.shouldShowTyreImg = false;
                           if(error.code == 55 && error.message == "No Content"){
                                    $rootScope.shouldShowTyreImg = false;
                                    $scope.$apply();                                    
                                  }
                                 
                        })
           
                    },function(error){
                     
                      $rootScope.shouldShowTyreImg = false;
                      $scope.$apply();
                      
                    })
            }catch(e){
            $rootScope.shouldShowTyreImg = false;
            //$scope.$apply();
            
          }

    };

    /**
     * @function getAuditList
     * @description function get all the audit response data submitted for the templates which are created by logged in manager
     */

      $rootScope.AuditListItems=[];
      $scope.getAuditList=function(enableDropDown){

          try{  
               
              
                    $rootScope.session.getClientObjectType("audit_response").then(function(clientObjectType){
                        clientObjectType.list(0,100000,null,false).then(function(response) {
                    
                        $rootScope.AuditListItems =[];                       
                       
                        //getting all the audit response data
                          for(i=0;i<response.length;i++){
                              $rootScope.AuditListItems.push(response[i].data);                              
                          }
                      
                         $rootScope.audit_response_data=[];

                        //Getting audit_response_data belongs to templates created and by logged in manger 
                         $rootScope.AuditListItems.forEach(function(obj){                        
                          for(i=0; i<$rootScope.templateListItems.length;i++){
                            if(obj.template_id == $rootScope.templateListItems[i].template_id){
                                 $rootScope.audit_response_data.push(obj);
                            }
                          }
                         })

                         localStorage.setItem('collectAuditResponse', JSON.stringify($rootScope.audit_response_data));

                         
                         $rootScope.uniqueAuditFromEachDealer = [];

                         // Getting distinct audits done for dealer under perticular user and under perticular manager
                         $rootScope.audit_response_data.forEach(function(o){
                            $rootScope.uniqueAuditFromEachDealer.push({'template_id':o.template_id,'dealer_id':o.dealer_id,'audit_done_by':o.audit_done_by});
                         });

                         $rootScope.uniqueAuditFromEachDealer = arrUnique($rootScope.uniqueAuditFromEachDealer);                      
                         // $scope.getAuditDetails(enableDropDown);
                         $scope.getDealerList(enableDropDown);
                          //$scope.$apply();
                        },function(error){
                         
                          $rootScope.shouldShowTyreImg = false;
                          if(error.code == 55 && error.message == "No Content"){
                                    $rootScope.shouldShowTyreImg = false;
                                    $scope.$apply();                                   
                                  }                                  
                        })
           
                    },function(error){
                      $scope.noDetails=true;
                      $rootScope.shouldShowTyreImg = false;
                      $scope.$apply();
                    
                    })

             }catch(e){
            $rootScope.shouldShowTyreImg = false;
            //$scope.$apply();
          
          }

    };


    /**
     * @function getTemplateList
     * @description function get the templates created by logged in manager, required for filter the audit response data
     */

       
        $scope.getTemplateList=function(){

          try{
              
                     var filterParams = {
                            "tam_name": $rootScope.UserName,                                               
                            };
                    $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){
                        clientObjectType.search("template_master", 0, 10000, filterParams, null, null, null, null, null, false).then(function(response) {
                      
                         
                         $rootScope.templateListItems =[];
                          for(i=0;i<response.length;i++){
                              $rootScope.templateListItems.push(response[i].data);
                          }
                          $scope.getAuditList(false);
                       
                         // $scope.$apply();
                        },function(error){
                           $scope.noDetails=true;
                           $rootScope.shouldShowTyreImg = false;
                           if(error.code == 55 && error.message == "No Content"){
                                  $rootScope.shouldShowTyreImg = false;
                                    $scope.$apply();
                                       }
                                  
                        })
           
                    },function(error){
                     
                     $rootScope.shouldShowTyreImg = false;
                     $scope.$apply();
                      
                    })

            }catch(e){
            $rootScope.shouldShowTyreImg = false;
            //$scope.$apply();
          
          }


    };

    /**
     * @function truckDetail
     * @description function to navigate to audit details when clicked on perticular audit from list
     * function collects all the dealer details and questions and answers of selected audits
     * @param {Object} auditList - json array of all audits
     */

      $scope.truckDetail = function(auditList,currentOrPreviousYrTab)
      {

        try{

          // When navigating to audit details page, storing the previous location (From CurrentYear audits page or prevoious year's audit page )
          localStorage.setItem('previousLocation', currentOrPreviousYrTab);

          var auditList = auditList;

          $rootScope.selectedAuditFromList = [];

          $rootScope.auditDetailsPerDealer.forEach(function(o){
              if(JSON.stringify(o) == JSON.stringify(auditList)){              
                $rootScope.selectedAuditFromList.push(o);
              }
          });

          
          $state.go("auditdetails");
          localStorage.setItem('collectSupertruck', JSON.stringify($rootScope.selectedAuditFromList));

          }catch(e){
            $rootScope.shouldShowTyreImg = false;            
            
          }

      }

    /**
     * @function arrUnique
     * @description function make the array containing unique objects
     * @param {Array }  - holds the data related
     */
      function arrUnique(arr) {

        try{

          var cleaned = [];
          arr.forEach(function(itm) {
              var unique = true;
              cleaned.forEach(function(itm2) {
                  if (_.isEqual(itm, itm2)) unique = false;
              });
              if (unique)  cleaned.push(itm);
          });
          return cleaned;

           }catch(e){           
          
          }
      }

    /**
     * @function sortByKey
     * @description functionsort the array based on specfied field/column
     * @param {Array }  - holds the data related
     */
      function sortByKey(array, key) {

        try{
                return array.sort(function(a, b) {
                    var x = a[key]; var y = b[key];
                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                });

            }catch(e){            
           
          }
     }  
    /**
     * @function getAuditDetails
     * @description functionsort collects the dealer details and template details of each distinct audit
     
     */
      $scope.getAuditDetails = function(enableDropDown){


          try{


                $rootScope.auditDetailsPerDealer = [];

                $rootScope.uniqueAuditFromEachDealer.forEach(function(obj){

                      $rootScope.audit_response_data.forEach(function(o){
                          if(obj.template_id = o.template_id && obj.dealer_id == o.dealer_id && obj.audit_done_by == o.audit_done_by){
                              $rootScope.auditDetailsPerDealer.push({'TempID': o.template_id, 'DealerId':o.dealer_id,'AuditDate': o.audit_date,'AuditDoneBy':o.audit_done_by ,'AuditPersonContact':o.audit_peron_onsite,'auditComments':o.audit_comments});
                          }      
                      });

                });

                $rootScope.auditDetailsPerDealer = arrUnique($rootScope.auditDetailsPerDealer); 


                // For each audit record having template id, dealer id, audit done by getting template informations and dealer information
                $rootScope.auditDetailsPerDealer.forEach(function(o){
                   for(i=0;i<$rootScope.templateListItems.length;i++){

                         if ($rootScope.templateListItems[i].template_id == o.TempID)
                         {                          
                          o['TempName'] = $rootScope.templateListItems[i].template_name ;
                          o['Creation_date'] =$rootScope.templateListItems[i].creation_date ;         
                         }        
                   }
                });

                $rootScope.auditDetailsPerDealer.forEach(function(o){
                  for(i=0;i<$rootScope.DealerListItems.length;i++){

                    if(o.DealerId == $rootScope.DealerListItems[i].dealer_id){
                        o['DelearName'] = $rootScope.DealerListItems[i].dealer_name;                  
                        o['Country'] = $rootScope.DealerListItems[i].country_name ;
                        o['city'] = $rootScope.DealerListItems[i].city_name ;
                        o['address'] =$rootScope.DealerListItems[i].address ;
                        o['postCode'] = $rootScope.DealerListItems[i].post_code;
                        o['Pos_code'] = $rootScope.DealerListItems[i].pos_code;
                        o['fosVendorCode'] = $rootScope.DealerListItems[i].payer_code ;
                        o['tts'] = $rootScope.DealerListItems[i].tts_name
                    }  
                  }
                });


                //$rootScope.auditDetailsPerDealer is an array of objects , object represents unique audit .
                // dividing the array into two so that to one containing current year audit and other containing previous year's audits
                
                var currentYear = new Date().getFullYear();
                $rootScope.currentYearAudits = [];
                $rootScope.previousYearsAudits= [];
                $rootScope.auditDetailsPerDealer.forEach(function(audit){

                      var aud_date= new Date(audit.AuditDate);
                      var dateOfAudit = aud_date.getFullYear();
                      

                      if(currentYear == dateOfAudit){
                        $rootScope.currentYearAudits.push(audit);

                      }else{
                        $rootScope.previousYearsAudits.push(audit);
                      }
                });

            
                if($rootScope.currentYearAudits.length<=0){
                  $scope.noDetails = true;
                }else{
                  $scope.noDetails = false;
                }
                if(enableDropDown){
                  if($rootScope.auditDetailsPerDealer.length>0){
                    $scope.countryLangSelected.enableFlag = true;
                  }else{
                    $scope.countryLangSelected.enableFlag = false;
                  }
                }else{
                  $scope.countryLangSelected.enableFlag = false;
                }
                // If there are no audits available we are not allowing to select template to download excel
                
                $rootScope.shouldShowTyreImg = false;
                $scope.$apply();
               // $state.reload();
              
               
            }catch(e){
            $rootScope.shouldShowTyreImg = false;
            
          }
      }

    /**
     * @function getQuestionOfAudit
     * @description function collect all the questions and answers of selected audit from list    
     */

      $scope.getQuestionOfAudit = function(){

      try{


        if($rootScope.selectedAuditFromList == null || typeof($rootScope.selectedAuditFromList) == "undefined"){
           $rootScope.selectedAuditFromList =JSON.parse(localStorage.getItem('collectSupertruck'));

        }
        if($rootScope.audit_response_data == null || typeof($rootScope.audit_response_data) == "undefined"){
           $rootScope.audit_response_data =JSON.parse(localStorage.getItem('collectAuditResponse'));

        }

        $rootScope.headersOfQuestionsInAudit = [];
       

        $rootScope.questionsAndAnsInAudit = [];
        $scope.questionsAndAnsInAuditWithouOrder =[];

        $rootScope.audit_response_data.forEach(function(o){
        
          if($rootScope.selectedAuditFromList[0].TempID == o.template_id && $rootScope.selectedAuditFromList[0].DealerId == o.dealer_id && $rootScope.selectedAuditFromList[0].AuditDoneBy == o.audit_done_by && $rootScope.selectedAuditFromList[0].AuditPersonContact == o.audit_peron_onsite){
              $scope.questionsAndAnsInAuditWithouOrder.push(o);
          }
        });

        //ordering based on position id of questions
        $rootScope.questionsAndAnsInAudit = sortByKey($scope.questionsAndAnsInAuditWithouOrder, 'position_id');

        $rootScope.groupedQuestionsArrayofAudit = groupArray($rootScope.questionsAndAnsInAudit, 'header_text', 'sub_header_text');

        $rootScope.headersOfQuestionsInAudit = Object.keys($rootScope.groupedQuestionsArrayofAudit);

        }catch(e){
            $rootScope.shouldShowTyreImg = false;
            
        }
      };
      
      // apply filter to search the data based on entered characters
       $scope.search = {}; 
       $scope.applyFilter = function(auditListCollection){    
           
              if (!$scope.search.searchFilter ||($rootScope.auditListCollection.DelearName.indexOf($scope.search.searchFilter) != -1)){
                  $scope.searchNotFound = false;
                  return true;

              }else{
                $scope.noDetails = true;
              }

           }

    /**
     * @function countryLanguageSelected
     * @description function to list the templates present for selected country and language combinations.
     * required to download excel sheet.    
     */
      $scope.countryLanguageSelected = function(){

        try{
        if(typeof($rootScope.templateInfoForAudit.selectedCountry)!='undefined' || typeof($rootScope.templateInfoForAudit.selectedLanguage)!='undefined'){
                $rootScope.shouldShowTyreImg = true;
               
                $rootScope.templateListItems =[];   
                $rootScope.auditDetailsPerDealer = [];
              
                                    
                        var filterParams = {
                            "tam_name": $rootScope.UserName,
                            "country_name": $rootScope.templateInfoForAudit.selectedCountry,
                            "language_name": $rootScope.templateInfoForAudit.selectedLanguage                
                        };              
                     
                    $rootScope.session.getClientObjectType("template_master").then(function(clientObjectType){
                          clientObjectType.search("template_master", 0, 10000, filterParams, null, null, null, null, null, false).then(function(response) {
                      
                          for(i=0;i<response.length;i++){
                              $rootScope.templateListItems.push(response[i].data);
                          }

                         
                          $rootScope.shouldShowTyreImg = false;
                          $scope.getAuditList(true);
                         
                          $scope.$apply();
                        },function(error){
                         
                           if(error.code == 55 && error.message == "No Content"){
                                    $rootScope.currentYearAudits = [];
                                    $rootScope.previousYearsAudits = [];
                                    $rootScope.shouldShowTyreImg = false;
                                    $scope.$apply();                                    
                                  }
                                 
                        })
           
                    },function(error){
                      $rootScope.shouldShowTyreImg = false;                     
                    })


          }else{
            $rootScope.shouldShowTyreImg = false;        
          }

        }catch(e){
            $rootScope.shouldShowTyreImg = false;            
        }
      }

      $scope.countrySelectedFun = function(){

        $scope.countrySelected = true;
       // $scope.$apply();
      }

      $scope.clearDownloadInfo = function(){

          $scope.countrySelected = false;
          $scope.countryLangSelected.enableFlag = false;
          $scope.templateSelected = false;
         
          $rootScope.templateInfoForAudit = {
            "selectedCountry":"",
            "selectedLanguage":"",
            "templateId" : ""          
          }; 

            
      }

    /**
     * @function templateSelectedFun
     * @description when perticular template is selected from list ,collects related data to download excel.       
     */
      $scope.templateSelectedFun = function(){

        try{

          $scope.templateSelected = true;
          var key = "$$hashKey";
          delete $rootScope.templateInfoForAudit.templateId[key];
          $rootScope.templateListItems=[];

          $rootScope.templateListItems.push($rootScope.templateInfoForAudit.templateId);          
          $scope.getAuditList(false);
      
        }catch(e){
            $rootScope.shouldShowTyreImg = false;          
        }
      }

    /**
     * @function getCountryLanguage
     * @description function to list country language combinations present for logged in manager.
     * required to select country,language,tenplate to download excel sheet.    
     */
      $scope.getCountryLanguage = function(){

                  try{
                  
                          //getting country language list
                          $rootScope.session.getClientObjectType("country_language").then(function(clientObjectType){
                                  clientObjectType.list(0,100000,null,true).then(function(response){
                                      
                                      $scope.dataOfCountryLang=[];
                                      for(i=0;i<response.length;i++){
                                          $scope.dataOfCountryLang.push(response[i].getData());
                                      }

                                      var filterParams = {
                                        "tam_name": $rootScope.UserName                    
                                        };
                                      

                                      // getting saved_locations and filtering based on tam
                                      $rootScope.session.getClientObjectType("saved_locations").then(function(clientObjectType){
                                             clientObjectType.search("saved_locations", 0, 1000, filterParams, null, null, null, null, null, true).then(function(response) {
                                                  
                                                  $scope.dataOfSavedLocForTam=[];
                                                  for(i=0;i<response.length;i++){
                                                     $scope.dataOfSavedLocForTam.push(response[i].getData());
                                                  }
                                                                                         
                                                 
                                                  for(i=0;i<$scope.dataOfSavedLocForTam.length;i++){
                                                    $scope.dataOfCountryLang.forEach(function(o){        
                                                      if (o.country_name == $scope.dataOfSavedLocForTam[i].country_name) 
                                                          $rootScope.countryLangForTam.push(o);
                                                         
                                                    });
                                                  }
                                                 //$rootScope.shouldShowTyreImg = false;
                                                 //$scope.$apply();
                                                 

                                              },function(error){
                                                $rootScope.shouldShowTyreImg = false;
                                                 $scope.$apply();                                                 
                                              });

                                      },function(error){
                                        $rootScope.shouldShowTyreImg = false;
                                         $scope.$apply();                                          
                                      });

                                  },function(error){
                                    $rootScope.shouldShowTyreImg = false;
                                     $scope.$apply();                                     
                                      if(error.code == 55 && error.message == "No Content"){
                                    $rootScope.shouldShowTyreImg = false;
                                    $scope.$apply();                                   
                                  }
                                  
                                  });

                          },function(error){
                            $rootScope.shouldShowTyreImg = false;
                             $scope.$apply();                              
                          });   

                    }catch(e){
                      $rootScope.shouldShowTyreImg = false;
                       $scope.$apply();                   
                      return false;
                    }

        };

    /**
     * @function getDataOfAllAuditsToCreateExcel
     * @description function to prepare data in required format to download as excel sheet.   
     */

       $scope.getDataOfAllAuditsToCreateExcel = function(isCurrentYear){
         try{

          $rootScope.currentOrPrevioudYearAudits = [];
          //based on current year or previous year flag downloading respective data as csv
          if(isCurrentYear){
              $rootScope.currentOrPrevioudYearAudits = $rootScope.currentYearAudits;
          }else{
              $rootScope.currentOrPrevioudYearAudits = $rootScope.previousYearsAudits;
          }
          $scope.dataOfAllAuditsToCreateExcelWithRequiredFields =[];
          $scope.dataOfAllAuditsToCreateExcel =[];
          $scope.auditResponseOfAllAudits = {};

        
          if($rootScope.audit_response_data.length>0){
              var temp = {};
              var csvFileName='';
              temp = $rootScope.templateInfoForAudit.templateId;
              var templateID = temp.template_id;
              

             // ISession.getInstance().then(function(session) {                          
             //      $scope.session = session;

              //Getting the list of question of template
                $rootScope.session.getClientObjectType("question").then(function(clientObjectType){

                    var filterParams={
                      "template_id" : templateID
                    };
                    clientObjectType.search("question", 0, 10000, filterParams, null, null, null, null, null, true).then(function(response) {
                      var questionUnderTemplate = [];                   
                      for(i=0;i<response.length;i++){

                        questionUnderTemplate.push(response[i].getData());
                      }              
                     
                      // constucting header of excel sheet
                      $scope.headerForExcelSheet={
                        'ID':'',
                        'Dealer Name':''
                      };

                      var questionss = {};                                   

                      for(i=0;i<questionUnderTemplate.length;i++){
                          
                            var quest=questionUnderTemplate[i].question_text;
                             //replacing special characters to prevent crash in csv
                            quest=quest.replace(/["',]/g, "");
                            quest=quest.replace(/\n/g, "");     
                            quest=quest.replace(/\r/g, "");  
                            $scope.headerForExcelSheet[quest]='';        
                            if(questionss.hasOwnProperty(quest)){
                                //if question already exist , adding . at the end to differentiate the question to prevent overriding the same question.
                                quest =quest+' ';
                                questionss[quest]='';
                            }else{                                                       
                                questionss[quest]='';
                            }
                      }                    
                   
                      $scope.headerForExcelSheet["Result"]=''

                      var data=[];
                      var dataToCompareQuestion=[];


                      /**
                       * preparing data for excel sheet, removing ',' present in data since it makes conflict while extracting as excel                      
                       */
                      for(i=0;i<$rootScope.currentOrPrevioudYearAudits.length;i++){
                              var object1={};
                              var objWithQuest={};
                              var dealerid=$rootScope.currentOrPrevioudYearAudits[i].DealerId;   
                              if(dealerid){                          
                              object1["ID"]=dealerid.toString().replace(/[^(){}@&$%#a-zA-Z0-9_-]/g,' ');
                              }

                              var dealerName=$rootScope.currentOrPrevioudYearAudits[i].DelearName;
                              if(dealerName){  
                              object1["DelearName"]=dealerName.toString().replace(/[^(){}@&$%#a-zA-Z0-9_-]/g,' '); 
                              }
                              var dealerAddress=$rootScope.currentOrPrevioudYearAudits[i].address;
                              if(dealerAddress){  
                              object1["Address"]=dealerAddress.toString().replace(/[^(){}@&$%#a-zA-Z0-9_-]/g,' '); 
                              }
                              var dealerPostCode=$rootScope.currentOrPrevioudYearAudits[i].postCode;
                              if(dealerPostCode){  
                              object1["Postalcode"]=dealerPostCode.toString().replace(/[^(){}@&$%#a-zA-Z0-9_-]/g,' ');
                              }
                              var dealerCountry=$rootScope.currentOrPrevioudYearAudits[i].Country; 
                              if(dealerCountry){  
                              object1["Country"]=dealerCountry.toString().replace(/[^(){}@&$%#a-zA-Z0-9_-]/g,' '); 
                              }
                              var dealerCity=$rootScope.currentOrPrevioudYearAudits[i].city;
                              if(dealerCity){  
                              object1["City"]=dealerCity.toString().replace(/[^(){}@&$%#a-zA-Z0-9_-]/g,' '); 
                              }
                              var dealerFosVendorCode=$rootScope.currentOrPrevioudYearAudits[i].fosVendorCode;
                              if(dealerFosVendorCode){  
                              object1["FOSVendorCode"]=dealerFosVendorCode.toString().replace(/[^(){}@&$%#a-zA-Z0-9_-]/g,' ');
                              }
                              var dealerTts=$rootScope.currentOrPrevioudYearAudits[i].tts; 
                              if(dealerTts){  
                              object1["TruckTireSalesperson"]=dealerTts.toString().replace(/[^(){}@&$%#a-zA-Z0-9_-]/g,' '); 
                              }
                              for(j=0;j<$rootScope.audit_response_data.length;j++){

                                    if($rootScope.currentOrPrevioudYearAudits[i].DealerId==$rootScope.audit_response_data[j].dealer_id && templateID==$rootScope.audit_response_data[j].template_id && $rootScope.currentOrPrevioudYearAudits[i].AuditDate==$rootScope.audit_response_data[j].audit_date && $rootScope.currentOrPrevioudYearAudits[i].AuditDoneBy==$rootScope.audit_response_data[j].audit_done_by && $rootScope.currentOrPrevioudYearAudits[i].AuditPersonContact==$rootScope.audit_response_data[j].audit_peron_onsite){
                                          var ans= $rootScope.audit_response_data[j].answer; 
                                          ans = ans.replace(/[^(){}@&$%#a-zA-Z0-9_-]/g,' ');                                         
                                          if( ans == "true"){
                                            ans='Yes';
                                          }else if( ans == "false"){
                                            ans='No'
                                          }
                                          var ques=$rootScope.audit_response_data[j].question_text; 
                                          //ques=ques.replace(/[^(){}@&$%#a-zA-Z0-9_-]/g,' '); 
                                          ques=ques.replace(/["',]/g, "");
                                          ques=ques.replace(/\n/g, "");     
                                          ques=ques.replace(/\r/g, ""); 
                                          object1[ques]=ans;
                                          objWithQuest[ques]=ans;
                                    }                      
                              }
                          
                              data.push(object1);
                              dataToCompareQuestion.push(objWithQuest);
                              
                      }

                      $scope.finalData=[];
                      //$scope.finalData = data;
                      var totalNoOfQuestionUnderThisTemplate = Object.keys(questionss).length;                     
                      for(i=0;i<data.length;i++){
                              var count1=0;
                             
                              var obj={};
                              obj['ID']=data[i].ID;
                              obj['Delear Name']=data[i].DelearName;
                              obj['Address']=data[i].Address;
                              obj['Postal code']=data[i].Postalcode;
                              obj['Country']=data[i].Country;
                              obj['City']=data[i].City;
                              obj['FOS Vendor Code']=data[i].FOSVendorCode;
                              obj['Truck TireSales person']=data[i].TruckTireSalesperson;

                              for(index2 in questionss){
                                  var ind = Object.keys(data[i]).indexOf(index2);
                                  if(ind<0){                               
                                     obj[index2]='';                               
                                  }else{

                                    obj[index2]=data[i][index2];                               
                                    count1 = count1+1;
                                  }
                                  
                              }                           
                                                      
                             var percentageOfAudit = (count1*100)/totalNoOfQuestionUnderThisTemplate;
                             percentageOfAudit=percentageOfAudit.toFixed(2);
                             obj['Result']=percentageOfAudit+'%';
                              
                             $scope.finalData.push(obj);                       
                            
                      }

                      
                      var templateName =temp.template_name.replace(/ /g,'_');
                      csvFileName = temp.country_name+'_'+temp.language_name+'_'+templateName;
                     

                      $scope.countrySelected = false;
                      $scope.countryLangSelected.enableFlag = false;
                      $scope.templateSelected = false;
                      
                      // Template information
                      $rootScope.templateInfoForAudit = {
                        "selectedCountry":"",
                        "selectedLanguage":"",
                        "templateId" : ""          
                      }; 
                   
                      JSONToCSVConvertor($scope.finalData,csvFileName,true);

                     
                     },function(error){             
                       $rootScope.shouldShowTyreImg = false;
                       $scope.$apply();                   
                    });

                },function(error){             
                   $rootScope.shouldShowTyreImg = false;
                   $scope.$apply();                 
                });

            }else{

                $scope.countrySelected = false;
                $scope.countryLangSelected.enableFlag = false;
                $scope.templateSelected = false;
                
                // Template information
                $rootScope.templateInfoForAudit = {
                  "selectedCountry":"",
                  "selectedLanguage":"",
                  "templateId" : ""          
                }; 

                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
                $scope.modalContent = dealerAudit_ConstantsConst.noAuditsAvailable;
                $scope.openModel("downloadExcelError");
                    
                $scope.initLoad();
                //return;
         
            }
        }catch(e){
         
        }
      }

    /**
     * @function JSONToCSVConvertor
     * @description function prepare the excel data headers and all rows which fit in excel sheet, triggers download of csv file   
     */
      function JSONToCSVConvertor(JSONData,ReportTitle,ShowLabel) {

        try{
         
          var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;    
          var CSV = '';    
          
          if (ShowLabel) {
              var row = "";     
              for (var index in arrData[0]) {    
                  row += index + ',';
              }     
              CSV += row + '\r\n';
          }
        
          for (var i = 0; i < arrData.length; i++) {
              var row = "";         
              for (var index in arrData[i]) {          
                row += arrData[i][index] + ',';          
              }        
              CSV += row + '\r\n';
          }
         
          if (CSV == '') {        
              alert("Invalid data");
              return;
          }  

          var fileName = ReportTitle.replace(/ /g,"_");  
          var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);  
          var link = document.createElement("a"); 

          if(typeof link.download != "undefined")
          {
          
                 
              link.href = uri;    
              link.style = "visibility:hidden;";
              link.download = fileName + ".csv";  
              document.body.appendChild(link);
              link.click();
              document.body.removeChild(link);

          }else{

              var csvContent=CSV;
              var blob = new Blob([csvContent],{
                  type: "text/csv;charset=utf-8;"
              });

              navigator.msSaveBlob(blob, fileName + ".csv");


          }
          $scope.initLoad();
          }catch(e){
            $rootScope.shouldShowTyreImg = false;
            
        }
      }

    /**
     * @function generatePDF
     * @description function prepare pdf data and download pdf.   
     */
      $scope.generatePDF = function() {

        
        try{

              var headerImage = dealerAudit_ConstantsConst.newLogoHeader;
              var footerImage = dealerAudit_ConstantsConst.newLogoFooter;
            

              var doc = new jsPDF();
              var pdfFileName='';
              doc.page=1;
              var columns = [
                    {title: "Header", dataKey: "header"},
                    {title: "Sub Header", dataKey: "subHeader"}, 
                    {title: "Question", dataKey: "question"},
                    {title: "Answer", dataKey: "answer"}                           
                ];
              var rows =[];
              for(i=0;i<$rootScope.questionsAndAnsInAudit.length;i++){
               // var auditDate = $filter('date')($rootScope.auditDetailsPerDealer[i].AuditDate, 'dd-MM-yyyy'); 
               var ans = $rootScope.questionsAndAnsInAudit[i].answer;
               if(ans == 'true'){
                ans = 'Yes';
               }else if(ans == 'false'){
                ans = 'No';
               }
                var dynamicObj={"header" :$rootScope.questionsAndAnsInAudit[i].header_text, 
                                "subHeader":$rootScope.questionsAndAnsInAudit[i].sub_header_text,
                                "question" : $rootScope.questionsAndAnsInAudit[i].question_text, 
                                "answer": ans                         
                               };


                rows.push(dynamicObj);
                doc.setDrawColor(0);
                doc.setFillColor(0, 85, 165);
                doc.rect(6, 6, 200, 25, 'F');
                doc.addImage(headerImage, 'JPEG', 170, 10, 30, 18);

                //doc.addImage(headerImage, 'JPEG',0, 0, 210, 35, 2143, 523, 127542);

                if(i==$rootScope.questionsAndAnsInAudit.length-1){          

                            doc.setFont(dealerAudit_ConstantsConst.pdfConstants.font);
                            doc.setFontSize(14);
                            doc.setFontType(dealerAudit_ConstantsConst.pdfConstants.fontType);
                            doc.setTextColor(255, 255, 255);
                            doc.text(20, 20, $rootScope.selectedAuditFromList[0].TempName);

                            doc.setFontSize(8);
                            doc.setFontStyle(dealerAudit_ConstantsConst.pdfConstants.fontStyle);
                            doc.setTextColor(0, 0, 0);

                            var companyName="";                                                  
                            var auditPersonContact="";
                            var country="";
                            var tts="";
                            var city="";
                            var auditDoneBy="";
                            var fosCode="";
                            var auditDate="";
                            var address="";
                            var postCode="";
                             

                            if($rootScope.selectedAuditFromList[0].DelearName!=undefined && $rootScope.selectedAuditFromList[0].DelearName!=null){
                              companyName = $rootScope.selectedAuditFromList[0].DelearName.toUpperCase();
                            }

                            if($rootScope.selectedAuditFromList[0].AuditPersonContact!=undefined && $rootScope.selectedAuditFromList[0].AuditPersonContact!=null){
                              auditPersonContact = $rootScope.selectedAuditFromList[0].AuditPersonContact;
                            } 
                            if($rootScope.selectedAuditFromList[0].Country!=undefined && $rootScope.selectedAuditFromList[0].Country!=null){
                              country = $rootScope.selectedAuditFromList[0].Country;
                            } 
                            if($rootScope.selectedAuditFromList[0].tts!=undefined && $rootScope.selectedAuditFromList[0].tts!=null){
                              tts = $rootScope.selectedAuditFromList[0].tts;
                            } 
                            if($rootScope.selectedAuditFromList[0].city!=undefined && $rootScope.selectedAuditFromList[0].city!=null){
                              city = $rootScope.selectedAuditFromList[0].city;
                            }
                            if($rootScope.selectedAuditFromList[0].AuditDoneBy!=undefined && $rootScope.selectedAuditFromList[0].AuditDoneBy!=null){
                              auditDoneBy = $rootScope.selectedAuditFromList[0].AuditDoneBy;
                            }
                            if($rootScope.selectedAuditFromList[0].Pos_code!=undefined && $rootScope.selectedAuditFromList[0].Pos_code!=null){
                              fosCode = $rootScope.selectedAuditFromList[0].Pos_code;
                            } 
                            if($rootScope.selectedAuditFromList[0].address!=undefined && $rootScope.selectedAuditFromList[0].address!=null){
                              address = $rootScope.selectedAuditFromList[0].address;
                            } 
                            if($rootScope.selectedAuditFromList[0].postCode!=undefined && $rootScope.selectedAuditFromList[0].postCode!=null){
                              postCode = $rootScope.selectedAuditFromList[0].postCode;
                            } 
                            var audit_date = $filter('date')($rootScope.selectedAuditFromList[0].AuditDate, dealerAudit_ConstantsConst.pdfConstants.dateFormat); 
                            var auditComments=$rootScope.selectedAuditFromList[0].auditComments;
                            if(auditComments=='undefined'){
                              auditComments='';
                            }
                            doc.text(10, 50, dealerAudit_ConstantsConst.pdfConstants.companyName);
                            doc.text(40, 50, ": " + companyName);

                            doc.text(100, 50, dealerAudit_ConstantsConst.pdfConstants.auditPerson);
                            doc.text(140, 50, ": " + auditPersonContact);

                            doc.text(10, 55, dealerAudit_ConstantsConst.pdfConstants.country);
                            doc.text(40, 55, ": " + country);

                            doc.text(100, 55, dealerAudit_ConstantsConst.pdfConstants.ttsName);
                            doc.text(140, 55, ": " + tts);

                            doc.text(10, 60, dealerAudit_ConstantsConst.pdfConstants.city);
                            doc.text(40, 60, ": " + city);

                            doc.text(100, 60, dealerAudit_ConstantsConst.pdfConstants.auditDoneBy);
                            doc.text(140, 60, ": " + auditDoneBy);

                            doc.text(10, 65, dealerAudit_ConstantsConst.pdfConstants.fosCode);
                            doc.text(40, 65, ": " + fosCode);

                            doc.text(100, 65, dealerAudit_ConstantsConst.pdfConstants.auditDate);
                            doc.text(140, 65, ": " + audit_date);

                            doc.text(10, 70, dealerAudit_ConstantsConst.pdfConstants.address);
                            doc.text(40, 70, ": " +address + "," + postCode);

                            doc.text(10, 75, dealerAudit_ConstantsConst.pdfConstants.comments);
                            doc.text(40, 75, ": " +auditComments);

                          doc.autoTable(columns, rows, {
                                theme: 'grid',
                                startY: 95,
                                margin: {left: 20, right: 20},
                                tableWidth: 'wrap',
                                columnWidth: 'wrap',                                
                                overflowColumns: true,
                                styles: {overflow: 'linebreak',cellPadding: 2},
                                headerStyles: {
                                    rowHeight: 7,
                                    fillColor: [0, 85, 165],
                                    fontSize: 8
                                },
                                bodyStyles: {rowHeight: 5, fontSize: 8, valign: 'middle'}
                            });

                            doc.setDrawColor(0, 85, 165);
                            doc.setLineWidth(1);
                            doc.line(20, 276, 160, 276);
                            doc.addImage(footerImage, 'JPEG', 170, 277, 32, 14);


                          var pdfDate = new Date($rootScope.selectedAuditFromList[0].AuditDate);
                          var pdfMonth = pdfDate.getMonth() + 1;
                          companyNameForFileName=companyName.replace(/ /g,'_');
                          pdfFileName = companyNameForFileName + "_" + pdfDate.getDate() + pdfMonth + pdfDate.getFullYear() +
                              "_" + pdfDate.getHours() + pdfDate.getMinutes();
                         
                }
              }
               doc.save(pdfFileName+'.pdf');

          }catch(e){
            $rootScope.shouldShowTyreImg = false;
            
        }
               
      };

      $scope.uploadAudits = function(){   
               
          $state.go('audits.uploadaudits');
      };

      $scope.loadDataOfPreviousAudits = function(){
        if($rootScope.currentYearAudits.length<=0){
          $scope.noDetails = true;
        }else{
          $scope.noDetails = false;
        }
      }

      $scope.navigateToAuditList = function(){                 
          $state.go('audits.currentyearaudits');
      }

      $scope.navigateToPreviousYearAuditList = function(){             
          $state.go('audits.previousaudits');
      }

  
      var filesArray = [];
      $scope.duplicate_filenames_found=[];
      $scope.numberOfFilesUploaded=0;
      var readFile = function readFile() {
        try{
            
           
            filesArray =[];
            var files=this.files;
            for(i=0;i<files.length;i++){                  

                      (function(ind) { setTimeout(function(){   
                         var fileNameExist=false;

                          //each file is compared with all the uploaded files to check for duplicate names
                          for(j=0;j<$scope.auditFiles.length;j++){

                            //if file name found duplicate making flag true
                           
                            if(files[ind].name == $scope.auditFiles[j].file_name){
                              fileNameExist = true;
                            }
                              
                          } 
                          //if file name is uniques
                          if(!fileNameExist){
                             var file = {
                                      name : files[ind].name,
                                      binary: files[ind],
                              }
                              filesArray.push(file);                            

                          }
                          //if file name is found duplicate , storing file name information
                          else{
                              $scope.duplicate_filenames_found.push(files[ind].name);
                          }

                          if(files.length == ind+1){
                              
                              $scope.uploadAuditFiles(filesArray);
                          } 
                         

                      }, 1500 * (ind) ); })(i);                  
              }


             
          }catch(e){
             
          }
      
    }


   
    $scope.uploadAuditFiles = function(filesArray){

        try{

              if(!$scope.fileUploadInPorcess){  

                $scope.fileUploadInPorcess =true;
                $rootScope.shouldShowTyreImg = true;
                $scope.$apply(); 
                
                if(filesArray.length>0){
                for(i=0;i<filesArray.length;i++){
                  (function(ind) { setTimeout(function(){ 

                     

                      ISession.getInstance().then(function(session) {

                      session.getClientObjectType("audit_upload").then(function(clientObjectType){
                        //converting base64 data to blob object                  
                          var date = new Date();
                          var date_upload = date.getTime();
                          var params = {
                            tam_name:$rootScope.UserName,
                            file_data:filesArray[ind].binary,
                            file_name:filesArray[ind].name,
                            upload_date:date_upload
                          }                          
                          
                          clientObjectType.create(params).then(function(response){                              

                              if(filesArray.length == ind+1){    

                                $scope.modalContent  = filesArray.length + " Files Uploaded Successfully.";

                                if($scope.duplicate_filenames_found.length>0){
                                    $scope.modalContent  =$scope.modalContent  + "\n"+" The Below file names already exists, Please change the name of the file and start the upload process again"
                                    for(i=0;i<$scope.duplicate_filenames_found.length;i++){                      
                                          $scope.modalContent  = $scope.modalContent + "\n" +$scope.duplicate_filenames_found[i]                      
                                      
                                    }
                                }
                                filesArray =[];
                                $scope.duplicate_filenames_found= [];
                                $scope.fileUploadInPorcess =false;
                                $rootScope.shouldShowTyreImg = false;

                               

                                $scope.modalTitle = dealerAudit_ConstantsConst.successModelTitle;                               
                                $scope.openModel("alertHandle");

                                $scope.retrieveAuditFiles();
                                angular.element("input[type='file']").val(null);
                              }

                          },function(error){
                              $rootScope.shouldShowTyreImg = false;
                              
                          });
                      },function(error){
                            $rootScope.shouldShowTyreImg = false;
                           
                      });
                      }, function(error) {   
                            $rootScope.shouldShowTyreImg = false; 
                                
                      }); 
                   }, 800 * (ind) ); })(i); 
                }
                }else{
                  if($scope.duplicate_filenames_found.length>0){
                    $scope.modalContent = "The Below file name already Exists, Please change the Name of the file and start the Upload process again ."
                    for(i=0;i<$scope.duplicate_filenames_found.length;i++){                      
                          $scope.modalContent = $scope.modalContent + "\n" +$scope.duplicate_filenames_found[i]                     
                      
                    }
                  }
                  $rootScope.shouldShowTyreImg = false;
                  $scope.fileUploadInPorcess =false;
                  $scope.duplicate_filenames_found= [];
                  $scope.modalTitle = dealerAudit_ConstantsConst.successModelTitle;                 
                  $scope.openModel("alertHandle");
                                
                } 
              }

        }catch(e){
            $rootScope.shouldShowTyreImg = false;
        }

    }

    var enp = document.getElementById("inp");
      if(enp){
      document.getElementById("inp").addEventListener("change", readFile);
    }    

    $scope.auditFiles = [];
    $scope.noAuditFiles = false;
    $scope.retrieveAuditFiles = function(){

      try{
        $rootScope.shouldShowTyreImg = true;
        ISession.getInstance().then(function(session) {
            session.getClientObjectType("audit_upload").then(function(clientObjectType){
                      $scope.auditFiles = [];
                      var filterParams ={
                        tam_name:$rootScope.UserName
                      }
                      clientObjectType.search("audit_upload",0,10000, filterParams, null, null, null, null, null, true).then(function(response) {
                        for(i=0;i<response.length;i++){
                          $scope.auditFiles.push(response[i].getData());
                          $scope.auditFiles[i]['checked']=false;
                        }
                        
                        $rootScope.shouldShowTyreImg = false;
                        $scope.$apply();
                      },function(error){
                         $rootScope.shouldShowTyreImg = false;
                         $scope.noAuditFiles = true;
                         $scope.$apply();
                        
                      });

            },function(error){
                $rootScope.shouldShowTyreImg = false;
                $scope.$apply();
                
            });
         }); 


      }catch(e){
        $rootScope.shouldShowTyreImg = false;
       
      }
    }


   

    $scope.downloadSelectedFiles = function(){

      $rootScope.shouldShowTyreImg = true;      
      ISession.getInstance().then(function(session) {
         session.getClientObjectType("audit_upload").then(function(clientObjectType){

              for(i=0;i<$rootScope.selected_files.length;i++){              

                  (function(ind) { setTimeout(function(){ 
                         
                                 
                                    var filterParams ={
                                      file_id:$rootScope.selected_files[ind].file_id
                                    }
                                    
                                    clientObjectType.get(filterParams.file_id).then(function(response) {                            
                                        

                                        
                                        $scope.unZipAndDownloadFiles(response.data.file_data,$rootScope.selected_files[ind].file_name);
                                        
                                       
                                        for(j=0;j<$scope.auditFiles.length;j++){
                                            if($scope.auditFiles[j].file_id==$rootScope.selected_files[ind].file_id){
                                               $scope.auditFiles[j].checked=false;
                                            }
                                        }
                                        if($rootScope.selected_files.length == ind+1){                                
                                            $rootScope.selected_files=[];
                                            $rootScope.shouldShowTyreImg = false;
                                        }
                                        $scope.$apply();
                                    },function(error){
                                        $rootScope.shouldShowTyreImg = false;
                                    });

                          
                  }, 2000 * (ind) ); })(i);
              }
        },function(error){
                $rootScope.shouldShowTyreImg = false;
        });
     },function(e){
         $rootScope.shouldShowTyreImg = false;
     }); 
    }


    $scope.unZipAndDownloadFiles = function(blob,file_name) {        

        var content = 'data:application/octet-stream;base64,' + blob;
        var myBlob = dataURItoBlob(content);       
        var data = encodeURI(content);
        var downloadLink = document.createElement('a');

        if(typeof downloadLink.download != "undefined")
        {
           // download attribute is supported           
            /*downloadLink.setAttribute('href', data);
            downloadLink.setAttribute('download', file_name);
            downloadLink.click();
            downloadLink.remove();*/

            url = URL.createObjectURL(myBlob);
            
            downloadLink.setAttribute("href",url);
            downloadLink.setAttribute("download",file_name);

            downloadLink.click();
            downloadLink.remove();
        }
        else
        {
          // download attribute is not supported
          window.navigator.msSaveBlob(myBlob, file_name);
        }


    };


    function dataURItoBlob(b64Data) {     
      
        var sliceSize = 512;
        var mimeString = b64Data.split(',')[0].split(':')[1].split(';')[0];
        b64Data = b64Data.replace(/^[^,]+,/, '');
        b64Data = b64Data.replace(/\s/g, '');


        var byteCharacters = window.atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, {type: mimeString});
        return blob;
    }

    if($rootScope.selected_files ==null || typeof($rootScope.selected_files)=='undefined'){
          $rootScope.selected_files=[]; 
    }
    
    $scope.fileSelected = function(o){     
          if(o.checked){          
            $rootScope.selected_files.push({'file_id':o.file_id,'file_name':o.file_name});
          }else{
            for(i=0;i<$rootScope.selected_files.length;i++){
                  if($rootScope.selected_files[i].file_id==o.file_id){
                    $rootScope.selected_files.splice(i, 1);
                  }
                }
          }    
          
    }

    $scope.navigateToOtherTabs = function(location){

      $scope.navigationLocation ='';
      $scope.navigationLocation= location;
      if($rootScope.selected_files.length>0){

            $scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
            $scope.modalContent = dealerAudit_ConstantsConst.discardChangesMsg;
            $scope.open("confirmNavigation");  

      }else{
         
              if($scope.navigateToLocation == 'login'){
                    $rootScope.logoutFlag = true;       
                    localStorage.removeItem('sessionObj');
                  }
              $scope.selectedTab=$scope.navigationLocation;
               $state.go($scope.navigationLocation);
            
          
      }
    }

      

  }catch(error){

    
  }

});


