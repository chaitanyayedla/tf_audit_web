/*
 * @controller: auditController
 * @description This module includes functionalities for displaying and downloading Audits, audit details.
 */

angular = require('angular');

angular.module('dealerAudit').controller('manageTeamsController', function ($scope,$state,$rootScope,$uibModal,$location,dealerAudit_ConstantsConst) {
try {

	$scope.showTtsListDiv = false;
	$scope.showSearch = false;
	$scope.search={};	
	$scope.assignFlag= false;
	$scope.revokeFlag= false;
	$scope.noOfTtsSelectedToAssign=0;	
	$scope.ttsListAvailableToAssign=[];
	$scope.ttsListSelectedToAssign=[];
	$scope.noTTSAddedForManager=false;
	$scope.noDetails=false;
		

	if($rootScope.ttsListFromTtsMaster ==null || typeof($rootScope.ttsListFromTtsMaster) =='undefined'){
			$rootScope.ttsListFromTtsMaster=[];
	}
	if($scope.ttsListSelectedToRevoke ==null || typeof($scope.ttsListSelectedToRevoke) =='undefined'){
			$scope.ttsListSelectedToRevoke=[];
	}
	
	  if(screen.width <=1000){
	    $scope.numberOfrows =  6;
	  }          
	  if(screen.width >1000 && screen.width <1500){
	    $scope.numberOfrows =  8;
	  }
	 
	  if(screen.width >1500 ){
	    $scope.numberOfrows =  11;
	  }
	  $scope.ttsSelectedToRevoke = false;
	// watch function keeps track of tts selected to assign
	$scope.$watch(function(){ 	
			
		$scope.noOfTtsSelectedToAssign = $scope.ttsListSelectedToAssign.length;

	});

	 /**
     * @function open
     * @description function to open the bootstrap confirmation popup model 
     */
        $scope.open = function(value) {
            $scope.modalInstance = $uibModal.open({
        
                animation: true,
                templateUrl: '/partials/confirmationmodal.html',
                controller: 'manageTeamsController',
                size: 'sm',
                scope: $scope,
                // Prevent closing by clicking outside or escape button.
                backdrop: 'static',
                keyboard: false
            });
            
            $scope.modalInstance.modalValue = value;
        };

     /**
     * @function openModel
     * @description function to open the bootstrap alert window popup  
     */
        $scope.openModel = function(value) {
            $scope.modalInstance = $uibModal.open({
        
            animation: true,
            templateUrl: '/partials/alertpopup.html',
            controller: 'modelController',
            size: 'sm',
            scope: $scope,
            // Prevent closing by clicking outside or escape button.
            backdrop: 'static',
            keyboard: false
          });
      
          $scope.modalInstance.modalValue = value;
        };
    /**
     * @function ok
     * @description function to perform actions when user ok cancel button on confirmation popup
     */
    $scope.ok = function() {
    	     if($scope.modalInstance.modalValue == "revokeTTS"){  
    	            $scope.revokeFlag= false;                  
                    $scope.revokeSelectedTTS();    
                    $scope.modalInstance.dismiss('cancel');                    
             }           
             else
             {    
                    $scope.modalInstance.dismiss('cancel');                               
                  
             }

    }
    /**
     * @function cancel
     * @description function to perform actions when user click cancel button on confirmation popup
     */
    $scope.cancel = function() {
            
         	$scope.revokeFlag= false;       
            $scope.modalInstance.dismiss('cancel');                        
          
    };  
	$scope.initLoad= function(){
		ISession.getInstance().then(function(session) {
              	$rootScope.session = session;
              	$scope.getTTSAssignedToManager();
              	$scope.getTTSAvailableToAssign();
     	},function(error) {  
     		$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
            $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
            $scope.openModel("connectionError");      
          
        });         	
	}

	$scope.applyFilterToTTSSearchToAssign= function(ttsListToAssign){

		if (!$scope.search.searchForTTSToAssign 
			||(ttsListToAssign.tts_name.indexOf($scope.search.searchForTTSToAssign) != -1)
			||(ttsListToAssign.tts_name.toLowerCase().indexOf($scope.search.searchForTTSToAssign.toLowerCase()) != -1)
			||(ttsListToAssign.tts_name.toUpperCase().indexOf($scope.search.searchForTTSToAssign.toUpperCase()) != -1)
			){

			return true;
		}else{
			
			return false;
		}
		return false;
	}

	$scope.applyFilterToTTSSearch= function(ttsListAssigned){

		if (!$scope.search.searchForTTSAssigned 
			||(ttsListAssigned.tts_name.indexOf($scope.search.searchForTTSAssigned) != -1)
			||(ttsListAssigned.tts_name.toLowerCase().indexOf($scope.search.searchForTTSAssigned.toLowerCase()) != -1)
			||(ttsListAssigned.tts_name.toUpperCase().indexOf($scope.search.searchForTTSAssigned.toUpperCase()) != -1)
			||(ttsListAssigned.first_name.indexOf($scope.search.searchForTTSAssigned) != -1)
			||(ttsListAssigned.first_name.toLowerCase().indexOf($scope.search.searchForTTSAssigned.toLowerCase()) != -1)
			||(ttsListAssigned.first_name.toUpperCase().indexOf($scope.search.searchForTTSAssigned.toUpperCase()) != -1)
			||(ttsListAssigned.last_name.indexOf($scope.search.searchForTTSAssigned) != -1)
			||(ttsListAssigned.last_name.toLowerCase().indexOf($scope.search.searchForTTSAssigned.toLowerCase()) != -1)
			||(ttsListAssigned.last_name.toUpperCase().indexOf($scope.search.searchForTTSAssigned.toUpperCase()) != -1)){
			
			$scope.noDetails=false;
			return true;

		}else{
			$scope.noDetails=true;
			return false;
		}
		return false;
	}


	$scope.ttsSelected = function(o){			
    		if(o.checked){    			
    			$scope.ttsListSelectedToRevoke.push({'tts_name':o.tts_name,'tts_id':o.tts_id});
    		}else{
    			for(i=0;i<$scope.ttsListSelectedToRevoke.length;i++){
            		if($scope.ttsListSelectedToRevoke[i].tts_name==o.tts_name){
            			$scope.ttsListSelectedToRevoke.splice(i, 1);
            		}
            	}
    		}    		
    	
	}
	$scope.getTTSAssignedToManager = function(){
		try{
			$rootScope.shouldShowTyreImg = true;
          		$rootScope.ttsListFromTtsMaster=[];
              	var filterParams={
              		"tam_name":$rootScope.UserName
              	}              	 
              	$rootScope.session.getClientObjectType("TTS_master").then(function(clientObjectType){
	                               
	                                clientObjectType.search("TTS_master", 0, 10000, filterParams, null, null, null, null, null, true).then(function(response) {
	                                	
	                                	for(i=0;i<response.length;i++){
                                			$rootScope.ttsListFromTtsMaster.push(response[i].getData());
	                                	}

	                                	$rootScope.ttsListFromTtsMaster.forEach(function(o){
	                                		o['checked']=false;
	                                	})
	                                	$scope.noTTSAddedForManager=false;
	                                	$rootScope.shouldShowTyreImg = false;  
	                                	$scope.$apply(); 
	                                	                         	
	                                },function(error){
	                                	
	                                	$scope.noTTSAddedForManager=true;
	                                	$rootScope.shouldShowTyreImg = false;
	                                	$scope.$apply(); 
	                                	
	                                });
	                             },function(error){		
	                                $rootScope.shouldShowTyreImg = false; 
	                                $scope.$apply();   
	                                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
						            $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
						            $scope.openModel("connectionError");                          	
			                    	
			                    });              
          

		}catch(e){
			$rootScope.shouldShowTyreImg = false; 
	        $scope.$apply(); 
			
		}

	};


	$scope.getTTSAvailableToAssign = function(){
		try{
							 $scope.ttsListAvailableToAssign=[];
              	           	 $rootScope.session.getClientObjectType("TTS_master").then(function(clientObjectType){
	                               
	                                clientObjectType.list(0, 100000,null,true).then(function(response) {
	                                	var tempArray=[];
	                                	
	                                	for(i=0;i<response.length;i++){
                                			tempArray.push(response[i].getData());
	                                	}	
	                                	//Pushing the tts which do not have any manager 
	                                	tempArray.forEach(function(o){
	                                		if(o.tam_name=='' || o.tam_name==null || o.tam_name=='undefined' || o.tam_name==undefined){
	                                				$scope.ttsListAvailableToAssign.push(o);
	                                		}
	                                	}); 
	                                	
	                                },function(error){	
	                                	//$rootScope.shouldShowTyreImg = false;
	                                	
	                                });
	                             },function(error){	
	                             	//$rootScope.shouldShowTyreImg = false;
	                             	$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
						            $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
						            $scope.openModel("connectionError");
			                    	
			                    });              
          

		}catch(e){

			$rootScope.shouldShowTyreImg = false; 
	        $scope.$apply(); 
			
		}

	};

	$scope.assignSelectedTTS = function(){

	
		try{
			if(!$scope.assignFlag){
				$scope.assignFlag= true;
          		if($scope.ttsListSelectedToAssign.length>0){

          			$rootScope.shouldShowTyreImg = true;
          			$scope.ttsListSelectedToRevoke = [];
          			$rootScope.session.getClientObjectType("TTS_master").then(function(clientObjectType){
          			for(i=0;i<$scope.ttsListSelectedToAssign.length;i++){

          			(function(ind) { setTimeout(function(){	
			              	var ttsToAssign={
			              		'tts_id':$scope.ttsListSelectedToAssign[ind].tts_id,
			              		
			              	} 
			              
				                    clientObjectType.get(ttsToAssign.tts_id).then(function(responseObject){  
			                        			                        		
			                        		var updateRecord = {			                        		
							              		tam_name:$rootScope.UserName							              		
							              	}						              
							              	
				                            responseObject.update(updateRecord).then(function(response){				                            		
						                        				                        	
				                            		
						                        	if($scope.ttsListSelectedToAssign.length == ind+1){
						                        		$rootScope.shouldShowTyreImg = false;
						                        		$scope.assignFlag= false;
						                        		$scope.search={};	
						                        		$scope.ttsListSelectedToAssign=[];
						                        		$scope.initLoad();

						                        	}	
						                        	$scope.$apply(); 
				                        	},function(error){
					                        	$rootScope.shouldShowTyreImg = false;
					                        	$scope.$apply(); 
					                        	
					                        });
			                        },function(error){
			                        	$rootScope.shouldShowTyreImg = false;
			                        	$scope.$apply(); 
			                        	$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
							            $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
							            $scope.openModel("connectionError");
			                        	
			                        });
		                                 
	          
					}, 500 * (ind) ); })(i); 

				}
				},function(error){		
                    $rootScope.shouldShowTyreImg = false;  
                    $scope.$apply(); 
                    $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
		            $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
		            $scope.openModel("connectionError");                           	
                	
                });  
			}
		}
		}catch(e){
			
		}


	}
	$scope.revokeSelectedTTSClicked = function(){
		if(!$scope.revokeFlag){
			if($scope.ttsListSelectedToRevoke.length>0){
				$scope.modalTitle = dealerAudit_ConstantsConst.confirmationModelTitle;
	            $scope.modalContent = dealerAudit_ConstantsConst.removeTTSMsg;
	            $scope.open("revokeTTS");
	        }else{
	        	$scope.modalTitle =dealerAudit_ConstantsConst.errorModalTitle;
	            $scope.modalContent = dealerAudit_ConstantsConst.noTTSSelectedToRemoveMsg;
	            $scope.openModel("alertHandle");
	        } 
		}
	}
	$scope.revokeSelectedTTS = function(){

		

		try{
			
			if($scope.ttsListSelectedToRevoke.length>0){
			$rootScope.shouldShowTyreImg = true;


				$rootScope.session.getClientObjectType("TTS_master").then(function(clientObjectType){
          		
          		for(i=0;i<$scope.ttsListSelectedToRevoke.length;i++){

          			

          			(function(ind) { setTimeout(function(){	

			              	var ttsToUnAssign={			              		
			              		'tts_id':$scope.ttsListSelectedToRevoke[ind].tts_id,
			              	} 
			                  
		                    clientObjectType.get(ttsToUnAssign.tts_id).then(function(responseObject){
		                    		var updateRecord = {			                        			
					              		tam_name:""
					              	}							              	
		                    		responseObject.update(updateRecord).then(function(response){             	
				                    		
				                    		for(j = 0; j< $rootScope.ttsListFromTtsMaster.length ; j++){
						                          if($rootScope.ttsListFromTtsMaster[j].tts_id == $scope.ttsListSelectedToRevoke[ind].tts_id){
						                            
						                              $rootScope.ttsListFromTtsMaster.splice(j,1);                                           

						                          }
						                      }

				                        	if($scope.ttsListSelectedToRevoke.length == ind+1){	
				                        	    					                        		
				                        		$rootScope.shouldShowTyreImg = false; 
				                        		$scope.search={};	
				                        		$scope.ttsListSelectedToRevoke=[];
				                        		//$scope.initLoad();
				                        		$scope.$apply(); 
				                        		$state.reload();
				                        	}	
				                        	
		                        	},function(error){
			                        	$rootScope.shouldShowTyreImg = false;
			                        	$scope.$apply(); 
			                        	
			                        });
		                    },function(error){
		                    	$rootScope.shouldShowTyreImg = false;
		                    	$scope.$apply(); 
		                    	$scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
					            $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
					            $scope.openModel("connectionError");
		                    	
		                    });
		            
					}, 300 * (ind) ); })(i); 

				}
			},function(error){		
                $rootScope.shouldShowTyreImg = false;    
                $scope.$apply();
                $scope.modalTitle = dealerAudit_ConstantsConst.errorModalTitle;
	            $scope.modalContent = dealerAudit_ConstantsConst.connectionError;
	            $scope.openModel("connectionError");                          	
            	
            });              
	          
			}
		}catch(e){
			
		}
	}
}catch(e){
	
}

});

angular.module('dealerAudit').directive("checkboxgrouptounassign", function() {
        return {
            restrict: "A",
            link: function(scope, elem, attrs) {
                
                // Update array on click
                elem.bind('click', function() {
                   
                    // Add if checked

                    if (elem[0].checked) {
                        scope.ttsListSelectedToRevoke.push({'tts_name':scope.tts.tts_name,'tts_id':scope.tts.tts_id});
                    }
                    // Remove if unchecked
                    else {
                    	for(i=0;i<scope.ttsListSelectedToRevoke.length;i++){
                    		if(scope.ttsListSelectedToRevoke[i].tts_name==scope.tts.tts_name){
                    			scope.ttsListSelectedToRevoke.splice(i, 1);
                    		}
                    	}
                        
                    }
                    // Sort and update DOM display
                    scope.$apply(scope.ttsListSelectedToRevoke.sort(function(a, b) {
                        return a - b
                    }));
                });
            }
        }
    });
angular.module('dealerAudit').directive("checkboxgrouptoassign", function() {
        return {
            restrict: "A",
            link: function(scope, elem, attrs) {
                
                // Update array on click
                elem.bind('click', function() {
                    
                    // Add if checked
                    if (elem[0].checked) {
                        scope.ttsListSelectedToAssign.push({'tts_name':scope.ttsToAssign.tts_name,'tts_id':scope.ttsToAssign.tts_id});
                    }
                    // Remove if unchecked
                    else {
                        
                         for(i=0;i<scope.ttsListSelectedToAssign.length;i++){
                    		if(scope.ttsListSelectedToAssign[i].tts_name==scope.ttsToAssign.tts_name){
                    			scope.ttsListSelectedToAssign.splice(i, 1);
                    		}
                    	}
                    }
                    // Sort and update DOM display
                    scope.$apply(scope.ttsListSelectedToAssign.sort(function(a, b) {
                        return a - b
                    }));
                });
            }
        }
    });