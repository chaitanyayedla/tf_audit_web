'use strict';

angular = require('angular');

angular.module('dealerAudit').directive('googleregion', function()
 {
 	try{
 	return {
		require: 'ngModel',
		link: function(scope, element, attrs, model) {
			var options = {
				types: ['(regions)']			
			};			
			scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

			google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
				scope.$apply(function() {
					model.$setViewValue(element.val());
				});
			});
		}
	};

}catch(e){

	console.log('error in googleregion directive =>')
}
});